<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();
Route::get('/ayuda/juan', [App\Http\Controllers\AdmonController::class, 'Juan']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/login_administrador', [App\Http\Controllers\AdmonController::class, 'login_admon']);


Route::group(['prefix'=>'admon','middleware' => ['auth:admin']],function(){

  Route::get('/obtain/data/home', [App\Http\Controllers\AdmonController::class, 'obtain_data_home']);


  Route::post('/eliminar/foto/1', [App\Http\Controllers\AdmonController::class, 'eliminar_foto1']);
  Route::post('/eliminar/foto/2', [App\Http\Controllers\AdmonController::class, 'eliminar_foto2']);
  Route::post('/eliminar/foto/3', [App\Http\Controllers\AdmonController::class, 'eliminar_foto3']);

  Route::get('/encrypted/pass', [App\Http\Controllers\AdmonController::class, 'encrypted']);
  Route::get('/home', [App\Http\Controllers\AdmonController::class, 'home']);
  Route::get('/logout', [App\Http\Controllers\AdmonController::class, 'logout_admin'])->name('admin.logout');
  ///CATEGORIAS//////
  Route::get('/categorias', [App\Http\Controllers\AdmonController::class, 'categorias']);
  Route::get('/obtain_categories', [App\Http\Controllers\AdmonController::class, 'obtain_categories']);
  Route::post('/guard_categorie', [App\Http\Controllers\AdmonController::class, 'guard_categorie']);
  Route::post('/delete_categoria', [App\Http\Controllers\AdmonController::class, 'delete_categoria']);

  //////PRODUCTOS////////
  Route::get('/productos', [App\Http\Controllers\AdmonController::class, 'productos']);
  Route::get('/obtain_productos', [App\Http\Controllers\AdmonController::class, 'obtain_productos']);
  Route::post('/guard_productos', [App\Http\Controllers\AdmonController::class, 'guard_productos']);
  Route::post('/delete_productos', [App\Http\Controllers\AdmonController::class, 'delete_productos']);

  ///////COBERURA///////////
  Route::get('/config/cobertura', [App\Http\Controllers\AdmonController::class, 'cobertura']);
  Route::post('/obtain_cobertura', [App\Http\Controllers\AdmonController::class, 'obtain_cobertura']);
  Route::post('/guard_cobertura', [App\Http\Controllers\AdmonController::class, 'guard_cobertura']);

  ////////TABLES/////////
  Route::get('/mesas', [App\Http\Controllers\AdmonController::class, 'mesas']);
  Route::get('/obtain_tables', [App\Http\Controllers\AdmonController::class, 'obtain_tables']);
  Route::post('/guard_tables', [App\Http\Controllers\AdmonController::class, 'guard_tables']);
  Route::post('/delete_cobertura', [App\Http\Controllers\AdmonController::class, 'delete_cobertura']);

  //////INFO RESTAURANT////
  Route::get('/config/inicial', [App\Http\Controllers\AdmonController::class, 'Info_rest']);
  Route::post('/guard_info', [App\Http\Controllers\AdmonController::class, 'guard_info']);
  Route::get('/obtain_info', [App\Http\Controllers\AdmonController::class, 'obtain_info']);
  Route::get('/obtain_departament', [App\Http\Controllers\AdmonController::class, 'obtain_departament']);
  Route::get('/obtain_municipio', [App\Http\Controllers\AdmonController::class, 'obtain_municipio']);

  ////Bannners ///////
  Route::get('/banners', [App\Http\Controllers\AdmonController::class, 'Banners']);
  Route::post('/guard_img', [App\Http\Controllers\AdmonController::class, 'guard_img']);
  Route::get('/obtain_img', [App\Http\Controllers\AdmonController::class, 'obtain_img']);


  /////USUARIOS ////////
  Route::get('/config/usuarios', [App\Http\Controllers\AdmonController::class, 'usuarios']);
  Route::get('/obtain_type_users', [App\Http\Controllers\AdmonController::class, 'obtain_type_users']);
  Route::get('/obtain_type_documents', [App\Http\Controllers\AdmonController::class, 'obtain_type_documents']);
  Route::get('/obtain_users', [App\Http\Controllers\AdmonController::class, 'obtain_users']);
  Route::post('/guard_users', [App\Http\Controllers\AdmonController::class, 'guard_users']);
  Route::post('/delete_users', [App\Http\Controllers\AdmonController::class, 'delete_users']);

  //////INFO RESTAURANT////
  Route::get('/Info_rest', [App\Http\Controllers\AdmonController::class, 'Info_rest']);
  Route::post('/guard_info', [App\Http\Controllers\AdmonController::class, 'guard_info']);
  Route::get('/obtain_info', [App\Http\Controllers\AdmonController::class, 'obtain_info']);
  Route::get('/obtain_departament', [App\Http\Controllers\AdmonController::class, 'obtain_departament']);
  Route::get('/obtain_municipio', [App\Http\Controllers\AdmonController::class, 'obtain_municipio']);

  ////Bannners ///////
  Route::get('/Banners', [App\Http\Controllers\AdmonController::class, 'Banners']);
  Route::post('/guard_img', [App\Http\Controllers\AdmonController::class, 'guard_img']);
  Route::get('/obtain_img', [App\Http\Controllers\AdmonController::class, 'obtain_img']);

  ///Additional////////
  Route::get('/Adicionales', [App\Http\Controllers\AdmonController::class, 'Adicionales']);
  Route::post('/guard_add', [App\Http\Controllers\AdmonController::class, 'guard_add']);
  Route::get('/obtain_adicion', [App\Http\Controllers\AdmonController::class, 'obtain_adicion']);
  Route::post('/delete_adicional', [App\Http\Controllers\AdmonController::class, 'delete_adicional']);
  Route::get('/obtain_add', [App\Http\Controllers\AdmonController::class, 'obtain_add']);

  ////Clientes /////
  Route::get('/clientes', [App\Http\Controllers\AdmonController::class, 'clientes']);
  Route::get('/obtain_clients', [App\Http\Controllers\AdmonController::class, 'obtain_clients']);
  Route::post('/delete_clients', [App\Http\Controllers\AdmonController::class, 'delete_clients']);

  ///Cupones/////
  Route::get('/cupones', [App\Http\Controllers\AdmonController::class, 'cupones']);
  Route::post('/guard_cupon', [App\Http\Controllers\AdmonController::class, 'guard_cupon']);
  Route::get('/obtain_cupon', [App\Http\Controllers\AdmonController::class, 'obtain_cupon']);

  ///Cuenta///
  Route::get('/config/cuenta', [App\Http\Controllers\AdmonController::class, 'Cuenta']);
  Route::get('/obtain_admin', [App\Http\Controllers\AdmonController::class, 'obtain_admin']);
  Route::post('/cambiar', [App\Http\Controllers\AdmonController::class, 'cambiar']);
  Route::post('/recovery', [App\Http\Controllers\AdmonController::class, 'recovery']);

  //PEDIDOS////
  Route::get('/pedidos/domicilio', [App\Http\Controllers\AdmonController::class, 'pedidos']);
  Route::get('/pedidos/mesa', [App\Http\Controllers\AdmonController::class, 'pedidos2']);
  Route::get('/pedidos/terminados', [App\Http\Controllers\AdmonController::class, 'pedidos3']);

  Route::post('/obtain_pedidos', [App\Http\Controllers\AdmonController::class, 'obtain_pedidos']);
  Route::post('/edit_state_pedido', [App\Http\Controllers\AdmonController::class, 'edit_state_pedido']);

  Route::post('/delete_pedido', [App\Http\Controllers\AdmonController::class, 'delete_pedido']);


  //NORIFICACIONES///
  Route::post('/obtain/notifications', [App\Http\Controllers\AdmonController::class, 'obtainNotifications']);
  Route::post('/domicilios/check', [App\Http\Controllers\AdmonController::class, 'domicilioscheck']);

  //INVENTARIO//////

  Route::get('inventario/proveedores', [App\Http\Controllers\AdmonController::class, 'proovedores']);
  Route::get('obtain_proovedores', [App\Http\Controllers\AdmonController::class, 'obtain_proovedores']);
  Route::post('delete_proveedor', [App\Http\Controllers\AdmonController::class, 'delete_proveedor']);
  Route::post('guard_proveedores', [App\Http\Controllers\AdmonController::class, 'guard_proveedores']);
  Route::get('inventario/productos', [App\Http\Controllers\AdmonController::class, 'inventario_productos']);
  Route::get('obtain_productos_inventario', [App\Http\Controllers\AdmonController::class, 'obtain_productos_inventario']);
  Route::post('guard_producto_inventario', [App\Http\Controllers\AdmonController::class, 'guard_producto_inventario']);


  Route::post('generate_enter', [App\Http\Controllers\AdmonController::class, 'generate_enter']);
  Route::post('consult_enter', [App\Http\Controllers\AdmonController::class, 'consult_enter']);


  ////HACER Pedido
  Route::post('guard/pedido/call', [App\Http\Controllers\AdmonController::class, 'guard_pedido_call']);


});
