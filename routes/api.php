<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/guard_token', [App\Http\Controllers\API\HomeController::class, 'guard_token']);
Route::get('/obtain_data_products', [App\Http\Controllers\API\HomeController::class, 'obtain_data_products']);
Route::post('/user/register/normal', [App\Http\Controllers\API\HomeController::class, 'user_register']);
Route::post('/login/user', [App\Http\Controllers\API\HomeController::class, 'login']);
Route::post('/login/google', [App\Http\Controllers\API\HomeController::class, 'login_google']);
Route::post('/login/facebook', [App\Http\Controllers\API\HomeController::class, 'login_facebook']);
Route::post('/obtain/favorites', [App\Http\Controllers\API\HomeController::class, 'obtainFavorites']);
Route::post('/guard/favorites', [App\Http\Controllers\API\HomeController::class, 'guardFavorites']);
Route::post('/consult_data_user/domi', [App\Http\Controllers\API\HomeController::class, 'consult_data_user']);
Route::post('/consult/cupones', [App\Http\Controllers\API\HomeController::class, 'consult_cupones']);
Route::post('/consult/state/pedido', [App\Http\Controllers\API\HomeController::class, 'state_pedido']);
Route::post('/guard/pedido/user', [App\Http\Controllers\API\HomeController::class, 'guard_pedido']);
Route::post('/calificate/pedido/user', [App\Http\Controllers\API\HomeController::class, 'calificate_pedido']);
Route::post('/obtain_data_user', [App\Http\Controllers\API\HomeController::class, 'obtain_data_user']);
Route::post('/edit/user', [App\Http\Controllers\API\HomeController::class, 'edit_user']);
Route::post('/edit/password', [App\Http\Controllers\API\HomeController::class, 'edit_password']);



///////////DOMICILIARIO////////////
Route::post('/update/location/domiciliario', [App\Http\Controllers\API\HomeController::class, 'edit_location_domiciliario']);
Route::post('/login/domiciliario', [App\Http\Controllers\API\HomeController::class, 'login_domiciliario']);
Route::post('/obtain/domiciliario', [App\Http\Controllers\API\HomeController::class, 'obtain_domiciliario']);
