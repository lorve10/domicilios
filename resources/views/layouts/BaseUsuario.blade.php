<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <script src="https://kit.fontawesome.com/7f8cc9d427.js" crossorigin="anonymous"></script>

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/css/adminlte.min.css">
  <script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgEN-BcI98pQmZy_O28qx1kiEDc6e_MAE&libraries=places"></script>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
    rel="stylesheet">
    <!-- Load Leaflet from CDN -->
      <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin=""/>
      <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>

      <!-- Load Esri Leaflet from CDN -->
      <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
        integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
        crossorigin=""></script>

      <!-- Load Esri Leaflet Geocoder from CDN -->
      <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
        integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
        crossorigin="">
      <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
        integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
        crossorigin=""></script>

        <link
rel="stylesheet"
href="https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.css"
/>

<script src="https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.umd.js"></script>


  <!-- Styles -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>-->

</head>
<body class="hold-transition sidebar-mini">

  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background:#e09900;">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>

      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->

        <li class="dropdown user user-menu pt-2">
          <a href="#" style="text-decoration: none;color: black;" class="dropdown-toggle" data-toggle="dropdown">
            <img src="http://pidomi.com/demo/admin-app//template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
            <span class="hidden-xs">Administrador</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="http://pidomi.com/demo/admin-app//template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
              <p>Administrador</p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="{{ asset('admon/logout') }}" class="btn btn-default btn-flat">Cerrar</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Usuario-->


        <!-- Notifications Dropdown Menu -->
        <!-- <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell" style="color:black;"></i>
            <span class="badge badge-warning navbar-badge" style="color:white; background-color:#c20000 !important;">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li> -->
        <li class="nav-item">
          <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt" style="color:black;"></i>
          </a>
        </li>

      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="{{ asset('admon/home') }}" class="brand-link d-flex justify-content-center">
        <span class="brand-text font-weight-light">FoodLike</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user (optional) -->


        <!-- SidebarSearch Form -->
        <div class="form-inline">
          <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
              </button>
            </div>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
            <li class="nav-item ">
              <a href="#" class = "nav-link d-flex justify-content-between" id="call-online-state" data-state="0">
              <div class = "d-flex">
                <i class="fa fa-circle-o text-green mt-1 ml-1"></i> <p class = "m-0 ml-2">Online</p>
              </div>
                <p class="pull-right-container">
                  <small class="d-flex mb-2 justify-content-center label pull-right bg-green" style="border-radius: 10px;width: 56px;height: 21px;">Abierto</small>
                </p>
              </a>

            </li>
            <li class="nav-item ">
              <a href="{{ asset('admon/home') }}" id = "principal" class="nav-link">
                <i class="nav-icon fas fa-desktop"></i>
                <p>
                  Principal
                </p>
              </a>

            </li>
            <li class="nav-item">
              <a href="{{ asset('admon/mesas') }}" id = "mesas" class="nav-link">
                <i class="nav-icon fas fa-shopping-bag"></i>
                <p>
                  Mesas
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>



            <li class="nav-item">
              <a href="#" class="nav-link" id = "pedidos">
                <i class="nav-icon fas fa-shopping-bag" ></i>
                <p>
                  Pedidos
                  <p id = "number_pedidos"></p>
                  <i class="fas fa-angle-left right"></i>
                  <!-- <span class="badge badge-info right">6</span> -->
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ asset('admon/pedidos/domicilio') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "pedidos_domicilio"></i>
                    <p>Pedidos domicilio
                      <p id = "number_pedidos_domicilio"></p>
                    </p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ asset('admon/pedidos/mesa') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "pedidos_mesa"></i>
                    <p>Pedidos mesa   <p id = "number_pedidos_mesa"></p>
                  </p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ asset('admon/pedidos/terminados') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "pedidos_terminados"></i>
                    <p>Pedidos terminados</p>
                  </a>
                </li>

              </ul>
            </li>
            <li class="nav-item">
              <a href="{{ asset('admon/Banners') }}" id = "banners" class="nav-link">
                <i class="nav-icon fas fa-align-justify"></i>
                <p>
                  Banners
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" id = "catalogos">
                <i class="nav-icon fas fa-edit" ></i>
                <p>
                  Catálogos
                  <i class="fas fa-angle-left right"></i>
                  <!-- <span class="badge badge-info right">6</span> -->
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ asset('admon/categorias') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "categorias"></i>
                    <p>Categorías</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ asset('admon/productos') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "productos"></i>
                    <p>Productos</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ asset('admon/Adicionales') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "adicionales"></i>
                    <p>Adicionales</p>
                  </a>
                </li>

              </ul>
            </li>
            <li class="nav-item">
              <a href="{{ asset('admon/clientes') }}" id = "clientes" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                  Clientes
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" id = "Terminos">
                <i class="nav-icon fas fa-shopping-bag" ></i>
                <p>
                  Inventarios
                  <p id = "number_pedidos"></p>
                  <i class="fas fa-angle-left right"></i>
                  <!-- <span class="badge badge-info right">6</span> -->
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ asset('admon/inventario/proveedores') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "proovedores"></i>
                    <p>Proveedores</p>
                  </a>
                </li>
              </ul>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ asset('admon/inventario/productos') }}" class="nav-link">
                    <i class="far fa-circle nav-icon" id = "productos_inventario"></i>
                    <p>Productos</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="{{ asset('admon/cupones') }}" id = "cupones" class="nav-link">
                <i class="nav-icon fas fa-tags"></i>
                <p>
                  Cupones
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="http://3.238.228.135:3000" id = "feedback" class="nav-link">
                <i class="nav-icon fas fa-thumbs-up"></i>
                <p>
                  Feedback
                  <!-- <span class="right badge badge-danger">New</span> -->
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" id = "config">
                <i class="nav-icon fas fa-cog"></i>
                <p>
                  Configuración
                  <i class="fas fa-angle-left right"></i>
                  <!-- <span class="badge badge-info right">6</span> -->
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ asset('admon/config/inicial') }}" class="nav-link">
                    <i class="fas fa-clock-o nav-icon" id = "config-inicial"></i>
                    <p>Configuración inicial</p>
                  </a>
                </li>
                <!-- <li class="nav-item">
                  <a href="/admon/config/cobertura" class="nav-link">
                    <i class="fas fa-street-view nav-icon" id = "admon-cober"></i>
                    <p>Cobertura</p>
                  </a>
                </li> -->
                <li class="nav-item">
                  <a href="{{ asset('admon/config/usuarios') }}" class="nav-link">
                    <i class="fas fa-user nav-icon" id = "users_config"></i>
                    <p>Usuarios </p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ asset('admon/config/cuenta') }}" class="nav-link">
                    <i class="nav-icon fas fa-cog" id = "cuenta"></i>
                    <p>Cuenta</p>
                  </a>
                </li>

              </ul>
            </li>

          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">

      </section>

      <!-- Main content -->
      <section class="content">
        <div id="app">
          <main>
            @yield('content')
          </main>
          <div id = "NotificationAdmon">
          </div>
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.1.0
    </div>
    <strong>Copyright &copy; 2021 <a href="https://oneturpial.com">OneTurpial</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<style>
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
.circle_pedidos{
  background: #ca2e2e;

border-radius: 100%;
padding: 3px 8px;
margin-left: 10px !important;
font-weight: bold;
font-size: 13px;
}
.img{
  font-size: 90px;
}
.file{
  display: none;
}
.dropdown-toggle::after{
  border:none !important;
  font-size:0px !important;
}
.sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
    background-color: #e09900 !important;
    color: #fff !important;
}
.active-icon{
  color:#e09900 !important;
}
.title-film-admon{
    font-weight: bolder;
    color: black;
    width: 133px;
    text-align: center;
  }
.imagen-admon-list{
    width: 100px;
  }
  .mobile-disabled{
    display: block;
  }
  .mobile-enabled{
    display: none;
  }
  @media only screen and (max-width:425px) {
  .mobile-enabled{
    display: block;
  }
  .mobile-disabled{
    display: none;
  }
  .mobile-disabled-table{
    display: none;
  }
  .imagen-admon-list{
    width: 90px;
  }

}
</style>

<script src="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/js/adminlte.min.js"></script>
</body>

</html>
