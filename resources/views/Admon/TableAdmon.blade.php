@extends('layouts.BaseAdmon')

@section('title','Home')

@section('content')


<div id = "Tables">

</div>

<script>
$(document).ready(function(){
$('#mesas').addClass('active');
})
</script>
@if($cookie != '' && $cookie != null)
<script>
$('#nombre_admon').html('<img src="http://pidomi.com/demo/admin-app//template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"><p >Usuario</p>');
$('#nombre_admon_grande').html('<img src="http://pidomi.com/demo/admin-app//template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"><span class="hidden-xs">Usuario</span>');
$('#nav_mesas').addClass('disapierd');
$('#nav_pedidos').addClass('disapierd');
$('#nav_banners').addClass('disapierd');
$('#nav_proovedores').addClass('disapierd');
$('#nav_catalogos').addClass('disapierd');
$('#nav_clientes').addClass('disapierd');
$('#nav_inventarios').addClass('disapierd');
$('#nav_cupones').addClass('disapierd');
$('#nav_feedback').addClass('disapierd');
$('#nav_configuracion').addClass('disapierd');
</script>

  @if(str_contains(json_decode($cookie)->accesos,'mesas'))
  <script>
  $('#nav_mesas').removeClass('disapierd');
  </script>
  @endif

  @if(str_contains(json_decode($cookie)->accesos,'pedidos'))
  <script>
  $('#nav_pedidos').removeClass('disapierd');
  </script>
  @endif

  @if(str_contains(json_decode($cookie)->accesos,'banners'))
  <script>
  $('#nav_banners').removeClass('disapierd');
  </script>
  @endif

  @if(str_contains(json_decode($cookie)->accesos,'catalogos'))
  <script>
  $('#nav_catalogos').removeClass('disapierd');
  </script>
  @endif

  @if(str_contains(json_decode($cookie)->accesos,'clientes'))
  <script>
  $('#nav_clientes').removeClass('disapierd');
  </script>
  @endif

  @if(str_contains(json_decode($cookie)->accesos,'inventario'))
  <script>
  $('#nav_inventarios').removeClass('disapierd');
  </script>
  @endif

  @if(str_contains(json_decode($cookie)->accesos,'cupones'))
  <script>
  $('#nav_cupones').removeClass('disapierd');
  </script>
  @endif

@endif



@endsection
