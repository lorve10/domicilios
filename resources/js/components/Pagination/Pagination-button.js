import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class PaginationButton extends Component{
  constructor(props){
    super(props);
    this.state={
      postsPerPage:this.props.postsPerPage,
      totalData:this.props.totalData,
      nButton:[],

      page:this.props.currentPage,
    }
  }
  componentDidMount(){
    this.paginationButton()
  }
  UNSAFE_componentWillReceiveProps(nextProps){
    this.setState({page:nextProps.currentPage,totalData:nextProps.totalData})
    this.paginationButton()
  }
  paginationButton(){
    const {postsPerPage,totalData} = this.state
    // console.log("Total: "+totalData)
    const contador = [];
    // console.log(postsPerPage)
    // console.log(totalData)
    for (var i = 1; i <= Math.ceil(totalData/postsPerPage); i++) {
      contador.push(i)
    }
    this.setState({nButton:contador})
    // console.log(this.state.nButton)
  }
  updatePage(data){
    this.setState({page:data})
    this.props.updateCurrentPage(data)
  }
  next(){
    const {page} = this.state

    var nPage = page + 1
    this.setState({page:nPage})
    this.props.updateCurrentPage(nPage)

  }
  prev(){
    const {page} = this.state
    var nPage = page - 1
    this.setState({page:nPage})
    this.props.updateCurrentPage(nPage)
    // console.log(nPage)
  }
  render(){
    const pagina = this.state.page
    return(
      <div>

        <nav aria-label="Page navigation example">
          <ul className="pagination">
            <li className={this.state.page<=1 ? 'page-item disabled':'page-item cursor-pointer'}>
              <a className="page-link button-pagination" aria-label="Previous" onClick={()=>this.prev()}>
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            {this.state.nButton.map((data,index)=>{
              if((pagina+5)>data && (pagina-5)<data){
                return(
                  <li onClick={()=>this.updatePage(data)} className="page-item cursor-pointer" key={index}><a className={this.state.page==data ? 'page-link button-pagination button-pagination-select':'page-link button-pagination'}>{data}</a></li>
                )
              }
            })
            }
            <li className={this.state.page< Math.ceil(this.state.totalData/this.state.postsPerPage) ? 'page-item cursor-pointer':'page-item disabled'}>
              <a className="page-link button-pagination" aria-label="Next" onClick={()=>this.next()}>
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>

      </div>
    )
  }
}
