import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from './ApiRest';
import Swal from 'sweetalert2'
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import $ from 'jquery'
function NotificationAdmon() {
  const [user, setUser] = useState('')
  const [pass, setPass] = useState('')
  const [loading, setLoading] = useState(false)
  const audio = new Audio(ip+"app-assets/audio/audio.mp3")

  useEffect(() => {
    console.log("entrooo a notificaciones");
    setTimeout(function(){
      obtainNotifications()
      setInterval(function(){ obtainNotifications()}, 30000);

    }, 2000);



  }, []);
  const obtainNotifications = async () => {
    axios.post(ip+'admon/obtain/notifications').then(response=>{

      var res = response;


      if (response.data.data.length == 0){
        window.document.getElementById('number_pedidos').innerHTML = ''
        $('#number_pedidos').removeClass('circle_pedidos');
        window.document.getElementById('number_pedidos_domicilio').innerHTML = ''
        $('#number_pedidos_domicilio').removeClass('circle_pedidos');
        window.document.getElementById('number_pedidos_mesa').innerHTML = ''
        $('#number_pedidos_mesa').removeClass('circle_pedidos');
      }

      var filterdomi = res.data.data.filter(e=>e.type_message == 1)
      var filtermesa = res.data.data.filter(e=>e.type_message == 2)
      if (filterdomi.length>0) {
        NotificationManager.warning('Tienes '+filterdomi.length+' pedidos pendientes de domicilio', 'Click me!', 10000, () => {
          window.location.href = ip+"admon/pedidos/domicilio"
        });
        audio.play()
        window.document.getElementById('number_pedidos').innerHTML = res.data.data.length
        $('#number_pedidos').addClass('circle_pedidos');
        window.document.getElementById('number_pedidos_domicilio').innerHTML = filterdomi.length
        $('#number_pedidos_domicilio').addClass('circle_pedidos');

      }
      if (filtermesa.length>0) {
        NotificationManager.info('Tienes '+filtermesa.length+' pedidos pendientes en tus mesas ', 'Click me!', 10000, () => {
          window.location.href = ip+"admon/pedidos/mesa"
        });
        audio.play()
        window.document.getElementById('number_pedidos').innerHTML = res.data.data.length
        $('#number_pedidos').addClass('circle_pedidos');
        window.document.getElementById('number_pedidos_mesa').innerHTML = filtermesa.length
        $('#number_pedidos_mesa').addClass('circle_pedidos');
      }
    })
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  return (
    <div>
      <NotificationContainer/>
    </div>
  );
}

export default NotificationAdmon;

if (document.getElementById('NotificationAdmon')) {
  ReactDOM.render(<NotificationAdmon />, document.getElementById('NotificationAdmon'));
}
