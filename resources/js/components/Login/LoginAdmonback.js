import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../css/app.scss'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2'
function LoginAdmon() {
  const [user, setUser] = useState('')
  const [pass, setPass] = useState('')
  const [loading, setLoading] = useState(false)

  const Login = async () => {
    if (user != '' && pass != '') {
      setLoading(true)
      const data = new FormData()
      data.append('email', user)
      data.append('password', pass)
      axios.post(ip+'admon/login_administrador',data).then(response=>{
        window.location.href = ip+"admon/home"
      })
      .catch(error=>{
        setLoading(false)
        MessageError("El usuario o la contraseña son incorrectos")

      })

    }
    else{
      MessageError("El usuario y la contraseña no pueden estar vacios")
    }

  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  return (


    <div className="wrapper-log">
      <div className="login">
        <p className="title">Iniciar Sesión</p>
        <input appnoautocomplete="" onChange = {(e)=>setUser(e.target.value)} type="text" placeholder="Usuario" autoFocus/>
        <i className="fa fa-user"></i>
        <input appnoautocomplete="" onChange = {(e)=>setPass(e.target.value)} type="password" placeholder="Contraseña" />
        <i className="fa fa-key"></i>
        <button onClick = {()=>Login()}>
          {
            loading ?
            <div class="spinner-border text-light" role="status">
              <span class="sr-only">Loading...</span>
            </div>

            :<span className="state">Ingresar</span>
        }
      </button>
    </div>
  </div>

);
}

export default LoginAdmon;

if (document.getElementById('LoginAdmon')) {
  ReactDOM.render(<LoginAdmon />, document.getElementById('LoginAdmon'));
}
