import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../css/app.scss'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2'
function LoginAdmon() {
  const [user, setUser] = useState('')
  const [pass, setPass] = useState('')
  const [loading, setLoading] = useState(false)

  const Login = async () => {
    if (user != '' && pass != '') {
      setLoading(true)
      const data = new FormData()
      data.append('email', user)
      data.append('password', pass)
      axios.post(ip+'login_administrador',data).then(response=>{
        window.location.href = ip+"admon/home"
      })
      .catch(error=>{
        setLoading(false)
        MessageError("El usuario o la contraseña son incorrectos")

      })

    }
    else{
      MessageError("El usuario y la contraseña no pueden estar vacios")
    }

  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  return (
    <div>
      <div className="col-md-12 justify-content-center d-flex" style={{background:'#D9D9D9'}} >

          <img src={ip+'app-assets/images/Oneturpial.png'} class="img"/>


      </div>
    <div className="container" style={{marginRight:0}}>
        <div className="d-flex mt-3">
            <div className=" mb-3 col-3" >
                <img src={ip+'app-assets/images/plato.png'} style={{height: 125, marginTop:35}}/>
            </div>
            <div class="verticalLine d-flex"> </div>
            <div className="separador  col-6">
                    <div className="mt-5">
                          <input type="text" className="form-control mt-3" placeholder="Usuario" value={user} onChange={(event)=>setUser(event.target.value)} style={{borderRadius:30, backgroundColor:'#f1f1f1', border: 'aqua'}} />
                    </div>
                    <div className=" ">
                          <input type="password" className="form-control mt-3" placeholder="Contraseña" value={pass} onChange={(event)=>setPass(event.target.value)}  style={{borderRadius:30, backgroundColor:'#f1f1f1', border: 'aqua'}} />
                    </div>
                    <div className="my-2 d-flex col-md-9 justify-content-end px-0 mt-5" style = {{marginLeft:120}}>
                      <button className="btn btn-success" style={{border:'1px solid #FFDE59', backgroundColor:'#FFDE59', color:'black', borderRadius:30}} onClick = {()=>Login()}>Iniciar sesión</button>
                    </div>
            </div>

        </div>
    </div>
    </div>
);
}

export default LoginAdmon;

if (document.getElementById('LoginAdmon')) {
  ReactDOM.render(<LoginAdmon />, document.getElementById('LoginAdmon'));
}
