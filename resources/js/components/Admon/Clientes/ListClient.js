import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.scss'
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import L from 'leaflet';
import Map from './Map';

function ListClient() {
  const [lat, setLat] = useState('')
  const [lng, setLng] = useState('')
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [lisUser, setLisUser] = useState([])
  const [dataModal, setDataModal] = useState([])
  const [usuarioBack , setUsuarioBack]=useState([])
  const [userBack , setUserBack]=useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(5)
  const [id, setId] = useState(null)
  const [text, setText] = useState('')
  const [log, setLog]= useState()

  useEffect(()=>{
    obtain_clients()
  },[]);
  const updateCurrentPage = async (number) => {
    console.log("esta haciendo locuras");
    await setLisUser([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(usuarioBack,number,postsPerPage)
    await setLisUser(dataNew)
  }

  const obtain_clients = async () => {
    axios.get(ip+'admon/obtain_clients').then(response=>{
      console.log(response.data);
      setUsuarioBack(response.data);
      setUserBack(response.data);
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setLisUser(dataNew)

    })
  }


  const searchInput = async (value) => {
    setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = userBack.filter(function(item2){
      var name = ((item2.user_name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }
      return res;
    })
    await setUsuarioBack(newData)
    await setCurrentPage(1)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setLisUser(dataNew)
    await setLoading(false)
  }



  const deshabilitar = async (value) => {
    var message = '¿Está seguro que quiere eliminar este cliente?'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        axios.post(ip+'admon/delete_clients',data).then(response=>{
          obtain_clients()
          Swal.fire('Eliminado correctamente!', '', 'success')

        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }
  const clickLog = async (value) =>{
    setDataModal(value)
    setShowForm(2)
  }
  const goback = () => {
    setShowForm(1)
  }


  return (
    <div>
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
            <h4 className="form-section d-flex align-items-center"><span class="material-icons mr-2">account_circle</span> Clientes </h4>
            </div>
            <div className = "card-body">


            {showForm == 1 &&
           <div>
            <table className="table table-striped">
              <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>telefono</th>
                    <th>deshabilitar</th>
                    <th>mas Info</th>

                </tr>
                {
                  lisUser.map((item)=>{
                    return (
                      <tr>
                        <td>{item.name_user}</td>
                        <td>{item.email}</td>
                        <td>{item.celphone_user}</td>
                        <td>
                          <div className = "d-flex">
                            <button onClick = {()=>deshabilitar(item.id_usuario)} className="btn btn-danger"><i class = "material-icons">delete</i></button>
                          </div>
                        </td>
                        <td><span class="material-icons" onClick = {()=>clickLog(item)}>visibility</span></td>
                      </tr>
                    );
                  })

                }

              </thead>
              <tbody>
              </tbody>
            </table>



            <div className="d-flex col-md-12 col-12 justify-content-end">
              <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={usuarioBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
            </div>

            </div>
          }

            </div>
            {
              showForm == 2 ?
              <Map data={dataModal}  goback={()=>goback()}/>
              :null
            }
          </div>


        </div>



    }
    </div>
);
}

export default ListClient;

if (document.getElementById('ListClient')) {
  ReactDOM.render(<ListClient />, document.getElementById('ListClient'));
}
