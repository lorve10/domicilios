import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../../css/app.scss'
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import L from 'leaflet';

function Map(props) {
  const [loading, setLoading] = useState(false)

useEffect(()=>{
  setMapa(props.data)
},[])

  const setMapa = async (data) => {
    // console.log("entro aqui al mapa madre santa");
    // console.log(data);
    // // var container = L.DomUtil.get('map2');
    // // if(container != null){
    // //   container._leaflet_id = null;
    // // }
    // var map = L.map('map2').setView([data.lotitud, data.longitud], 16);
    //
    // L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    //   attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
    // }).addTo(map);
    // var pump = null
    // if (pump != null) {
    //   map.removeLayer(pump);
    // }
    //
    //   pump = L.marker([data.lotitud, data.longitud]).addTo(map);
    //   var circle = L.circle([data.lotitud, data.longitud], {
    //     color: 'red',
    //     fillColor: '#f03',
    //     fillOpacity: 0.5,
    //   }).addTo(map);
    //   circle.bindPopup("Soy la cobertura "+data.name_user );
    // // const provider = new OpenStreetMapProvider();
    // // const searchControl = new GeoSearchControl({
    // //   provider: provider,
    // //   showMarker: false,
    // //   searchLabel: 'Busque la dirección en orden Ciudad, dirección y seleccione'
    // // });

  }

  return (
    <div className= {{width:'100%'}}>
      {
        !loading &&
            <div className = "card-body">
            {!loading &&
            <div>
            <table className="table table-striped">
              <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>telefono</th>
                    <th>dirección</th>
                </tr>
                <tr>
                  <td>{props.data.name_user}</td>
                  <td>{props.data.email}</td>
                  <td>{props.data.celphone_user}</td>
                  <td>{props.data.direc_user}</td>

                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          {/*  <div className= "mt-2">
            <h3 className = "title-film-admon"> Ubicación</h3>
            <div className = "col-md-6 pt-3">
              <div id = "map2" style = {{height:500, maxHeight:500}}>
              </div>
            </div>
            </div>*/}

            <div className="my-1 d-flex col-md-2 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-warning mr-2" onClick = {()=>props.goback()} >Regresar</button>
            </div>

          </div>
          }
        </div>
      }
    </div>
);
}

export default  Map;

if (document.getElementById('Map')) {
  ReactDOM.render(<Map />, document.getElementById('Map'));
}
