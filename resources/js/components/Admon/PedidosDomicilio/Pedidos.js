import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
// import New_categories from './New-categorie'
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'
import moment from 'moment'
import makeAnimated from 'react-select/animated';
import Select from 'react-select'
import ListProd from './ListProd'
function PedidosAdmon() {
  const [loading, setLoading] = useState(false)
  const [pedidos, setPedidos] = useState([])
  const [pedidosBack, setPedidosBack] = useState([])
  const [pedidosBack2, setPedidosBack2] = useState([])
  const [usuarios, setUsuarios] = useState([])
  const [productos, setProductos] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(5)
  const [paso, setPaso] = useState(1)
  const [ view, setView ] = useState(1)
  const [showForm, setShowForm] = useState(1)
  const [dataShow, setDataShow] = useState(null)
  const [usuarioSelect, setUsuarioSelect] = useState(null)
  const [id, setId] = useState(null)
  const [estadoselect, setEstadoSelect] = useState(0)
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  moment.locale('es')
  const [text, setText] = useState('')
  useEffect(() => {

    obtianPedidos(paso,0)
    obtainProductos()
  }, []);

  const obtainProductos = async () => {
    axios.get(ip+'admon/obtain_productos').then(response=>{
      setProductos(response.data)
    })
  }
  const obtianPedidos = async (type,deleted) => {
    var dataform = new FormData()
    dataform.append('type',1)
    await axios.post(ip+'admon/domicilios/check',dataform).then(response=>{
      console.log("Notificaciones leídas");
      window.document.getElementById('number_pedidos').innerHTML = ''
      $('#number_pedidos').removeClass('circle_pedidos');
      window.document.getElementById('number_pedidos_domicilio').innerHTML = ''
      $('#number_pedidos_domicilio').removeClass('circle_pedidos');
      window.document.getElementById('number_pedidos_mesa').innerHTML = ''
      $('#number_pedidos_mesa').removeClass('circle_pedidos');
    })
    axios.post(ip+'admon/obtain_pedidos').then(response=>{
      console.log("entroooo?");
      console.log(response.data);
      var res = response.data.data
      var filterDomicilo = res.filter(e=>e.tipo_pedido == 1 && e.estado_pedido == type && e.deleted == deleted)

      setPedidosBack(filterDomicilo)
      setPedidosBack2(filterDomicilo)
      var arrayByPush = []
      response.data.usuarios.map(item => {
        const array2 = {
          value: item.user_id,
          label: item.user_name+" "+item.user_lastname,

        }
        arrayByPush.push(array2)
      })
      setUsuarios(arrayByPush)



      const dataNew = Pagination.paginate(filterDomicilo,currentPage,postsPerPage)
      setPedidos(dataNew)
      setLoading(false)


    })
  }
  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      title: 'Acción realizada',
      text: data,
      icon: 'success',
    })
  }
  const goback = async () => {
    var view = showForm
    view = view - 1
    setShowForm(view)
    obtianPedidos(paso,0)
  }
  const searchInput = async (value) => {
    setText(value)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = pedidosBack2.filter(function(item2){
      console.log(item2);
      var name = ((item2.direcc_pedido).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var celular = (item2.tel_pedido+'')
      var nameuser = ((item2.name_user).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var resCelular = celular.indexOf(inputSearch) > -1
      var resCelularLasT = celular.indexOf(inputSearch) > -1
      var resNameUser = nameuser.indexOf(inputSearch) > -1
      var resNameUserLasT = nameuser.indexOf(inputSearch) > -1
      var res = false
      if(resName||resNameLasT){
        res = true
      }
      if(resCelular||resCelularLasT){
        res = true
      }
      if(resNameUser||resNameUserLasT){
        res = true
      }
      return res;
    })
    await setPedidosBack(newData)
    await setPedidos(newData)
  }
  const deshabilitar = async (value) => {
    Swal.fire({
      title: '¿Quieres eliminar esta categoría? Pueden existir películas o series de esta misma.',
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value)
        axios.post(ip+'admon/delete_categorie',data).then(response=>{
          obtaincategories()
          Swal.fire('Eliminado correctamente!', '', 'success')
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  const updateCurrentPage = async (number) => {
    await setPedidos([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(pedidosBack,number,postsPerPage)
    await setPedidos(dataNew)
  }

  const viewModalPedido = async (data) => {
    console.log(data);
    await setDataShow(data)
    $('#modalViewPedido').modal('show')

  }

  const viewModalPrint = async (data) => {
    console.log(data);
    await setDataShow(data)
    $('#modalViewPrint').modal('show')
  }

  const convertMoney = (amount) => {

    try {
      var thousands = "."
      var decimalCount = 0
      var decimal = "."
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? "-" : "";

      let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
      let j = (i.length > 3) ? i.length % 3 : 0;

      return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
      console.log("Error de moneda "+e)
      return null
    }
  }

  const viewModalConfig = async (data) => {
    console.log(data);
    await setId(data.id_pedido)
    $('#modalConfigPedido').modal('show')
    var databack = usuarios.filter(e=>e.value == data.domiciliario_id)
    if (databack.length>0) {
      setUsuarioSelect(databack[0])
    }

  }



  const changepedido = async (data) => {
    if (estadoselect == 0) {
      MessageError("Selecciona un estado del pedido")
    }
    else if ((estadoselect == 3 || estadoselect == 4) && usuarioSelect == null)  {
      MessageError("Selecciona un domiciliario")
    }
    else{
      console.log(usuarioSelect);
      var dataform = new FormData()
      dataform.append('value',data)
      dataform.append('id',id)
      dataform.append('domicilio',usuarioSelect == null ? '':usuarioSelect.value)
      axios.post(ip+'admon/edit_state_pedido',dataform).then(response=>{
        $('#modalConfigPedido').modal('hide')
        setCurrentPage(1)
        MessageSuccess('Se cambió el estado correctamente del pedido '+id)
        if (paso != 5) {
          obtianPedidos(paso,0)
        }
        else{
          changeStateEliminados()
        }

        console.log(response)
      })
    }
  }
  const changepedidoafter = async (data) => {
    await setEstadoSelect(data)
  }
  const deletepedido = async (data) => {
    Swal.fire({
      title: '¿Quieres eliminar este pedido?',
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        var dataform = new FormData()
        dataform.append('id',data.id_pedido)
        axios.post(ip+'admon/delete_pedido',dataform).then(response=>{
          setCurrentPage(1)
          MessageSuccess('Se eliminó el pedido '+data.id_pedido)
          if (paso != 4) {
            obtianPedidos(paso,0)
          }
          else{
            changeStateEliminados()
          }
          console.log(response)
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })

  }
  const changeState = async(data) => {
    setLoading(true)
    setPaso(data)
    setCurrentPage(1)
    obtianPedidos(data,0)

  }
  const changeStateEliminados = async() => {
    setPaso(5)
    var dataform = new FormData()
    dataform.append('type',1)
    await axios.post(ip+'admon/domicilios/check',dataform).then(response=>{
      console.log("Notificaciones leídas");
      window.document.getElementById('number_pedidos').innerHTML = ''
      $('#number_pedidos').removeClass('circle_pedidos');
      window.document.getElementById('number_pedidos_domicilio').innerHTML = ''
      $('#number_pedidos_domicilio').removeClass('circle_pedidos');
      window.document.getElementById('number_pedidos_mesa').innerHTML = ''
      $('#number_pedidos_mesa').removeClass('circle_pedidos');
    })
    axios.post(ip+'admon/obtain_pedidos').then(response=>{
      console.log("entroooo?");
      console.log(response.data);
      var res = response.data.data
      var filterDomicilo = res.filter(e=>e.tipo_pedido == 1 && e.deleted == 1)

      setPedidosBack(filterDomicilo)
      setPedidosBack2(filterDomicilo)


      const dataNew = Pagination.paginate(filterDomicilo,currentPage,postsPerPage)
      setPedidos(dataNew)
      setLoading(false)


    })
  }

  const totaladicc = (data) => {
    console.log("entroooo a data por mi");
    console.log(data);
    var total = 0
    data.map(item=>{
      total = total + item.adicional.price_add
    })
    console.log(total);
    return total;
  }

  const print = async () => {
    var mywindow = window.open('', 'PRINT', 'height="fit-content",width="fit-content"');

     mywindow.document.write('<html><head><link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">');
     mywindow.document.write('</head><body>');
     mywindow.document.write(document.getElementById('thisprinted').innerHTML);
     mywindow.document.write('<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script></body></html>');

     mywindow.document.close(); // necessary for IE >= 10
     mywindow.focus(); // necessary for IE >= 10*/
     setTimeout(function(){  mywindow.print(); }, 2000);


     // mywindow.close();

  }
  return (
    <div id='app'>
      <div className="" style={{minHeight:600}}>

        <div className="modal fade bd-example-modal-lg" id = "modalViewPedido" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-lg">

            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Detalles del pedido</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                    </button>
              </div>
              <div className="modal-body">
                {dataShow != null &&<div className = "row pt-2">
                  <div className = "col-md-12">
                    <p><span style = {{fontWeight:'bold'}}>Dirección de entrega: </span>{dataShow.direcc_pedido}</p>
                  </div>
                  <div className = "col-md-12">
                    <p><span style = {{fontWeight:'bold'}}>Número celular: </span>{dataShow.tel_pedido}</p>
                  </div>
                </div>}
                <div className = "row pt-2">
                  <div className = "col-md-2" style = {{fontWeight:'bold'}}>Nombre y precio y adicionales</div>
                  <div className = "col-md-2" style = {{fontWeight:'bold'}}>Comentario</div>
                  <div className = "col-md-2"style = {{fontWeight:'bold'}} >Cantidad</div>
                  <div className = "col-md-2"style = {{fontWeight:'bold'}}>Precio</div>
                  <div className = "col-md-2"style = {{fontWeight:'bold'}}>Descuento</div>
                  <div className = "col-md-2"style = {{fontWeight:'bold'}}>Total</div>

                </div>

                  { dataShow != null &&
                    dataShow.productos.map((item,index)=>{
                      return (
                        <div className = "row pt-2">
                          <div className = "col-md-2">
                            <p className = "m-0">{item.producto.prod_nombre}</p>
                            <p className = "mt-1">Precio del producto ${convertMoney(item.producto.prod_precio-item.producto.prod_descuento)}</p>
                            <p className = "mt-1">Adicionales: {

                              item.adicionales.map(item2=>{
                                return(
                                  <span> + {item2.adicional.name_add} </span>
                                )
                              })
                            }</p>

                          <p className = "mt-1">Precio de los adicionales ${convertMoney(totaladicc(item.adicionales))}</p>

                          </div>
                          <div className = "col-md-2">
                            {
                              item.comentario == '' || item.comentario == null ?
                              <p className = "mt-1">No tiene comentario</p>
                              :<p className = "mt-1">{item.comentario}</p>
                            }

                          </div>
                          <div className = "col-md-2">
                            <p className = "m-0">{item.cantidad}</p>
                          </div>
                          <div className = "col-md-2">
                            <p className = "m-0">${convertMoney(item.valot_t)}</p>
                          </div>
                          <div className = "col-md-2">${convertMoney(item.descuento)}</div>
                          <div className = "col-md-2">${convertMoney(item.valot_t-item.descuento)}</div>
                          <hr/>
                        </div>

                      );
                    })
                  }
                  {dataShow != null &&<div className = "row pt-2">
                    <div className = "col-md-12">
                      <p><span style = {{fontWeight:'bold'}}>Total: </span>{convertMoney(dataShow.valor_total)}</p>
                    </div>
                    <div className = "col-md-12">
                      <p><span style = {{fontWeight:'bold'}}>Descuento: </span>{dataShow.total_descuento}</p>
                    </div>
                    <div className = "col-md-12">
                      <p><span style = {{fontWeight:'bold'}}>Total a pagar: </span>{dataShow.valor_total-dataShow.total_descuento}</p>
                    </div>
                    <div className = "col-md-12">
                      <p><span style = {{fontWeight:'bold'}}>Método de pago: </span>{dataShow.metodo_pago == 1 ? 'Efectivo':'Datafono'}</p>
                    </div>


                  </div>}
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade bd-example-modal-lg" id = "modalConfigPedido" tabindex="-1" role="dialog">
          <div className="modal-dialog  modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Cambiar de estado del pedido {id}</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className = "row">
                  <div className = "col-md-3 ">

                    <button type="button" onClick={()=>changepedidoafter(1)} class={estadoselect == 1 ? 'btn btn-success d-flex align-items-center':'btn btn-info d-flex align-items-center'} style = {{color:'white'}}><span class="material-icons">
                      pending_actions
                    </span>Pendiente</button>
                  </div>
                  <div className = "col-md-3 ">

                    <button type="button" onClick={()=>changepedidoafter(2)}  class={estadoselect == 2 ? 'btn btn-success d-flex align-items-center':'btn btn-info d-flex align-items-center'}  style = {{color:'white'}}><span class="material-icons">
                      outdoor_grill
                    </span>Preparación</button>
                  </div>
                  <div className = "col-md-3 ">

                    <button type="button" onClick={()=>changepedidoafter(3)}  class={estadoselect == 3 ? 'btn btn-success d-flex align-items-center':'btn btn-info d-flex align-items-center'}  style = {{color:'white'}}><span class="material-icons">
                      delivery_dining
                    </span>En camino</button>
                  </div>
                  <div className = "col-md-3 ">

                    <button type="button" onClick={()=>changepedidoafter(4)}  class={estadoselect == 4 ? 'btn btn-success d-flex align-items-center':'btn btn-info d-flex align-items-center'} style = {{color:'white'}}><span class="material-icons">
                      delivery_dining
                    </span>Entregado</button>
                  </div>

                </div>
                {(estadoselect == 3 || estadoselect == 4) && <div className="row pt-4">
                  <div className="col-md-6">
                    <label>Seleccione el domiciliario</label>
                    <Select
                      value={usuarioSelect}
                      closeMenuOnSelect={true}
                      components={animatedComponents}
                      options={usuarios}
                      onChange={(e) => setUsuarioSelect(e)}
                      placeholder="Seleccione el domiciliario"
                      name="selectUsers"
                      />
                  </div>
                </div>}
                <div className="row pt-4 justify-content-end">
                  <div className="col-md-6 d-flex justify-content-end">
                    <button className="btn btn-success" onClick = {()=>changepedido(estadoselect)}>Guardar Cambios</button>
                  </div>
                </div>


              </div>

            </div>
          </div>
        </div>

        <div className="modal fade" id = "modalViewPrint" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div className="modal-dialog">

            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Impresión del pedido</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className = "row justify-content-end px-4">
                  <span className = "material-icons" onClick = {()=>print()} style = {{fontSize:28, cursor:'pointer'}}>print</span>
                </div>

                {dataShow != null &&<div className = "row pt-2 pl-2">

                  <div id = "thisprinted" style = {{width:155, maxWidth:155}}>
                    <div className = "row">
                      <img className = "pl-3" src = {ip+'app-assets/logo_ticket.png'} style = {{width:110}}/>
                      <div className = "col-md-12 pl-3">

                        <p className = "mb-1" style = {{fontSize:13, fontFamily:'Times New Roman'}}>Factura N° </p>
                        <p className = "mb-1" style = {{fontSize:13, fontFamily:'Times New Roman'}}>00{dataShow.id_pedido} </p>
                        <p className = "mb-1" style = {{fontSize:13, fontFamily:'Times New Roman'}}>{moment().format('DD/MM/YYYY')}</p>
                        <p className = "mb-1" style = {{fontSize:13, fontFamily:'Times New Roman'}}>Hora: {moment().format('HH:mm:ss')}</p>
                        <p className = "mb-1" style = {{fontSize:13, fontFamily:'Times New Roman'}}>Nombre: {dataShow.name_user}</p>
                        <p className = "mb-1" style = {{fontSize:13, fontFamily:'Times New Roman'}}>Dirección: {dataShow.direcc_pedido} </p>
                        <p className = "mb-1" style = {{fontSize:13, fontFamily:'Times New Roman'}}>Célular: {dataShow.tel_pedido} </p>
                        <div style = {{borderBottom:'1px solid black', width:155, maxWidth:155}}></div>

                          <div className = "row pt-2" style = {{width:155, maxWidth:155}}>
                            <div className = "col-md-3 col-3" style = {{fontWeight:'bold'}} >Cant</div>
                            <div className = "col-md-5 col-5" style = {{fontWeight:'bold'}}>Prod</div>
                            <div className = "col-md-3 col-3" style = {{fontWeight:'bold'}}>Total</div>

                          </div>

                            { dataShow != null &&
                              dataShow.productos.map((item,index)=>{
                                return (
                                  <div className = "row pt-2" style = {{width:155, maxWidth:155}}>
                                    <div className = "col-md-1 col-1">
                                      <p className = "m-0" style = {{fontSize:12, fontFamily:'Times New Roman'}}>{item.cantidad}</p>
                                    </div>
                                    <div className = "col-md-6 col-6">
                                      <p className = "m-0" style = {{fontSize:12, fontFamily:'Times New Roman'}}>{item.producto.prod_nombre}
                                        {
                                          item.adicionales.length>0 &&
                                          <span>
                                            {  item.adicionales.map(item2=>{
                                                return(
                                                  <span> + {item2.adicional.name_add} </span>
                                                )
                                              })}
                                          </span>
                                        }

                                      </p>


                                    </div>


                                    <div className = "col-md-3 col-3">
                                      <p className = "m-0" style = {{fontSize:12, fontFamily:'Times New Roman'}}>${convertMoney((item.producto.prod_precio-item.producto.prod_descuento)*item.cantidad)}</p>
                                      {
                                        item.adicionales.length>0 &&
                                        <span>
                                          {  item.adicionales.map(item2=>{
                                              return(
                                                <span style = {{fontSize:12, fontFamily:'Times New Roman'}}> + {(item2.adicional.price_add*item.cantidad)} </span>
                                              )
                                            })}
                                        </span>
                                      }
                                    </div>


                                  </div>

                                );
                              })
                            }

                            <div style = {{borderBottom:'1px solid black', width:155, maxWidth:155}}></div>
                              {dataShow != null &&<div className = "row pt-2" style = {{width:155, maxWidth:155}}>
                                <div className = "col-md-12">
                                  <p style = {{fontSize:13, fontFamily:'Times New Roman'}}><span style = {{fontWeight:'bold', fontSize:13, fontFamily:'Times New Roman'}}>Total: </span>{convertMoney(dataShow.valor_total)}</p>
                                </div>
                                <div className = "col-md-12">
                                  <p style = {{fontSize:13, fontFamily:'Times New Roman'}}><span style = {{fontWeight:'bold', fontSize:13, fontFamily:'Times New Roman'}}>Descuento: </span>{dataShow.total_descuento}</p>
                                </div>
                                <div className = "col-md-12">
                                  <p style = {{fontSize:13, fontFamily:'Times New Roman'}}><span style = {{fontWeight:'bold', fontSize:13, fontFamily:'Times New Roman'}}>Total a pagar: </span>{dataShow.valor_total-dataShow.total_descuento}</p>
                                </div>
                                <div className = "col-md-12">
                                  <p style = {{fontSize:13, fontFamily:'Times New Roman'}}><span style = {{fontWeight:'bold', fontSize:13, fontFamily:'Times New Roman'}}>Método de pago: </span>{dataShow.metodo_pago == 1 ? 'Efectivo':'Datafono'}</p>
                                </div>


                              </div>}


                      </div>
                    </div>
                  </div>


                </div>}
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="ModalNewPedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo Pedido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Realizar</button>
              </div>
            </div>
          </div>
        </div>



        <div className="card" style={{borderRadius:10}}>
          {
            showForm == 1 ?
            <div>

              <div className="card-header">
                <div className = "d-flex justify-content-between">
                  <h4 className="form-section d-flex align-items-center d-flex align-items-center"><i className="nav-icon fas fa-shopping-bag" style = {{marginRight:4}}></i> <span className= "mt-2">Listado de Pedidos</span></h4>
                  <div>
                    <button type="button" class="btn btn-primary" onClick = {()=>setShowForm(2)}>Agregar nuevo pedido</button>

                  </div>
                </div>
                <h1> Pedidos a domicilio </h1>
              </div>
              <div className="card-body card-dashboard">
                {/*<div className="row justify-content-end">
                  <button onClick = {()=>setShowForm(2)} className="btn btn-outline-success round btn-min-width mr-1 mb-1">Pedido Nuevo</button>
                </div>*/}

                <div className = "row">
                  <div className = "col-md-2 ">

                    <button type="button" onClick={()=>changeState(1)} class={paso == 1 ? "btn btn-success d-flex align-items-center":"btn btn-info d-flex align-items-center"} style = {{color:'white'}}><span class="material-icons">
                      pending_actions
                    </span>Pendiente</button>
                  </div>
                  <div className = "col-md-2 ">

                    <button type="button" onClick={()=>changeState(2)} class={paso == 2 ? "btn btn-success d-flex align-items-center":"btn btn-info d-flex align-items-center"} style = {{color:'white'}}><span class="material-icons">
                      outdoor_grill
                    </span>Preparación</button>
                  </div>

                  <div className = "col-md-2 ">

                    <button type="button" onClick={()=>changeState(3)} class={paso == 3 ? "btn btn-success d-flex align-items-center":"btn btn-info d-flex align-items-center"} style = {{color:'white'}}><span class="material-icons">
                      delivery_dining
                    </span>En camino</button>
                  </div>
                  <div className = "col-md-2 ">

                    <button type="button" onClick={()=>changeState(4)} class={paso == 4 ? "btn btn-success d-flex align-items-center":"btn btn-info d-flex align-items-center"} style = {{color:'white'}}><span class="material-icons">
                      delivery_dining
                    </span>Entregado</button>
                  </div>

                  <div className = "col-md-2 ">

                    <button type="button" onClick={()=>changeStateEliminados(5)} class={paso == 5 ? "btn btn-success d-flex align-items-center":"btn btn-info d-flex align-items-center"} style = {{color:'white'}}><span class="material-icons">                      delete
                    </span>Eliminados</button>
                  </div>

                </div>
                <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #e4e4e4', borderRadius:20, backgroundColor:'white'}}>
                  <input onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Busca el pedido (dirección, nombre, celular)"/>
                  {
                    text.length == 0 ?
                    <span style = {{color:'#c3c3c3', cursor:'pointer'}}  className= "material-icons">search</span>
                    :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
                }


              </div>
              { !loading ? <table className="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Celular</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Fecha y hora</th>
                    <th scope="col">Opciones</th>
                  </tr>
                  {
                    pedidos.map((item,index)=>{
                      return (
                        <tr>
                          <td className = "pt-3">{item.id_pedido}</td>
                          <td className = "pt-3">{item.direcc_pedido}</td>
                          <td className = "pt-3">{item.tel_pedido}</td>
                          {item.name_user != null ?
                            <td className = "pt-3">{item.name_user}</td>
                            :<td className = "pt-3">Usuario no registrado</td>
                        }

                        <td className = "pt-3">{moment(item.created_at).format('LLLL')}</td>
                        <td>
                          <div className = "d-flex">
                            <button onClick = {()=>viewModalPedido(item)}  data-toggle="tooltip" data-placement="top" title="Visualizar" className="btn btn-secondary mr-2 d-flex align-items-center py-2" style = {{backgroundColor:'#17a7f3', border:'none'}}><i className= "material-icons">visibility</i></button>
                            <button onClick = {()=>viewModalConfig(item)}  data-toggle="tooltip" data-placement="top" title="Configurar estado" className="btn btn-secondary  d-flex align-items-center py-2 mr-2" style = {{backgroundColor:'#e09900', border:'none'}}><i className="nav-icon fas fa-cog" style = {{fontSize:20}} aria-hidden="true"></i></button>
                            <button onClick = {()=>viewModalPrint(item)}  data-toggle="tooltip" data-placement="top" title="Configurar estado" className="btn btn-secondary  d-flex align-items-center py-2 mr-2"><span className = "material-icons">print</span></button>
                            {item.deleted == 0 &&
                              <button onClick ={()=>deletepedido(item)} data-toggle="tooltip" data-placement="top" title="Eliminar" className="btn btn-secondary  d-flex align-items-center py-2 " style = {{backgroundColor:'#ce0202', border:'none'}}><i className= "material-icons">delete</i></button>
                            }


                          </div>
                        </td>

                      </tr>
                    );
                  })
                }
              </thead>
              <tbody>
                {

                }
              </tbody>
            </table>
            :null
          }
          {
            pedidos.length>0&&
            <div className="d-flex col-md-12 col-12 justify-content-end">
              <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={pedidosBack.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
            </div>
          }
        </div>
      </div>
      :showForm == 2 ?
      <div>
        <ListProd productos= {productos} goback = {(data)=>goback(data)}/>
      </div>
      :null
    }

  </div>
</div>
</div>

);
}

export default PedidosAdmon;

if (document.getElementById('PedidosAdmon')) {
  ReactDOM.render(<PedidosAdmon />, document.getElementById('PedidosAdmon'));
}
