import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { ip } from '../../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import makeAnimated from 'react-select/animated';
import Select from 'react-select'
import AsyncSelect from 'react-select/async';

function New_Terceros(props) {
  const [name, setName] = useState(props.name)
  const [name2, setName2] = useState(props.name2)

  const [email, setEmail] = useState(props.email)
  const [lastname, setLastname] = useState(props.lastname)
  const [lastname2, setLastname2] = useState(props.lastname2)
  const [phone, setPhone] = useState(props.phone)
  const [adress, setAdress] = useState(props.adress)
  const [city, setCity] = useState(props.city)
  const [departament, setDepartament] = useState(props.departament)
  const [document, setDocument] = useState(props.document)
  const [typedocuments, setTypedocuments] = useState(null)
  const [typedocumentSelect, setTypedocumentSelect] = useState(null)
  const [typeuserslists, setTypeuserlists] = useState([])
  const [typeperson, setTypePerson] = useState(null)
  const [typepersonList, setTypepersonList] = useState([
    {
      value:1,
      label:'Natural'
    },
    {
      value:2,
      label:'Jurídica'
    }
  ])
  const [typeuserSelect, setTypeuserSelect] = useState({
    value:1,
    label:'Natural'
  })
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)


  useEffect(() => {
    if (props.id > 0){
      setTypeuserSelect(props.typeuser)
    }
    obtain_type_documents()

  }, []);


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',

    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  const obtain_type_documents = async () => {
    axios.get(ip + 'admon/obtain_type_documents').then(response => {
      var res = response.data
      var data = []
      var data2 = []
      var filtro = response.data.filter(e => e.id == props.typedocumentSelect)
      filtro.map(item => {
        const document = {
          value: item.id,
          label: item.nombre
        }
        data.push(document)
        setTypedocumentSelect(document)
      })


      console.log(document);
      res.map(item => {
        const array = {
          value: item.id,
          label: item.nombre,

        }
        data2.push(array)
      })
      setTypedocuments(data2)

    })
  }


  //listado de tipos de usuario



  const save_User = async () => {

    var message = ''
    var error = false
    ///validaciones///
    if (name == '' && typeuserSelect.value == 1) {
      message = "Digite el nombre"
      error = true

    }

    else if (name == '' && typeuserSelect.value == 2) {
      message = "Digite la razón social"
      error = true
    }

    else if (name2 == '' && typeuserSelect.value == 2) {
      message = "Digite el nombre representante legal"
      error = true
    }

    else if (lastname == '' && typeuserSelect.value == 1) {
      message = "Digite el apellido"
      error = true

    }
    else if (lastname == '' && typeuserSelect.value == 2) {
      message = "Digite el nombre comercial"
      error = true

    } else if (email == '') {
      message = "Digite el email"
      error = true

    } else if (document == '') {
      message = "Digite el documento o Nit"
      error = true

    }
    else if (typedocumentSelect == null && typeuserSelect.value == 1) {
      message = "Seleccione el tipo de documento"
      error = true
    }
    else if (adress == '') {
      message = "Digite su dirección"
      error = true

    }
    else if (phone == '') {
      message = "Digite su teléfono"
      error = true

    }
    else if (city == '') {
      message = "Digite su ciudad"
      error = true
    }
    else if (departament == '') {
      message = "Digite su departamento"
      error = true
    }




    if (error) {
      MessageError(message)
    }

    else {
      const data = new FormData()
      data.append('id', props.id)
      data.append('type_user', typeuserSelect.value)
      data.append('name', name)
      data.append('name2', name2)
      data.append('lastname', lastname)
      data.append('lastname2', lastname2)
      data.append('phone', phone)
      data.append('document', document)
      data.append('email', email)
      data.append('adress', adress)
      data.append('city', city)
      data.append('departament', departament)
      data.append('tipodocumento', typedocumentSelect == null ? '':typedocumentSelect.value)



      axios.post(ip + 'admon/guard_proveedores', data).then(response => {
        if (props.id > 0) {
          MessageSuccess("Usuario editado correctamente")
        }
        else {
          MessageSuccess("Usuario creado correctamente")
        }

        props.resetUsers()
      })
    }

  }

  return (

    <div className="card-body card-dashboard">
      <h2 id="title" style = {{fontWeight:'bold'}}>Registro de proovedores:</h2>
      <div className="form-group">
        <div className="row">
          <div className="col-md-10">
            <label className="sr-only" for="type_user">Tipo de tercero</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Tipo de tercero</div>
              </div>
              <Select
                value={typeuserSelect}
                className = "col-9 px-0"
                id = "type_user"
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={typepersonList}
                onChange={(e) => setTypeuserSelect(e)}
                placeholder="Seleccione el tipo de tercero"
                name="selectUsers"
                />

            </div>
          </div>
        </div>
        {
            typeuserSelect.value == 2 &&
          <div className = "row">
            <div className  = "col-md-10">
              <label className="sr-only" for="username">Nit</label>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Nit</div>
                </div>
                <input value={document} className="form-control" maxLength = "20" onChange={(e) => setDocument(e.target.value)} id="userlastname" type="number" name="lastname" />
              </div>
            </div>

            <div className  = "col-md-10">
              <label className="sr-only" for="username">Razón social</label>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Razón social</div>
                </div>
                <input value={name} className="form-control" maxLength = "40" onChange={(e) => setName(e.target.value)} id="username" type="text" name="name" />
              </div>
            </div>
            <div className  = "col-md-10">
              <label className="sr-only" for="username">Nombre representante legal</label>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Nombre representante legal</div>
                </div>
                <input value={name2} className="form-control" maxLength = "40" onChange={(e) => setName2(e.target.value)} id="username" type="text" name="name" />

              </div>
            </div>
            <div className  = "col-md-10">
              <label className="sr-only" for="username">Nombre comercial</label>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Nombre comercial</div>
                </div>
                <input value={lastname} className="form-control" maxLength = "40" onChange={(e) => setLastname(e.target.value)} id="username" type="text" name="name" />

              </div>
            </div>
          </div>
        }
        {
          typeuserSelect.value == 1 &&
          <div className="row">

            <div className="col-md-5">
              <label className="sr-only" for="username">Primer Nombre</label>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Primer Nombre</div>
                </div>
                <input value={name} className="form-control" maxLength = "40" onChange={(e) => setName(e.target.value)} id="username" type="text" name="name" />
              </div>
            </div>
            <div className="col-md-5">
              <label className="sr-only" for="username">Segundo Nombre</label>
              <div className="input-group mb-2">
                <div className="input-group-prepend">
                  <div className="input-group-text">Segundo Nombre</div>
                </div>
                <input value={name2} className="form-control" maxLength = "40" onChange={(e) => setName2(e.target.value)} id="username" type="text" name="name" />
              </div>
            </div>
          </div>
        }
      { typeuserSelect.value == 1 && <div className = "row">
          <div className="col-md-5">
            <label className="sr-only" for="username">Primer Apellido</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Primer Apellido</div>
              </div>
              <input value={lastname} className="form-control" maxLength = "40" onChange={(e) => setLastname(e.target.value)} id="userlastname" type="text" name="lastname" />
            </div>
          </div>
          <div className="col-md-5">
            <label className="sr-only" for="username">Segundo Apellido</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Segundo Apellido</div>
              </div>
              <input value={lastname2} className="form-control" maxLength = "40" onChange={(e) => setLastname2(e.target.value)} id="userlastname" type="text" name="lastname" />
            </div>
          </div>

        </div>
      }

      {
        typeuserSelect.value == 1 &&
        <div className="row">
          <div className="col-md-5">
            <label className="sr-only" for="username">Tipo de documento</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Tipo de documento</div>
              </div>
              <Select
                className = "col-7 px-0"
                value={typedocumentSelect}
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={typedocuments}
                onChange={(e) => setTypedocumentSelect(e)}
                placeholder="Seleccione"
                name="selectUsers"
                />
            </div>
          </div>
          <div className="col-md-5">
            <label className="sr-only" for="username">Documento</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Documento</div>
              </div>
              <input value={document} className="form-control" maxLength = "20" onChange={(e) => setDocument(e.target.value)} id="userlastname" type="number" name="lastname" />
            </div>
          </div>
        </div>
      }



        <div className="row">
          <div className="col-md-10">
            <label className="sr-only" for="username">Correo electronico</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Correo electronico</div>
              </div>
              <input value={email} className="form-control" maxLength = "90" onChange={(e) => setEmail(e.target.value)} id="useremail" type="email" name="email" />
            </div>
          </div>
        </div>


        <div className = "row">

          <div className="col-md-5">
            <label className="sr-only" for="username">Teléfono</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Teléfono</div>
              </div>
              <input value={phone} className="form-control" maxLength = "20" onChange={(e) => setPhone(e.target.value)} id="userlastname" type="number" name="lastname" />
            </div>
          </div>
          <div className="col-md-5">
            <label className="sr-only" for="username">País</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">País</div>
              </div>
              <input value={adress} className="form-control" maxLength = "50" onChange={(e) => setAdress(e.target.value)} id="userlastname" type="text" name="lastname" />
            </div>
          </div>
        </div>

        <div className = "row">

          <div className="col-md-5">
            <label className="sr-only" for="username">Ciudad</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Ciudad</div>
              </div>
              <input value={city} className="form-control" maxLength = "20" onChange={(e) => setCity(e.target.value)} id="userlastname"  name="lastname" />
            </div>
          </div>
          <div className="col-md-5">
            <label className="sr-only" for="username">Departamento</label>
            <div className="input-group mb-2">
              <div className="input-group-prepend">
                <div className="input-group-text">Departamento</div>
              </div>
              <input value={departament} className="form-control" maxLength = "50" onChange={(e) => setDepartament(e.target.value)} id="userlastname" type="text" name="lastname" />
            </div>
          </div>
        </div>



        <div className="my-2 d-flex col-md-3 justify-content-end px-0" style={{ float: 'right' }}>
          <button className="btn btn-warning mr-2" onClick={() => props.goback()} >Regresar</button>
          <button v-if="id" className="btn btn-success" onClick={() => save_User()}>Guardar Cambios</button>
        </div>
      </div>

    </div>
  );
}

export default New_Terceros;

if (document.getElementById('New_Terceros')) {
  ReactDOM.render(<New_Terceros />, document.getElementById('New_Terceros'));
}
