import React, { useState, useEffect } from 'react';
import New_Terceros from './New-Terceros';
import ReactDOM from 'react-dom';
import { ip } from '../../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'
import makeAnimated from 'react-select/animated';
import Select from 'react-select'
import AsyncSelect from 'react-select/async';

function Terceros() {
    const [loading, setLoading] = useState(false)
    const [showForm, setShowForm] = useState(1)
    const [usuarios, setUsuarios] = useState([])
    const [usuarioBack, setUsuarioBack] = useState([])
    const [userBack, setUserBack] = useState([])
    const [id, setId] = useState(null)
    const [name, setName] = useState('')
    const [name2, setName2] = useState('')
    const [lastname, setLastname] = useState('')
    const [lastname2, setLastname2] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [adress, setAdress] = useState('')
    const [city, setCity] = useState('')
    const [typeuser, setTypeUser] = useState(null)
    const [departament, setDepartament] = useState('')
    const [typedocumentSelect, setTypedocumentSelect] = useState(null)
    const [document, setDocument] = useState('')
    const [lisUser, setLisUser] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage, setPostsPerPage] = useState(5)
    const [text, setText] = useState('')
    const [typepersonList, setTypepersonList] = useState([
      {
        value:1,
        label:'Natural'
      },
      {
        value:2,
        label:'Jurídica'
      }
    ])
    const [typeuserSelect, setTypeuserSelect] = useState({
      value:1,
      label:'Natural'
    })
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
    useEffect(() => {

        obtain_users(typeuserSelect.value);

    }, []);


    const updateCurrentPage = async (number) => {
        await setLisUser([])
        await setCurrentPage(number)
        const dataNew = Pagination.paginate(usuarioBack, number, postsPerPage)
        await setLisUser(dataNew)
    }
    //volver
    const goback = async () => {
        setId(null)
        setName('')
        setName2('')
        setLastname('')
        setLastname2('')
        setEmail('')
        setDocument('')
        setPhone('')
        setAdress('')
        setCity('')
        setTypeUser(null)
        setDepartament('')
        setTypedocumentSelect('')
        var view = showForm
        view = view - 1
        setShowForm(view)
    }

    // parametros de busqueda
    const searchInput = async (value) => {
        setText(value)
        const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g, "")
        var newData = lisUser.filter(function (item2) {
            var name = ((item2.user_name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g, "")
            var resName = name.indexOf(inputSearch) > -1
            var resNameLasT = name.indexOf(inputSearch) > -1
            var res = false
            if (resNameLasT || resNameLasT) {
                res = true
            }
            return res;
        })
        setLisUser(newData)
    }

    const convertDocument = (value) => {
        if (value == 1) {
            return "R.C"
        }
        if (value == 2) {
            return "T.I";
        }
        if (value == 3) {
            return "C.C"
        }

        if (value == 4) {
            return "C.E"
        }

        if (value == 5) {
            return "P.E"
        }

        if (value == 6) {
            return "M.S"
        }

        if (value == 7) {
            return "A.S"
        }

    }

    const convertUser = (value) => {
        if (value == 1) {
            return "Repartidor"
        }
        if (value == 2) {
            return "Trabajador";
        }

    }

    //listando los usuarios
    const obtain_users = async (value) => {
        axios.get(ip + 'admon/obtain_proovedores').then(response => {
            var res = response.data.data
            let filter = res.filter(e=>e.type_user == value)
            setId(null)
            setName('')
            setUsuarioBack(filter);
            setShowForm(1)
            setUserBack(filter);
            const dataNew = Pagination.paginate(filter, currentPage, postsPerPage)
            setLisUser(dataNew)

        })
    }
      //eliminar usuarios
    const deshabilitar = async (value) => {
        Swal.fire({
            title: '¿Quieres eliminar este usuario?',
            showDenyButton: true,
            confirmButtonText: `Sí`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                const data = new FormData()
                data.append('id', value)
                axios.post(ip + 'admon/delete_proveedor', data).then(response => {
                    obtain_users(typeuserSelect.value)
                    Swal.fire('Eliminado correctamente!', '', 'success')
                })

            } else if (result.isDenied) {
                Swal.fire('Acción cancelada', '', 'info')
            }
        })
    }



       //llevar datos al formulario
    const edit = async (index) => {

        var data = lisUser[index]

        setName(data.user_name)
        setName2(data.user_name_2)
        setLastname(data.user_lastname)
        setEmail(data.user_email)
        setDocument(data.user_document)
        setTypedocumentSelect(data.tip_document)
        setAdress(data.user_adress)
        setId(data.user_id)
        if (data.type_user == 1) {
          setTypeUser({value:1,label:'Natural'})
        }
        else{
          setTypeUser({value:2,label:'Jurídica'})
        }
        setCity(data.ciudad)
        setDepartament(data.departamento)
        setPhone(data.user_phone)
        setShowForm(2)
    }


    const cambiarTipoUsuario = (value) => {
      setTypeuserSelect(value)
      obtain_users(value.value)
    }
    return (
        <div id='app'>

            <div className="card" style={{ borderRadius: 10 }}>
              {
                  showForm == 1 &&
                  <div className = "row">
                  <div className = "col-md-6">
                  <h2 className = "pt-2 pl-2" id="title" style = {{fontWeight:'bold'}}>Listado de proovedores</h2>
                  </div>

                  <div className = "col-md-6 d-flex justify-content-end">

                        <button className = "btn-create-prov mt-2 mr-2" onClick={() => setShowForm(2)}>
                          <div className = "d-flex pr-5">
                            <span>
                            Crear nuevo proovedor
                            </span>
                            <span className = "material-icons icon-add-prov">
                              add
                            </span>
                          </div>

                        </button>
                  </div>
                  </div>

              }


                {
                    showForm == 1 &&
                    <div id="search" className="col-12 col-md-6 mt-5 mb-3 d-flex justify-content align-items-center" style={{ height: 45, border: '1px solid #e4e4e4', borderRadius: 20, backgroundColor: 'white' }}>
                        <input onChange={(e) => searchInput(e.target.value)} style={{ width: 'inherit', border: 'none', fontSize: 14, outlineStyle: 'auto', outlineWidth:0 }} value={text} placeholder="Nombre del Usuario, cédula o Nit del proovedor" />
                        {
                            text.length == 0 ?
                                <span style={{ color: '#c3c3c3', cursor: 'pointer' }} className="material-icons">search</span>
                                : <span style={{ color: '#c3c3c3', cursor: 'pointer' }} onClick={() => searchInput('')} className="material-icons-round">cancel</span>
                        }
                    </div>
                }
                <div>

                    {
                        showForm == 1 &&
                        <div>

                          <Select
                            value={typeuserSelect}
                            className = "col-4 px-2"
                            id = "type_user"
                            closeMenuOnSelect={true}
                            components={animatedComponents}
                            options={typepersonList}
                            onChange={(e) => cambiarTipoUsuario(e)}
                            placeholder="Seleccione el tipo de tercero"
                            name="selectUsers"
                            />
                          <table className="mt-3 table-bordered table-sm table table-striped">
                            <thead>
                                <tr style = {{backgroundColor:'#e4e4e4'}}>
                                    <th>Id</th>
                                    {typeuserSelect.value == 1 ?
                                      <th>Nombre</th>
                                      :<th>NIT</th>
                                    }
                                    {typeuserSelect.value == 1 ?
                                      <th>Apellidos</th>
                                      :<th>Nombre comercial</th>
                                    }
                                    <th>Telefono</th>
                                    <th></th>
                                </tr>
                                {
                                    lisUser.map((item, index) => {
                                        return (
                                            <tr key={item.user_id}>

                                              <td>{item.user_id}</td>
                                              {typeuserSelect.value == 1 ?
                                                <td>{item.user_name}</td>
                                                :<td>{item.user_document}</td>
                                              }

                                                <td>{item.user_lastname}</td>

                                                <td>{item.user_phone}</td>
                                                <td>
                                                    <div className="d-flex justify-content-start">
                                                        <button onClick={() => edit(index)} className="btn-create-prov mr-2" style = {{borderRadius:10}}>
                                                          <i class="nav-icon fas fa-edit"></i>
                                                      </button>
                                                        <button onClick={() => deshabilitar(item.user_id)} className="btn-create-prov" style = {{borderRadius:10}}>
                                                          <i class="nav-icon fas fa-trash-alt"></i>
                                                        </button>
                                                    </div>
                                                </td>

                                            </tr>
                                        );
                                    })

                                }

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        </div>
                    }
                    {
                        !loading && showForm == 1 &&

                        <div className="d-flex col-md-12 col-12 justify-content-fluid">
                            <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={usuarioBack.length} updateCurrentPage={(number) => updateCurrentPage(number)} />
                        </div>
                    }
                </div>

                {
                    showForm == 2 ?
                        <div>
                            <New_Terceros lastname={lastname} lastname2={lastname2} name={name} name2 = {name2} email={email} adress={adress} city = {city} departament = {departament} phone={phone} document={document} typeuser = {typeuser} typedocumentSelect={typedocumentSelect}  id={id} goback={() => goback()} resetUsers={() => obtain_users(typeuserSelect.value)} />
                        </div>
                        : null
                }

            </div>
        </div>




    );

}
export default Terceros;
if (document.getElementById('Terceros')) {
    ReactDOM.render(<Terceros />, document.getElementById('Terceros'));
}
