import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2';
import ListProdInventario from './ListProdInventario';
import New_ProdInventario from './New-ProdInventario';

function ProductsInventario() {
  const [loading, setLoading] = useState(true)
  const [showForm, setShowForm] = useState(1)
  const [productos, setProductos] = useState([])
  const [id, setId] = useState(null)
  const [data, setData] = useState({})

  useEffect(() => {
    obtainProductos()
  }, []);
  const obtainProductos = async () => {
    axios.get(ip+'admon/obtain_productos_inventario').then(response=>{
      console.log(response.data);
      setProductos(response.data)
      setLoading(false)
    })
  }
  const goback = () => {
    setShowForm(1)
    setId(null)

  }
  const gobackSave = () => {
    setLoading(true)
    obtainProductos()
    setShowForm(1)
    setId(null)
    setData({})
  }
  const dataUpdate = async (data) => {
    console.log(data);

    var nombre = data.prod_nombre
    var descrip = data.prod_descp
    var price = data.prod_precio
    var categoria = data.prod_categoria

    const dataForAll = {
      nombre,
      descrip,
      price,
      money,
      categoria,
      descuento,
      img,

    }
    await setId(data.prod_id)
    await setData(dataForAll)
    await setShowForm(2)
    console.log(dataForAll);


  }

  return (
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i> Productos - inventario </h4>
            </div>
            <div className = "card-body">
              {
                showForm == 1 &&
                <div className="row justify-content-end">
                  <button onClick = {()=>setShowForm(2)} className="btn btn-outline-success round btn-min-width mr-1 mb-1">Agregar</button>
                </div>
              }
              <div className = "row padding-forms-admin">
                {
                  showForm == 1 ?
                  <ListProdInventario dataUpdate={(data)=>dataUpdate(data)}  productos= {productos} gobackSave = {()=>gobackSave()}/>
                  :showForm == 2 ?
                  <New_ProdInventario   goback = {()=>goback()} data={data}  gobackSave = {()=>gobackSave()} id = {id} />
                  :null

                }
              </div>
            </div>
          </div>
        </div>
      }
    </div>

);
}

export default ProductsInventario;

if (document.getElementById('ProductsInventario')) {
  ReactDOM.render(<ProductsInventario />, document.getElementById('ProductsInventario'));
}
