import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import AsyncSelect from 'react-select/async';
function ListProdInventario(props) {
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [productos, setProductos] = useState([])
  const [backProd, setBackProd] = useState(props.productos)
  const [backProd2, setBackProd2] = useState(props.productos)
  const [id, setId] = useState(null)
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [ usuariosSelect, setUsuariosSelect ] = useState([])
  const [type, setType] = useState(0)
  const [data, setData] = useState(null)
  const [text, setText] = useState('')
  const [descrip, setDescrip] = useState('')
  const [cantidad, setCantidad] = useState('')
  const [valor, setValor] = useState('')
  useEffect(() => {
    const dataNew = Pagination.paginate(backProd,currentPage,postsPerPage)
    setProductos(dataNew)
  }, []);

  const updateCurrentPage = async (number) => {
    await setProductos([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backProd,number,postsPerPage)
    await setProductos(dataNew)
  }

  const searchInput = async(value) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backProd2.filter(function(item2){
      var name = ((item2.prod_nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }

      return res;
    })
    await setCurrentPage(1)
    await setBackProd(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setProductos(dataNew)
    await setLoading(false)
  }
  const deshabilitar = async (value) => {
    var message = 'Quieres eliminar este producto'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Si`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value.prod_id)
        axios.post(ip+'admon/delete_productos',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          props.gobackSave()
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  const editar = (data) => {
   props.dataUpdate(data)
  }

  const generateEnter = async (item,value) => {
    await setData(item)
    await setType(value)
    $('#ModalGenerate').modal('show')
  }
  const generarMovimiento = async () => {
    if ((data.amount == 0 || (data.amount-cantidad) < 0)&& type == 1) {
      MessageError("No puedes generar salidas no hay prodcutos en stock")
    }
    else{
      if (usuariosSelect == null && usuariosSelect.value == null && type == 1 ) {
        MessageError("Seleccione el proovedor")
      }
      else if (descrip == '') {
        MessageError("Escriba la descripción")
      }
      else if (cantidad == '') {
        MessageError("Escriba la cantidad")
      }
      else if (valor == '') {
        MessageError("Escriba el precio")
      }
      else{
      var dataform = new FormData()
      dataform.append('usuariosSelect',usuariosSelect.value != null ? usuariosSelect.value : '')
      dataform.append('descrip',descrip)
      dataform.append('cantidad',cantidad)
      dataform.append('valor',valor)
      dataform.append('type',type)
      dataform.append('product',data.prod_id)
      axios.post(ip+'admon/generate_enter',dataform).then(response=>{
        MessageSuccess("Generada correctamente")
        $('#ModalGenerate').modal('hide')
        props.gobackSave()
      })
      }
    }
  }


  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
const generateReport = async (item) => {
  props.report(item)

}
const generateReportGeneral = async () => {
  props.reportgeneral()

}
  return (

    <div className = "col-md-12">

      <div className="modal fade" id="ModalGenerate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Generar {type == 1 ? 'Salida':'Entrada'}</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {
        data != null &&
        <div className="modal-body">
        {type == 2 && <div className="col-md-12">
            <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Proovedor</label>

            <Select
              value={usuariosSelect}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={JSON.parse(data.prod_proveedor)}
              onChange={(e)=>setUsuariosSelect(e)}
              placeholder = "Seleccione el proovedor"
              name="colors"
              />
          </div>}


          <div className="col-md-12">
            <label htmlFor="descrip" className = "titulo-form">Descripción</label>
            <input value = {descrip} className="form-control" placeholder = "Descripción" maxLength = "70" onChange = {(e)=>setDescrip(e.target.value)} id = "descrip" type="text" name=""/>
          </div>

          <div className="col-md-12">
            <label htmlFor="cantidad" className = "titulo-form">Cantidad</label>
            <input value = {cantidad} className="form-control" placeholder = "Cantidad" maxLength = "70" onChange = {(e)=>setCantidad(e.target.value)} id = "cantidad" type="number" name=""/>
          </div>
          <div className="col-md-12">
            <label htmlFor="valor" className = "titulo-form">Precio</label>
            <input value = {valor} className="form-control" placeholder = "Valor" maxLength = "70" onChange = {(e)=>setValor(e.target.value)} id = "valor" type="number" name=""/>
          </div>

        </div>
      }
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" className="btn btn-primary" onClick = {()=>generarMovimiento()}>Generar</button>
      </div>
    </div>
  </div>
</div>


<div className = "row d-flex justify-content-end mt-4" >
  <button className = "btn btn-secondary" onClick = {()=>generateReportGeneral()}>Generar Reporte General</button>
</div>

      <div className = "row">
        <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #e4e4e4', borderRadius:20, backgroundColor:'white'}}>
          <input onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Digite el nombre deL producto"/>
          {
            text.length == 0 ?
            <span style = {{color:'#c3c3c3', cursor:'pointer'}}  className= "material-icons">search</span>
            :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
        }


      </div>
      { !loading &&
        productos.map((item,index)=>{
          return (
            <div key = {index} className = "col-md-12 col-12 d-flex justify-content-center align-items-center" >

                

                <div className = "pt-1 d-flex justify-content-center">
                  <p className = "title-film-admon">Nombre: {item.prod_nombre}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">Descripción: {item.prod_descp}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">Cantidad: {item.amount}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">Unidad de medida: {JSON.parse(item.unity).label}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">Proovedores: {JSON.parse(item.prod_proveedor).map(itemProv=>{
                      return(
                        <span>{itemProv.label}</span>
                      )
                    }
                  )}</p>
                </div>
                <div className = "row">

                  <div className = "col-md-6">
                    <button className = "btn btn-danger" onClick = {()=>generateEnter(item,1)}>Salida</button>
                  </div>
                  <div className = "col-md-6">
                    <button className = "btn btn-success" onClick = {()=>generateEnter(item,2)}>Entrada</button>
                  </div>
                </div>

                <div className = "row pt-2">

                  <div className = "col-md-12 d-flex justify-content-center">
                    <button className = "btn btn-secondary" onClick = {()=>generateReport(item)}>Generar Reporte</button>

                  </div>
                </div>

            </div>
          );
        })
      }
      {
        !loading &&
        <div className="d-flex col-md-12 col-12 justify-content-end">
          <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backProd.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
        </div>
      }

    </div>
  </div>
);
}

export default ListProdInventario;

if (document.getElementById('ListProdInventario')) {
  ReactDOM.render(<ListProdInventario />, document.getElementById('ListProdInventario'));
}
