import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2';
import ListProdInventario from './ListProdInventario';
import New_ProdInventario from './New-ProdInventario';
import moment from 'moment';
import exportFromJSON from 'export-from-json'


function ProductsInventario() {
  const [loading, setLoading] = useState(true)
  const [showForm, setShowForm] = useState(1)
  const [productos, setProductos] = useState([])
  const [id, setId] = useState(null)
  const [FechaIni, setFechaIni] = useState('')
  const [FechaEnd, setFechaEnd] = useState('')
  const [dataReport, setDataReport] = useState({})
  const [data, setData] = useState({})
  const [resReport, setResRerport] = useState([])

  useEffect(() => {
    moment.locale('es')
    obtainProductos()
  }, []);
  const obtainProductos = async () => {
    axios.get(ip+'admon/obtain_productos_inventario').then(response=>{
      console.log(response.data);
      setProductos(response.data)
      setLoading(false)
    })
  }
  const goback = () => {
    setShowForm(1)
    setId(null)
    setResRerport([])

  }
  const gobackSave = () => {
    setLoading(true)
    obtainProductos()
    setShowForm(1)
    setId(null)
    setData({})
  }
  const dataUpdate = async (data) => {
    console.log(data);

    var nombre = data.prod_nombre
    var descrip = data.prod_descp
    var unidad = JSON.parse(data.unity)
    var proveedores = JSON.parse(data.prod_proveedor)

    const dataForAll = {
      nombre,
      descrip,
      unidad,
      proveedores
    }
    await setId(data.prod_id)
    await setData(dataForAll)
    await setShowForm(2)
    console.log(dataForAll);


  }

  const cambiar_report = async (data) => {
    await setDataReport(data)
    await setShowForm(3)
  }
  const report_general = async () => {
    await setDataReport({})
    await setShowForm(3)
  }

  const buscar = async () => {
    if (FechaIni == '' || FechaEnd == '') {
      MessageError("Selecciona las fechas")
    }
    else{
      const dataform = new FormData()
      dataform.append('FechaIni',moment(FechaIni).format('YYYY-MM-DD'))
      dataform.append('FechaEnd',moment(FechaEnd).format('YYYY-MM-DD'))
      dataform.append('Producto',dataReport.prod_id != null ? dataReport.prod_id:'')
      axios.post(ip+'admon/consult_enter',dataform).then(response=>{
        console.log(response);
        setResRerport(response.data.data)
      })
    }
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }

  const convertMoney = (number, decPlaces, decSep, thouSep) => {

    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
     decSep = typeof decSep === "undefined" ? "." : decSep;
     thouSep = typeof thouSep === "undefined" ? "," : thouSep;
     var sign = number < 0 ? "-" : "";
     var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
     var j = (j = i.length) > 3 ? j % 3 : 0;

     return sign +
         (j ? i.substr(0, j) + thouSep : "") +
         i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
         (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

const calcular_cantidad = (data) => {
  var total = 0
  data.map(item=>{
    if (item.type == 1) {
      total = total - item.cantidad
    }
    else{
      total = total + item.cantidad
    }
  })
  return total;
}
const calcular_total = (data) => {
  var total = 0
  data.map(item=>{
    if (item.type == 1) {
      total = total + item.valor
    }
    else{
      total = total - item.cantidad
    }
  })
  return total;
}
  const Excel = () => {
    const object = []
      resReport.map(item=>{
        const data = {
          Id: item.id,
          Fecha: moment(item.fecha).format('LL'),
          Tipo: item.type == 1 ? 'Salida':'Entrada',
          Cantidad:item.cantidad+' '+JSON.parse(item.producto.unity).label,
          Descripcion: item.descript,
          Valor_Unidad: "$"+convertMoney(item.valor_unidad),
          Valor_Total: "$"+convertMoney(item.valor),
          Saldo:item.saldo,
          Valor_total_inventario:"$"+convertMoney(item.valor_total_inv),
          Costo_promedio_Unidad:"$"+convertMoney(item.costo_pro),
        }
        object.push(data)
      })
    





      const fileName = 'Reporte_Movimientos'
      const exportType = exportFromJSON.types.csv
      console.log(object);

      JSONToCSVConvertor( object, fileName, true )

  }
  const JSONToCSVConvertor = (JSONData, ReportTitle, ShowLabel) => {

   var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

   var CSV = 'sep=,' + '\r\n\n';

   if (ShowLabel) {
       var row = "";


       for (var index in arrData[0]) {


           row += index + ',';
       }

       row = row.slice(0, -1);


       CSV += row + '\r\n';
   }


   for (var i = 0; i < arrData.length; i++) {
       var row = "";


       for (var index in arrData[i]) {
           row += '"' + arrData[i][index] + '",';
       }

       row.slice(0, row.length - 1);

       CSV += row + '\r\n';
   }

   if (CSV == '') {
       alert("Invalid data");
       return;
   }


   var fileName = "MyReport_";
   fileName += ReportTitle.replace(/ /g,"_");


   var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);


   var link = document.createElement("a");
   link.href = uri;

   link.style = "visibility:hidden";
   link.download = fileName + ".csv";

   //this part will append the anchor tag and remove it after automatic click
   document.body.appendChild(link);
   link.click();
   document.body.removeChild(link);
  }
  return (
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i> Productos - inventario </h4>
            </div>
            <div className = "card-body">
              {
                showForm == 1 &&
                <div className="row justify-content-end">
                  <button onClick = {()=>setShowForm(2)} className="btn btn-outline-success round btn-min-width mr-1 mb-1">Agregar nuevo producto</button>
                </div>
              }
              <div className = "row padding-forms-admin">
                {
                  showForm == 1 ?
                  <ListProdInventario dataUpdate={(data)=>dataUpdate(data)} report = {(data)=>cambiar_report(data)} reportgeneral = {()=>report_general()}  productos= {productos} gobackSave = {()=>gobackSave()}/>
                  :showForm == 2 ?
                  <New_ProdInventario   goback = {()=>goback()} data={data}  gobackSave = {()=>gobackSave()} id = {id} />
                  :showForm == 3 ?
                  <div className = "col-md-12">
                    <h4 className = "pt-2" id="title" style = {{fontWeight:'bold'}}>Informe del producto: {dataReport.prod_nombre}</h4>
                    <div className = "row pt-2 px-2">
                      <div>
                        <label htmlFor="FechaIni" className = "titulo-form">Fecha Inicial</label>
                        <input type = "date" id = "FechaIni" className = "form-control" onChange = {(e)=>setFechaIni(e.target.value)} placeholder = "Fecha Inicial"/>
                      </div>
                      <div className = "pl-2">
                        <label htmlFor="FechaEnd" className = "titulo-form">Fecha Final</label>
                        <input type = "date" id = "FechaEnd" className = "form-control" onChange = {(e)=>setFechaEnd(e.target.value)} placeholder = "Fecha Final"/>
                      </div>
                      <div className = "pl-2 pt-2">

                        <button className="btn btn-success mt-4" onClick = {()=>buscar()}>Buscar</button>
                      </div>
                    </div>
                    <div className = "pt-3">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Valor Unidad</th>
                            <th scope="col">Valor Total</th>
                            <th scope="col">Saldo</th>
                            <th scope="col">Valor total inventario</th>
                            <th scope="col">Costo promedio Unidad</th>
                          </tr>
                        </thead>
                        <tbody>
                          {
                            resReport.map(item=>{
                              return(
                                <tr>
                                  <th scope="row">{item.id}</th>
                                  <td>{moment(item.fecha).format('LL')}</td>
                                  <td>{item.type == 1 ? 'Salida':'Entrada'}</td>
                                  <td>{item.cantidad} {JSON.parse(item.producto.unity).label}</td>
                                  <td>{item.descript}</td>
                                  <td>${convertMoney(item.valor_unidad)}</td>
                                  <td>${convertMoney(item.valor)}</td>
                                  <td>{item.saldo}</td>
                                  <td>${convertMoney(item.valor_total_inv)}</td>
                                  <td>${convertMoney(item.costo_pro)}</td>

                                </tr>
                              )
                            })
                          }


                        </tbody>
                      </table>
                      <div className = "row d-flex justify-content-end">
                        <div className = "col-md-6 d-flex justify-content-end">
                          {resReport.length>0 &&<button className="btn btn-success mr-2" onClick = {()=>Excel()} >Excel</button>}
                          <button className="btn btn-warning" onClick = {()=>goback()} >Regresar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  :null

                }
              </div>
            </div>
          </div>
        </div>
      }
    </div>

  );
}

export default ProductsInventario;

if (document.getElementById('ProductsInventario')) {
  ReactDOM.render(<ProductsInventario />, document.getElementById('ProductsInventario'));
}
