import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import AsyncSelect from 'react-select/async';
import Compressor from 'compressorjs';
import $ from 'jquery';

function New_ProdInventario(props) {
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
    const [nombre, setNombre] = useState('')
    const [descrip, setDescrip] = useState('')
    const [price, setPrice] = useState('')
    const [usuarioList, setUsuariosList] = useState(null)
    const [unidadList, setUnidadList] = useState([
      {
        value:'mg',
        label: 'Miligramo',
      },
      {
        value:'g',
        label: 'Gramo',
      },
      {
        value:'kg',
        label: 'Kilo Gramo',
      },
      {
        value:'ml',
        label: 'Mililitro',
      },
      {
        value:'L',
        label: 'Litro',
      },
      {
        value:'kl',
        label: 'Kilo Litro',
      },
      {
        value:'mm',
        label: 'Milímetro',
      },
      {
        value:'cm',
        label: 'Centímetro',
      },
      {
        value:'m',
        label: 'Metro',
      },


    ])
    const [usuariosSelect,  setUsuariosSelect] = useState([])
    const [unidadSelect,  setUnidadSelect] = useState(null)
    const [loading, setLoading] = useState(false)
    const [loadingSave, setLoadingSave] = useState(false)

  useEffect(() => {
    obtainCategories()
    validarId()
  }, []);
  const obtainCategories = async () => {
    axios.get(ip+'admon/obtain_proovedores').then(response=>{
      var res = response.data.data

      console.log(res);
      console.log("estos son los proveedores mk");
      console.log("entrooooo");
      console.log("entrooooo");
      console.log("entrooooo");
      console.log("entrooooo");

      var proovedores = [];
      res.map(item=>{
        const data = {
          value:item.user_id,
          label: item.user_name + ' ' + item.user_lastname,
        }
        proovedores.push(data)
      })
      setUsuariosList(proovedores)




    })
  }
  const validarId = async () => {
    if (props.id>0) {
      setNombre(props.data.nombre)
      setDescrip(props.data.descrip)
      setUsuariosSelect(props.data.proveedores)
      setUnidadSelect(props.data.unidad)
    }
  }


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_films = async () => {
    setLoadingSave(true)
    console.log(nombre);

    var message = ''
    var error = false
    if (nombre == '') {
      error = true
      message = "El nombre no puede ir vacío"
    }

    else if (descrip == '' || descrip == null) {
      error = true
      message = "Escribe una descripción"
    }

    else if (unidadSelect == null) {
      error = true
      message = "Elige una unidad de medida"
    }

    else if (usuariosSelect.length == 0) {
      error = true
      message = "Elige almenos un proovedor"
    }


    else{
      const data = new FormData()
      data.append('id', props.id)
      data.append('nombre',nombre)
      data.append('descrip',descrip)
      data.append('proovedores',JSON.stringify(usuariosSelect))
      data.append('unidad',JSON.stringify(unidadSelect))
      axios.post(ip+'admon/guard_producto_inventario',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Producto creado correctamente")
          props.gobackSave()
        }
        else{
          MessageSuccess("Producto editado correctamente")
          props.gobackSave()
        }
      })
    }

  }
  return (
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre</label>
            <input value = {nombre} className="form-control" placeholder = "Nombre " maxLength = "70" onChange = {(e)=>setNombre(e.target.value)} id = "Nombre" type="text" name="nombre"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="descrip" className = "titulo-form">Descripción</label>
            <input value = {descrip} className="form-control" placeholder = "Descripcion" maxLength = "70" onChange = {(e)=>setDescrip(e.target.value)} id = "descrip" type="text" name="descrip"/>
          </div>


          <div className="col-md-6">
            <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Proovedor</label>
            <Select
            isMulti
            value={usuariosSelect}
            closeMenuOnSelect={false}
            components={animatedComponents}
            options={usuarioList}
            onChange={(e)=>setUsuariosSelect(e)}
            placeholder = "Seleccione el proovedor"
            name="colors"
              />
          </div>
          {
            props.id == null &&
            <div className="col-md-6">
              <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Unidad de medida</label>
              <Select
                value={unidadSelect}
                closeMenuOnSelect={false}
                components={animatedComponents}
                options={unidadList}
                onChange={(e)=>setUnidadSelect(e)}
                placeholder = "Seleccione la unidad de medida"
                name="colors"
                />
            </div>
          }
        </div>


        {
          !loadingSave ?
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-warning mr-2" onClick = {()=>props.goback()} >Regresar</button>
            <button className="btn btn-success" onClick = {()=>save_films()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button v-if="id" className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }

    </div>

  </div>

);
}

export default New_ProdInventario;

if (document.getElementById('New_ProdInventario')) {
  ReactDOM.render(<New_ProdInventario />, document.getElementById('New_ProdInventario'))
}
