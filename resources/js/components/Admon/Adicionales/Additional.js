import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import ListAdd from './ListAdd';
import NewAdd from './NewAdd';


function Additional() {
  const [loading, setLoading] = useState(true)
  const [showForm, setShowForm] = useState(1)
  const [adicional, setAdicional] = useState([])
  const [cat, setCat] = useState([])
  const [id, setId] = useState(null)
  const [data, setData] = useState({})

  useEffect(() => {
    obtainAdd()
  }, []);
  const obtainAdd = async () => {
    axios.get(ip+'admon/obtain_add').then(response=>{
      setAdicional(response.data)
      setLoading(false)
    })
  }


  const goback = () => {
    setShowForm(1)
    setId(null)

  }
  const gobackSave = () => {
    setLoading(true)
    obtainAdd()
    setShowForm(1)
    setId(null)
    setData({})
  }
  const dataUpdate = async (data) => {
    console.log(data);

    var name = data.name_add
    var price = data.price_add

    var categoria = []
  data.data.map(item=>{
    const data2 = {
      value:item.categoria.cat_id,
      label:item.categoria.cat_nombre,
      check:1
    }
    categoria.push(data2)
  })

    const dataForAll = {
      name,
      price,
      categoria
    }
    await setId(data.id_adicional)
    await setData(dataForAll)
    await setShowForm(2)
    console.log("esto es:",dataForAll);


  }

  return (
    <div >
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i> Adicionales </h4>
            </div>
            <div className = "card-body">
              {
                showForm == 1 &&
                <div className="row">
                    <div className="col-md-6 justify-content-end">
                      <button onClick = {()=>setShowForm(2)} className="btn btn-outline-success round btn-min-width mr-1 mb-1">Agregar</button>

                    </div>

                </div>

              }
              <div className = "row padding-forms-admin">
                {

                  showForm == 3 ?
                  <NewAdd    gobackSave = {()=>gobackSave()}  data={data}  goback = {()=>goback()}  id = {id} />
                  :showForm == 3 ?
                  <ListAdd  dataUpdate={(data)=>dataUpdate(data)} adicional= {adicional}   gobackSave = {()=>gobackSave()}  />
                  :null

                }
              </div>
            </div>
          </div>
        </div>
    </div>
);
}

export default Additional;

if (document.getElementById('Additional')) {
  ReactDOM.render(<Additional />, document.getElementById('Additional'));
}
