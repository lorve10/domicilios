import React, { useState, useEffect}  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import $ from 'jquery';

function Adicionales(props) {
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [name, setName] = useState('')
  const [price, setPrice] = useState('')
  const [categoria, setCategoria] = useState(null)
  const [categorialist, setCategoriaslist] = useState(null)
  const [categoriasSelect,  setCategoriasSelect] = useState([])
  const [loading, setLoading] = useState(false)
  const [loadingSave, setLoadingSave] = useState(false)

  useEffect(() => {
    obtainCategories()
    validarId()
  }, []);
  const obtainCategories = async () => {
    axios.get(ip+'admon/obtain_categories').then(response=>{
      var res = response.data

      console.log(res);
      var categorias = [];
      res.map(item=>{
        console.log(props);
        if (props.id>0 && item.cat_id == props.data.categoria) {
          console.log("entro a editar");
          console.log(props);
          const data2 = {
            value:item.cat_id,
            label:item.cat_nombre,
          }
          setCategoriasSelect(data2)
        }
        const data = {
          value:item.cat_id,
          label:item.cat_nombre
        }
        categorias.push(data)
      })
      setCategoriaslist(categorias)

    })
  }
  const validarId = async () => {
    if (props.id > 0) {
      console.log("entrooo a editar si");
      setName(props.data.name);
      setPrice(props.data.price)
      setCategoriasSelect(props.data.categoria)
    }
  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_Adi = async () => {
    console.log('este es el nombre', name);
    console.log("entro");
    var message = ''
    var error = false
    if (name == '') {
      error = true
      message = "El nombre no puede ir vacío"
    }
    else if (price == '') {
      error = true
      message = "Escribe un precio"
    }
    else if (categoriasSelect.length == 0) {
      error = true
      message = "Selecciona una categoria"
    }
    if (error) {
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id',props.id)
      data.append('name',name)
      data.append('price', price)
      data.append('categoria',JSON.stringify(categoriasSelect))
      axios.post(ip+'admon/guard_add',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Producto creado correctamente")
          props.gobackSave()
        }
        else{
          MessageSuccess("Producto editado correctamente")
          props.gobackSave()
        }
      })
    }

  }
  return (
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre Adicional</label>
            <input value = {name} className="form-control" placeholder = "Nombre del Adicionl" maxLength = "70" onChange = {(e)=>setName(e.target.value)} id = "Nombre" type="text" name="nombre"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Precio</label>
            <input value = {price} className="form-control" placeholder = "Precio del producto" maxLength = "70" onChange = {(e)=>setPrice(e.target.value)} id = "price" type="number" name="price"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Categoría</label>
            <Select
              value={categoriasSelect}
              closeMenuOnSelect={false}
              components={animatedComponents}
              options={categorialist}
              onChange={(e)=>setCategoriasSelect(e)}
              isMulti
              placeholder = "Seleccione las categorías"
              name="colors"
              />
        </div>
        </div>
        {
          !loadingSave ?
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-warning mr-2" onClick = {()=>props.goback()} >Regresar</button>
            <button className="btn btn-success" onClick = {()=>save_Adi()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button v-if="id" className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }
    </div>
    </div>


);
}

export default Adicionales;

if (document.getElementById('Adicionales')) {
  ReactDOM.render(<Adicionales />, document.getElementById('Adicionales'))
}
