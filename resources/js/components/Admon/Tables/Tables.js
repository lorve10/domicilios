import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import ListProd from './ListProd';
import CryptoJS  from 'crypto-js';
import $ from 'jquery';
import QRCode from "react-qr-code";



function Tables() {
  const [loading, setLoading] = useState(true)
  const [loadingSave, setLoadingSave] = useState(false)
  const [showForm, setShowForm] = useState(0)
  const [categories, setCategories] = useState(1)
  const [cantTables, setCatTables] = useState(0)
  const [tables, setTables] = useState([])
  const [tableSelect, setTableSelect] = useState(null)
  const [id, setId] = useState(null)
  const [productos, setProductos] = useState([])
  const [encrypt, setEncrypt] = useState('')
  const [data, setData] = useState({})
  useEffect(() => {
    obtain_tables()
    obtainProductos()

  }, []);
  const obtainProductos = async () => {
    axios.get(ip+'admon/obtain_productos').then(response=>{
      setProductos(response.data)
    })
  }
  const obtain_tables = async () => {


    axios.get(ip+'admon/obtain_tables').then(response=>{
      console.log(response.data);
      if (response.data == '') {
        console.log("entroooo");
        setShowForm(1)
      }
      else{
        console.log("entrooo 2");
        setShowForm(2)
        setCatTables(response.data.can_tab)
        var mesasarray = []
        for (var i = 0; i < response.data.cant_tab; i++) {
          const data = {
            id:i+1,
            mesa:'Mesa '+ (i+1)
          }
          mesasarray.push(data)
        }
        console.log(mesasarray);
        setTables(mesasarray)
        setLoading(false)

      }
    })
  }

  const save_tables = () => {
    if (cantTables>0) {
      setLoadingSave(true)
      const datapost = new FormData()
      datapost.append('mesas', cantTables)
      axios.post(ip+'admon/guard_tables',datapost).then(response=>{
        console.log("entroooo");
        setShowForm(2)
        var mesasarray = []
        for (var i = 0; i < cantTables; i++) {
          const data = {
            id:(i+1),
            mesa:'Mesa '+ (i+1)
          }
          mesasarray.push(data)
        }
        console.log(mesasarray);
        setTables(mesasarray)
        setLoadingSave(false)

      })
    }
    else{
      MessageError("El número de mesas debe ser mayor a 0")
    }

  }
  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const selectTable = (data) => {
    setTableSelect(data)
    setShowForm(3)
  }
  const goback = (data) => {
    setShowForm(data)
  }

  const generatedQr = async (data) => {
    setTableSelect(data)
    data = "OneturpialMesas "+data
    var cripted =  CryptoJS.AES.encrypt(data, '100200OneTurpial-2021AdminConsult').toString();
    var decrypt =  CryptoJS.AES.decrypt("U2FsdGVkX19hWSGmgovfwPV3lw9g9aX33ocLhdtJopgPd2FSy3sPeretQ40Bc7Bk", '100200OneTurpial-2021AdminConsult');
    var originalText = decrypt.toString(CryptoJS.enc.Utf8);
    console.log(originalText);
    console.log(cripted);
    console.log("entroooo");
    await setEncrypt(cripted)
    $('#modalContentQR').modal('show')
  }
  const print = async () => {
    var mywindow = window.open('', 'PRINT', 'height="fit-content",width="fit-content"');

     mywindow.document.write('<html><head><title>' + 'CÓDIGO QR' + '</title>');
     mywindow.document.write('</head><body>');
     mywindow.document.write(document.getElementById('printThis').innerHTML);
     mywindow.document.write('</body></html>');

     mywindow.document.close(); // necessary for IE >= 10
     mywindow.focus(); // necessary for IE >= 10*/

     mywindow.print();
     mywindow.close();

  }
  return (
    <div >
      <div className="modal fade" id="modalContentQR" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div className="modal-dialog" role="document">
<div className="modal-content">
  <div className="modal-header">
    <h5 className="modal-title" id="exampleModalLabel">Código Qr de la mesa {tableSelect}</h5>
    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div className="modal-body">
    <div className = "row justify-content-end px-4">
      <span className = "material-icons" onClick = {()=>print()} style = {{fontSize:28, cursor:'pointer'}}>print</span>
    </div>

    <div className = "row d-flex justify-content-center" id = "printThis">

    <QRCode value={encrypt} />
    </div>
  </div>
  <div className="modal-footer">
    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>

      {
        !loading &&

        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>


          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i>Configuración de mesas</h4>
            </div>
            <div className = "card-body">
              <div  className="form-group">
                {
                  showForm == 1?
                  <div>
                  <div className="row">
                    <div className="col-md-6">
                      <label htmlFor="Nombre" className = "titulo-form">Cantidad de mesas</label>
                      <input value = {cantTables} type = "number" className="form-control" placeholder = "Cantidad de mesas" maxLength = "70" onChange = {(e)=>setCatTables(e.target.value)} id = "Nombre" type="text" name="nombre"/>
                    </div>
                  </div>
                  {
                    !loadingSave ?
                    <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
                      <button className="btn btn-warning mr-2" onClick = {()=>setShowForm(2)} >Regresar</button>
                      <button v-if="id" className="btn btn-success" onClick = {()=>save_tables()}>Guardar Cambios</button>
                    </div>
                    :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
                    <button v-if="id" className="btn btn-success">
                      <div className="spinner-grow text-light" role="status">
                        <span className="sr-only">Loading...</span>
                      </div>

                    </button>
                  </div>
                }
                </div>
                  :showForm == 2 ?
                  <div>
                    <div className="my-2 d-flex col-md-12 justify-content-end px-0" style = {{float:'right'}}>
                      <button v-if="id" className="btn btn-success" onClick = {()=>setShowForm(1)}>Configurar mesas</button>
                    </div>
                  <div className = "row justify-content-center">

                    {
                      tables.map((item,i)=>{
                        return (

                          <div key = {i} className = " p-2 col-md-4 col-6 " style = {{zIndex:1}}>
                            <div style = {{zIndex:2, position:'absolute', right:10, top:10}} onClick = {()=>generatedQr(item.id)}>
                              <span className= "material-icons" style = {{fontSize:34, cursor:'pointer'}}>qr_code_2</span>
                            </div>
                            <div onClick = {()=>generatedQr(item.id)} className = "tables d-flex align-items-center justify-content-center" style = {{width:'100%'}}>
                            <p className = "m-0">{item.mesa}</p>
                            </div>
                          </div>


                        );
                      })
                    }
                    {
                    tables.length%2 ?
                    <div className = " p-2 col-md-4 col-6">
                    </div>
                    :null
                    }
                  </div>
                  </div>
                  :showForm == 3 ?
                  <ListProd tableSelect = {tableSelect} productos= {productos} goback = {(data)=>goback(data)}/>
                  :null
                }
              </div>
            </div>
            </div>
          </div>

      }
    </div>

);
}

export default Tables;

if (document.getElementById('Tables')) {
  ReactDOM.render(<Tables />, document.getElementById('Tables'));
}
