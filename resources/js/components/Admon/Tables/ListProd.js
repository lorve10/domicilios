import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'
import Cookies from 'universal-cookie'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import $ from 'jquery'
function ListProd(props) {
  const [loading, setLoading] = useState(false)
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [loadingButton, setLoadingButton] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [productos, setProductos] = useState([])
  const [backProd, setBackProd] = useState([])
  const [backProd2, setBackProd2] = useState([])
  const [id, setId] = useState(null)
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [cartCounter, setCartCounter] = useState(0);


  const [text, setText] = useState('')
  const [cart, setCart] = useState([])
  useEffect(() => {
    adicionales()
    cookieget()
  }, []);
  const adicionales = () => {
    var productos = props.productos
    productos.map(item=>{
      item.adicionales = []
      item.adicionales_select = []
      item.categoria.adicionales.map(item2=>{
        const dataPush = {
          value:item2.adicional.id_adicional,
          label:item2.adicional.name_add,
          price:item2.adicional.price_add
        }
        item.adicionales.push(dataPush)
      })
    })
    setBackProd(productos)
    setBackProd2(productos)
    const dataNew = Pagination.paginate(productos,currentPage,postsPerPage)
    setProductos(dataNew)
  }
  const cookieget = () =>{
    const cookies = new Cookies();

    var mesa = "mesa-"+props.tableSelect
    var getCookie = cookies.get(mesa)
    var cartArray = []
    if(getCookie){
      cartArray =getCookie
    }
    console.log("esta es la cookie");
    console.log(cartArray);
    setCart(cartArray)
    setCartCounter(cartArray.length)

  }
  const updateCurrentPage = async (number) => {
    await setProductos([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backProd,number,postsPerPage)
    await setProductos(dataNew)
  }
  const searchInput = async(value) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backProd2.filter(function(item2){
      var name = ((item2.prod_nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }

      return res;
    })
    await setCurrentPage(1)
    await setBackProd(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setProductos(dataNew)
    await setLoading(false)
  }


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const showCart = () => {
    $('#exampleModal').modal('show')

  }
  const total = () => {
    var total = 0
    cart.map(item=>{
      total = total + item.total
    })
    return total
  }
  const convertMoney = (value) => {
      const formatter = new Intl.NumberFormat('de-DE', {
         style: 'currency',
         currency: 'COP',
         minimumFractionDigits: 0
       })
       return formatter.format(value)
    }
    const delete_productos = (value,index) => {
      const cookies = new Cookies();
      var mesa = "mesa-"+props.tableSelect
      setLoadingButton(true)
      var carrito = cart
      carrito.splice(index,1)
      setCart(carrito)
      setCartCounter(carrito.length)
      setLoadingButton(false)
      cookies.set(mesa,carrito)



    }
    const add = (item) => {
      var cantidad = window.document.getElementById('data_to_record'+item.prod_id).value
      const cookies = new Cookies();
      var adicionales = item.adicionales_select
      var mesa = "mesa-"+props.tableSelect
      var getCookie = cookies.get(mesa)
      var cartArray = []
      if(getCookie){
        cartArray =getCookie
      }
      console.log("esta es la cookie");
      console.log(cartArray);
      var totaladicionales = 0
      adicionales.map(item2 => {
        console.log(item2);
        totaladicionales = totaladicionales+(item2.price*cantidad)
      })
      console.log(totaladicionales);
      setLoadingButton(true)
      console.log(cantidad);
      if (cantidad > 0) {
        const data = {
          cantidad,
          id:item.prod_id,
          nombre:item.prod_nombre,
          imagen:item.prod_imagen,
          preciou:item.prod_precio,
          total:(item.prod_precio*cantidad)+totaladicionales,
          adicionales:item.adicionales_select
        }
        console.log(data);
        cartArray.push(data)
        cookies.set(mesa,cartArray)

        console.log(cartArray);
        setCartCounter(cartArray.length)
        setCart(cartArray)
        setLoadingButton(false)
        window.document.getElementById('data_to_record'+item.prod_id).value = ''
      }
      else{
        MessageError("Por favor digita una cantidad")
      }

    }
    const selectAdicionales = async (data,index) => {
      var productosback = productos
      productosback[index].adicionales_select = data
      await setProductos(productosback)
    }
  return (
    <div className = "col-md-12">
      <div className = "row pt-2">
        <div className = "col-md-6 col-6 ">
        <h6 className = "mobile-enabled">Pedido de mesa {props.tableSelect}</h6>
        <h4 className = "mobile-disabled">Pedido de mesa {props.tableSelect}</h4>
        </div>
        <div className = "col-md-6 col-6 d-flex justify-content-end align-items-center">
          <button className="btn btn-warning mr-2" onClick = {()=>props.goback(2)} >Regresar</button>
        </div>
      </div>
      <div className ="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className ="modal-dialog" role="document">
          <div className ="modal-content">
            <div className ="modal-header">
              <h5 className ="modal-title" id="exampleModalLabel">Pedido de mesa {props.tableSelect}</h5>
              <button type="button" className ="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className ="modal-body">
              <div className = "row pt-2">
                <div className = "col-md-4 col-5 d-flex">
                  <strong>Producto</strong>
                </div>
                <div className = "col-md-4 col-2 d-flex">
                  <strong>Valor u * cantidad</strong>

                </div>
                <div className = "col-md-3 col-3 d-flex">
                <strong>Total</strong>
                </div>
                <div className = "col-md-1 col-2">
                  <div className = "d-flex justify-content-center">

                  </div>
                </div>



              </div>
              { !loadingButton &&
                cart.map((item,index)=>{
                  return (
                    <div className = "row pt-2">
                      <div className = "col-md-4 col-5">
                        <div className = "d-flex">
                          <div>
                          <img src = {ip+item.imagen} className = "imagen-admon-list"/>
                          <p class = "m-0 mt-1">{item.nombre}</p>
                          </div>
                        </div>
                      </div>
                      <div className = "col-md-4 col-3">
                        <div className = "d-flex align-items-center">
                          <div style = {{width:'100%'}} className = "d-flex">

                          <p className = "m-0"  style = {{textAlign:'center'}}>${convertMoney(item.preciou)}</p>
                          <p className = "m-0 ml-1"  style = {{textAlign:'center'}}>x</p>
                          <p className = "m-0 ml-1" style = {{textAlign:'center'}}>{item.cantidad}</p>
                          </div>
                        </div>
                      </div>
                      <div className = "col-md-3 col-3">
                        <div className = "d-flex justify-content-center">
                          <span>${convertMoney(item.total)}</span>
                        </div>
                      </div>
                      <div className = "col-md-1 col-1">
                        <div className = "d-flex justify-content-center">
                          <i className = "material-icons" style = {{color:'red', cursor:'pointer'}} onClick = {()=>delete_productos(item,index)}>delete</i>
                        </div>
                      </div>
                      {
                        item.adicionales.length > 0 &&
                        <div>
                        <div className = "col-md-4 col-5 d-flex">
                          <strong>Adicionales</strong>
                        </div>
                        <div className = " d-flex">
                        </div>
                            {
                              item.adicionales.map((item2,index)=>{
                                console.log("su madre");
                                console.log(item2);
                                return(
                                  <div style={{marginLeft:10}} className = "row pt-1">
                                    <div className = "col-md-4 col-5">
                                      <div className = "d-flex">
                                        <div>
                                        <p class = "m-0 mt-1">{item2.label}</p>
                                        </div>
                                      </div>
                                    </div>
                                    <div style={{marginLeft:75}} className = "col-md-4 col-8">
                                      <div className = "d-flex align-items-center">
                                        <div style = {{width:'100%'}} className = "d-flex">
                                        <p className = "m-0"  style = {{textAlign:'center'}}>${convertMoney(item2.price)}</p>
                                        </div>
                                      </div>
                                    </div>


                                    </div>

                                )
                              })
                            }
                        </div>

                      }



                    </div>
                  );
                })
              }
              <div className = "row pt-4">
                <div className = "col-md-5 col-5 d-flex">
                  <strong>Total:</strong>
                </div>

                <div className = "col-md-7 col-7">
                  <div className = "d-flex justify-content-start">
                    ${convertMoney(total())}
                  </div>
                </div>



              </div>
            </div>
            <div className ="modal-footer">
              <button type="button" className ="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" className ="btn btn-primary">Guardar pedido</button>
            </div>
          </div>
        </div>
      </div>

      <div className = "row">
        <div className = "col-md-12 mt-5 d-flex justify-content-end px-4">
        <div className = "d-flex">
          { cartCounter > 0 &&
            <div className = "cant-cart" >
                <span>{cartCounter}</span>
            </div>
          }
          <i className ="fas fa-shopping-cart" onClick= {()=>showCart()} style = {{fontSize:28, cursor:'pointer'}}></i>
        </div>
        </div>
        <div className="col-12 mt-4 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #e4e4e4', borderRadius:20, backgroundColor:'white'}}>
          <input onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Digite el nombre del producto"/>
          {
            text.length == 0 ?
            <span style = {{color:'#c3c3c3', cursor:'pointer'}}  className= "material-icons">search</span>
            :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
        }


      </div>
      { !loading &&
        productos.map((item,index)=>{
          return (
            <div key = {index} className = "col-md-4 col-12 d-flex justify-content-center align-items-center my-4">
              <div className  = "box-films-admon mb-3">

                <div className = "d-flex justify-content-center">
                  <img src = {ip+item.prod_imagen} className = "imagen-admon-list"/>
                </div>

                <div className = "pt-1 d-flex justify-content-center">
                  <p className = "title-film-admon">{item.prod_nombre}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">{item.prod_descp}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">${convertMoney(item.prod_precio)}</p>
                </div>
                <div className = "pt-1">
                <Select
                  closeMenuOnSelect={false}
                  components={animatedComponents}
                  options={item.adicionales}
                  onChange={(e)=>selectAdicionales(e,index)}
                  isMulti
                  placeholder = "Seleccione los adicionales"
                  name="colors"
                  />
                  </div>

                <div className = "pt-3 d-flex justify-content-center ">
                  <input id = {'data_to_record'+item.prod_id} className="form-control" placeholder = "Digite la cantidad" maxLength = "10" type="number" />
                </div>
                <div className = "pt-2 d-flex justify-content-center ">
                  <button  className="btn btn-success" onClick = {()=>add(item)}>agregar</button>
                </div>

              </div>
            </div>
          );
        })
      }
      {
        !loading &&
        <div className="d-flex col-md-12 col-12 justify-content-end">
          <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backProd.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
        </div>
      }

    </div>
  </div>
);
}

export default ListProd;

if (document.getElementById('ListProd')) {
  ReactDOM.render(<ListProd />, document.getElementById('ListProd'));
}
