import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'
function ListProd(props) {
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [productos, setProductos] = useState([])
  const [backProd, setBackProd] = useState(props.productos)
  const [backProd2, setBackProd2] = useState(props.productos)
  const [id, setId] = useState(null)
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [text, setText] = useState('')
  useEffect(() => {
    const dataNew = Pagination.paginate(backProd,currentPage,postsPerPage)
    setProductos(dataNew)
  }, []);

  const updateCurrentPage = async (number) => {
    await setProductos([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backProd,number,postsPerPage)
    await setProductos(dataNew)
  }

  const searchInput = async(value) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backProd2.filter(function(item2){
      var name = ((item2.prod_nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }

      return res;
    })
    await setCurrentPage(1)
    await setBackProd(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setProductos(dataNew)
    await setLoading(false)
  }
  const deshabilitar = async (value) => {
    var message = 'Quieres eliminar este producto'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Si`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value.prod_id)
        axios.post(ip+'admon/delete_productos',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          props.gobackSave()
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  const editar = (data) => {
   props.dataUpdate(data)
  }
  return (
    <div className = "col-md-12">
      <div className = "row">
        <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #e4e4e4', borderRadius:20, backgroundColor:'white'}}>
          <input onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Digite el nombre deL producto"/>
          {
            text.length == 0 ?
            <span style = {{color:'#c3c3c3', cursor:'pointer'}}  className= "material-icons">search</span>
            :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
        }


      </div>
      { !loading &&
        productos.map((item,index)=>{
          return (
            <div key = {index} className = "col-md-4 col-12 d-flex justify-content-center align-items-center">
              <div className  = "box-films-admon mb-3">
                <div className="dropdown dropright d-flex justify-content-end">
                  <a className="dropdown-toggle" style = {{color:'black'}} href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span className = "material-icons">more_vert</span>
                  </a>

                  <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a className="dropdown-item" onClick = {()=>editar(item)} href="#">Editar</a>
                    <a onClick = {()=>deshabilitar(item)} className="dropdown-item" href="#">Eliminar</a>

                  </div>
                </div>

                <div className = "d-flex justify-content-center">
                  <img src = {ip+item.prod_imagen} className = "imagen-admon-list"/>
                </div>

                <div className = "pt-1 d-flex justify-content-center">
                  <p className = "title-film-admon">{item.prod_nombre}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">{item.prod_descp}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">{item.prod_precio}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">{item.categoria.cat_nombre}</p>
                </div>
              </div>
            </div>
          );
        })
      }
      {
        !loading &&
        <div className="d-flex col-md-12 col-12 justify-content-end">
          <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backProd.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
        </div>
      }

    </div>
  </div>
);
}

export default ListProd;

if (document.getElementById('ListProd')) {
  ReactDOM.render(<ListProd />, document.getElementById('ListProd'));
}
