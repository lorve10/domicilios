import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import AsyncSelect from 'react-select/async';
import Compressor from 'compressorjs';
import $ from 'jquery';

function New_Prod(props) {
    const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
    const [nombre, setNombre] = useState('')
    const [descrip, setDescrip] = useState('')
    const [price, setPrice] = useState('')
    const [money, setMoney] = useState('')
    const [descuento, setDescuento] = useState(0)
    const [categoria, setCategoria] = useState(null)
    const [categorialist, setCategoriaslist] = useState(null)
    const [categoriasSelect,  setCategoriasSelect] = useState([])
    const [img, setImg] = useState({})
    const [urlImg, setUrlImg] = useState(null)
    const [loading, setLoading] = useState(false)
    const [loadingSave, setLoadingSave] = useState(false)

  useEffect(() => {
    obtainCategories()
    validarId()
  }, []);
  console.log(props.id);
  const obtainCategories = async () => {
    axios.get(ip+'admon/obtain_categories').then(response=>{
      var res = response.data

      console.log(res);
      var categorias = [];
      res.map(item=>{
        console.log(props);
        if (props.id>0 && item.cat_id == props.data.categoria) {
          console.log("entro a editar");
          console.log(props);
          const data2 = {
            value:item.cat_id,
            label:item.cat_nombre,
          }
          setCategoriasSelect(data2)
        }
        const data = {
          value:item.cat_id,
          label:item.cat_nombre
        }
        categorias.push(data)
      })
      setCategoriaslist(categorias)

    })
  }
  const validarId = async () => {
    if (props.id>0) {

      setNombre(props.data.nombre)
      setDescrip(props.data.descrip)
      setPrice(props.data.price)
      setMoney(props.data.money)
      setDescuento(props.data.descuento)
      setUrlImg(ip+props.data.img)
    }
  }


  const loadImage = (event) => {
    const file = event.target.files[0];
    if (file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg"){
      if (file.size <= 10000000) {
        new Compressor(file, {
          quality: 0.6,

          success(result) {
            var file = new File ([result], result.name, {type:result.type});
            setImg(file)
            console.log(file);
            let reader = new FileReader();
            reader.onload = e => {
              setUrlImg(e.target.result)
            };
            reader.readAsDataURL(file);
          }
        })
      }
      else{
        alert("imagen muy pesada")
      }
    }
    else{
      alert("Formato no valido")
    }
    console.log(file);

  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_films = async () => {
    setLoadingSave(true)
    console.log(nombre);
    console.log(categoriasSelect);
    console.log(img);

    var message = ''
    var error = false
    if (nombre == '') {
      error = true
      message = "El nombre no puede ir vacío"
    }
    else if (urlImg == null) {
      error = true
      message = "Sube la imagen del producto"
    }
    else if (descrip == '') {
      error = true
      message = "Escribe una descripción"
    }
    else if (price == '') {
      error = true
      message = "Escribe un precio"
    }
    else if (categoriasSelect.value == null) {
      error = true
      message = "Selecciona una categoria"
    }

/*
    else if (urlImg == null) {
      error = true
      message = "Sube la imagen de la serie o película"
    }

    if (error) {
      MessageError(message)
    }*/
    else{
      const data = new FormData()
      data.append('id', props.id)
      data.append('nombre',nombre)
      data.append('descrip',descrip)
      data.append('price', price)
      data.append('gastos', money)
      data.append('descuento', descuento)
      data.append('categoria',categoriasSelect.value)
      data.append('file',img)
      axios.post(ip+'admon/guard_productos',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Producto creado correctamente")
          props.gobackSave()
        }
        else{
          MessageSuccess("Producto editado correctamente")
          props.gobackSave()
        }
      })
    }

  }
  return (
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre</label>
            <input value = {nombre} className="form-control" placeholder = "Nombre " maxLength = "70" onChange = {(e)=>setNombre(e.target.value)} id = "Nombre" type="text" name="nombre"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Descripcion</label>
            <input value = {descrip} className="form-control" placeholder = "Descripcion" maxLength = "70" onChange = {(e)=>setDescrip(e.target.value)} id = "descrip" type="text" name="descrip"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Precio</label>
            <input value = {price} className="form-control" placeholder = "Precio del producto" maxLength = "70" onChange = {(e)=>setPrice(e.target.value)} id = "price" type="number" name="price"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Gastos de produccion</label>
            <input value = {money} className="form-control" placeholder = "Gastos de produccion" maxLength = "70" onChange = {(e)=>setMoney(e.target.value)} id = "money" type="number" name="money"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Descuento</label>
            <input value = {descuento} className="form-control" placeholder = "Gastos de produccion" maxLength = "70" minValue = "0" onChange = {(e)=>setDescuento(e.target.value)} id = "money" type="number" name="money"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Categoría</label>
            <Select
            value={categoriasSelect}
            closeMenuOnSelect={false}
            components={animatedComponents}
            options={categorialist}
            onChange={(e)=>setCategoriasSelect(e)}
            placeholder = "Seleccione el tipo"
            name="colors"
              />
          </div>
        </div>


        <div className="col-md-6 justify-content-center">
          <label>Imagen</label>
          <div className=" d-flex justify-content-center align-items-center mt-2 ">
            <label htmlFor="hola">
              {
                urlImg == null ?
                <div className="material-icons border radius-10">
                  <div className="img">camera_alt</div>
                </div>
                :<img style = {{
                  maxWidth: 100,
                  maxHeight: 100
                }} src = {urlImg}/>
              }

            </label>
          </div>
          <div className="justify-Content-center flie">
            <input onChange = {(e)=>loadImage(e)}className="file" id = "hola" type="file"/>
          </div>
        </div>
        {
          !loadingSave ?
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-warning mr-2" onClick = {()=>props.goback()} >Regresar</button>
            <button className="btn btn-success" onClick = {()=>save_films()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button v-if="id" className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }

    </div>

  </div>

);
}

export default New_Prod;

if (document.getElementById('New_Prod')) {
  ReactDOM.render(<New_Prod />, document.getElementById('New_Prod'))
}
