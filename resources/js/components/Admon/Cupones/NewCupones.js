import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import Compressor from 'compressorjs';
import $ from 'jquery';
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import AsyncSelect from 'react-select/async';

function NewCupones(props) {
  const [showForm, setShowForm ] = useState(1)
  const [nombre, setNombre] = useState('')
  const [cupon, setCupon] = useState('')
  const [descuento, setDescuento] = useState('')
  const [loading, setLoading] = useState(true)
  const [loadingSave, setLoadingSave] = useState(false)
  const [categoria, setCategoria] = useState(null)
  const [categorialist, setCategoriaslist] = useState(null)
  const [categoriasSelect,  setCategoriasSelect] = useState([])
  const [productolist, setProductolist] = useState(null)
  const [porductoSelect,  setporductoSelect] = useState([])
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [selecciona, setSelecciona] = useState(null)
  const [select, setSelect] = useState([
    {
      value:'1',
      label:'Categoría',
    },
    {
      value:'2',
      label:'Productos',
    },
    {
      value:'3',
      label:'Todos',
    }
  ])

  useEffect(() => {
    validarId()
    obtainCategories()
    obtain_productos()
  }, []);

  const obtainCategories = async () => {
    axios.get(ip+'admon/obtain_categories').then(response=>{
      var res = response.data
      var categorias = []

       res.map(item=>{
        const data = {
          value:item.cat_id,
          label:item.cat_nombre
        }
        categorias.push(data)
      })
      setCategoriaslist(categorias)

    })
  }

  const obtain_productos = () =>{
      axios.get(ip+'admon/obtain_productos').then(response=>{
        var res  = response.data;
        var producto = [];
        res.map(item=>{
          const data = {
            value:item.prod_id,
            label:item.prod_nombre
          }
          producto.push(data)
        })
        setProductolist(producto)

      })
  }

  const validarId = async () =>{
    console.log("holaaa");
    if(props.id > 0){
      console.log(props);
      await setNombre(props.data.name)
      await setCupon(props.data.cupon)
      await setDescuento(props.data.descuento)
      await setCategoriasSelect(props.data.categoria_all)
      await setporductoSelect(props.data.producto_all)
      if (props.data.todos == 1) {
        await setSelecciona({
            value:'1',
            label:'Categoría',
          })
          await setShowForm(2)
      }
      else if (props.data.todos == 2) {
        await setSelecciona({
            value:'2',
            label:'Productos',
          })
          await setShowForm(3)
      }
      else{
        await setSelecciona({
            value:'3',
            label:'Todos',
          })
      }


    }
    await setLoading(false)
  }




  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_films = async () => {
    console.log("entro");
    console.log(descuento);
    var message = ''
    var error = false
    if (nombre == '') {
      error = true
      message = "El nombre no puede ir vacío"
    }
    else if (cupon == ''){
      error = true
      message = "El cupon no puede ir vacío"
    }
    else if(descuento <= 0 && descuento > 100 || descuento == ''){
      error = true
      message = "El decuento tiene que estar entre 1 y 100%"
    }
    else if (selecciona == null) {
      error = true
      message = "Debes seleccionar tipo categoria o producto"

    }
    else if (selecciona == 1 && categoriasSelect.length>0) {
      console.log("entro cat");
      error = true
      message = "El tipo de categorias no puede ir vacìo"
    }
    else if (selecciona == 2 && porductoSelect.length>0) {
      console.log("entro pro");
      error = true
      message = "El tipo de producto no puede ir vacìo"
    }
    if (error) {
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id', props.id)
      data.append('nombre',nombre)
      data.append('cupon',cupon)
      data.append('descuento',descuento)
      data.append('productos',JSON.stringify(porductoSelect))
      data.append('categorias',JSON.stringify(categoriasSelect))
      data.append('todos',selecciona.value)
      axios.post(ip+'admon/guard_cupon',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Categoría creada correctamente")
          props.gobackSave()
        }
        else{
          MessageSuccess("Categoría editada correctamente")
          props.gobackSave()
        }
      })
    }

  }
  const escoje = async (e) => {
    await setCategoriasSelect([])
    await setporductoSelect([])
    console.log("entro funcion");
    console.log(e);
    await setSelecciona(e)

    if(e.value == '1'){
      setShowForm(2)
    }
    if(e.value == '2'){
      setShowForm(3)
    }
  }

  return (
    <div style = {{width:'100%'}}>
    {
      !loading &&
      <div  className="form-group">
        <div className="row">
          <div className="col-md-5">
            <label htmlFor="Nombre" className = "titulo-form">Nombre</label>
            <input value = {nombre} className="form-control" placeholder = "Nombre del cupón" maxLength = "70" onChange = {(e)=>setNombre(e.target.value)} id = "Nombre" type="text" name="nombre"/>
          </div>
          <div className="col-md-5">
            <label htmlFor="cupones" className = "titulo-form">Cupón</label>
            <input value = {cupon} className="form-control" placeholder = "Cupón" maxLength = "70" onChange = {(e)=>setCupon(e.target.value)} id = "cupon" type="text" name="cupon"/>
          </div>
          <div className="col-md-5">
            <label htmlFor="decuento" className = "titulo-form">Descuento</label>
            <input value = {descuento} className="form-control" placeholder = "Descuento %" maxLength = "3" onChange = {(e)=>setDescuento(e.target.value)} id = "cupon" type="number" name="cupon"/>
          </div>
          <div className="col-md-5">
            <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Selecciona el tipo de cupón</label>
            <Select
            value={selecciona}
            closeMenuOnSelect={false}
            components={animatedComponents}
            options={select}
            onChange={(e)=>escoje(e)}
            placeholder = "Selecciona los productos"
            name="colors"
              />
          </div>
         {
           showForm == 2 ?
           <div className="col-md-5">
             <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Categoría</label>
             <Select
             value={categoriasSelect}
             closeMenuOnSelect={false}
             components={animatedComponents}
             options={categorialist}
             onChange={(e)=>setCategoriasSelect(e)}
             isMulti
             placeholder = "Selecciona las categorías"
             name="colors"
               />
           </div>

           :showForm == 3 ?
           <div className="col-md-5">
             <label htmlFor="exampleFormControlTextarea1" className = "titulo-form">Productos</label>
             <Select
             value={porductoSelect}
             closeMenuOnSelect={false}
             components={animatedComponents}
             options={productolist}
             onChange={(e)=>setporductoSelect(e)}
             isMulti
             placeholder = "Seleccione el tipo"
             name="colors"
               />
           </div>

          :null
         }


      </div>

        {
          !loadingSave ?
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-warning mr-2" onClick = {()=>props.goback()} >Regresar</button>
            <button  className="btn btn-success" onClick = {()=>save_films()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }

    </div>
  }
  </div>

);
}

export default NewCupones;

if (document.getElementById('NewCupones')) {
  ReactDOM.render(<NewCupones />, document.getElementById('NewCupones'))
}
