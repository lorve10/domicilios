import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import ListCupo from './ListCupo';
import NewCupones from './NewCupones';
function Cupon() {
  const [loading, setLoading] = useState(true)
  const [showForm, setShowForm] = useState(1)
  const [cupon, setCupon] = useState([])
  const [data, setData] = useState({})
  const [id, setId] = useState(null)
  useEffect(() => {
    obtain_cupon()
  }, []);

  const obtain_cupon = async () => {
    axios.get(ip+'admon/obtain_cupon').then(response=>{
      setCupon(response.data)
      setLoading(false)
    })
  }

  const goback = () => {
    setShowForm(1)
    setId(null)
  }
  const gobackSave = () => {
    obtain_cupon()
    setLoading(true)
    setShowForm(1)
    setData({})
  }
  const dataUpdate = async (data) => {
    console.log(data);
    var name = data.nombre_cupon
    var cupon = data.cupon
    var descuento = data.descuento
    var todos = data.todos
    var producto_all = []
    var categoria_all = []
    if (data.categorias != null) {
      categoria_all = JSON.parse(data.categorias)
    }
    if(data.productos != null){
      producto_all = JSON.parse(data.productos)
    }
    const dataForAll = {
      name,
      cupon,
      descuento,
      todos,
      categoria_all,
      producto_all
    }
    await setId(data.id_cupon)
    await setData(dataForAll)
    await setShowForm(2)
    console.log(dataForAll);
  }

  return (
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>

          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i> Cupones</h4>
            </div>
            <div className = "card-body mx-3">
            {
              showForm == 1 &&
              <div className="row justify-content-end">
                <button onClick = {()=>setShowForm(2)} className="btn btn-outline-success round btn-min-width mr-1 mb-1">Agregar</button>
              </div>
            }
            <div className = "row padding-forms-admin">
              {
                showForm == 1 ?
                <ListCupo dataUpdate={(data)=>dataUpdate(data)}  cupon = {cupon} gobackSave = {()=>gobackSave()} />
                :showForm == 2 ?
                <NewCupones   goback = {()=>goback()} data={data} gobackSave = {()=>gobackSave()} id = {id} />
              :null
              }


              </div>
            </div>
          </div>
          </div>

      }
    </div>

);
}

export default Cupon;

if (document.getElementById('Cupon')) {
  ReactDOM.render(<Cupon/>, document.getElementById('Cupon'));
}
