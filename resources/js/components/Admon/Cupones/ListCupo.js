import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'
function ListCupo(props) {
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)

  const [cupones, setCupones] = useState([])
  const [backcupon, setBackcupon] = useState([])
  const [backcupon2, setBackcupon2] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [text, setText] = useState('')


  useEffect(() => {
    var cupones = props.cupon
    cupones.map(item=>{
      var categorias = JSON.parse(item.categorias);
      var productos = JSON.parse(item.productos);
      item.categorias_show = categorias
      item.productos_show = productos

    })
    setBackcupon(cupones)
    setBackcupon2(cupones)
    const dataNew = Pagination.paginate(cupones,currentPage,postsPerPage)
    setCupones(dataNew)
  }, []);

  const updateCurrentPage = async (number) => {
    await setCupones([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backcupon,number,postsPerPage)
    await setCupones(dataNew)
  }

  const searchInput = async(value) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    console.log(inputSearch);
    var newData = backcupon2.filter(function(item2){
      var name = ((item2.nombre_cupon).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }

      return res;
    })
    await setCurrentPage(1)
    await setBackcupon(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setCupones(dataNew)
    await setLoading(false)
  }
  const deshabilitar = async (value) => {
    var message = "¿Quieres eliminar este cupón?."
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
          data.append('id', value.cat_id)
        axios.post(ip+'admon/delete_categoria',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          props.gobackSave()
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  const editar = (data) => {
   props.dataUpdate(data)
  }

  return (
    <div className = "col-md-12">
      <div className = "row">
        <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #e4e4e4', borderRadius:20, backgroundColor:'white'}}>
          <input onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Digite el nombre del cupón"/>
          {
            text.length == 0 ?
            <span style = {{color:'#c3c3c3', cursor:'pointer'}}  className= "material-icons">search</span>
            :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
        }


      </div>

      { !loading &&

        cupones.map((item,index)=>{
          return (
            <div key = {index} className = "col-md-4 col-12 d-flex justify-content-center align-items-center">
              <div className  = "box-films-admon mb-3">
                <div className="dropdown dropright d-flex justify-content-end">
                  <a className="dropdown-toggle" style = {{color:'black'}} href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span className = "material-icons">more_vert</span>
                  </a>

                  <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a className="dropdown-item" onClick = {()=>editar(item)} href="#">Editar</a>
                    <a onClick = {()=>deshabilitar(item)} className="dropdown-item" href="#">Eliminar</a>

                  </div>
                </div>

                <div className = "pt-1 d-flex justify-content-center">
                  <p className = "title-film-admon">{item.nombre_cupon}</p>
                </div>
                <div className = "pt-1 d-flex justify-content-center ">
                  <p className = "title">{item.cupon} - {item.descuento}%</p>
                </div>
                <div>
                  {item.categorias_show.length>0 &&
                    <p className="title-film-admon">Categorías</p>
                  }
                  <div>
                {

                  item.categorias_show.map((item2,index2)=>{
                    return (
                      <p key = {index2} className = "m-0" style = {{textAlign:'center'}}>{item2.label}</p>
                    );
                  })

                }
                </div>
                {item.productos_show.length>0 &&
                <p className="title-film-admon">productos</p>
                }
                <div>
                {
                  item.productos_show.map((item3,index3)=>{
                    return (
                      <p key = {index3} className = "m-0"  style = {{textAlign:'center'}}>{item3.label}</p>
                    );
                  })

                }
                </div>


                </div>
              </div>
            </div>
          );
        })
      }


    </div>
  </div>
);
}


export default ListCupo;

if (document.getElementById('ListCupo')) {
  ReactDOM.render(<ListCupo />, document.getElementById('ListCupo'));
}
