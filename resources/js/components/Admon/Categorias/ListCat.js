import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../pagination/Pagination'
import PaginationButton from '../../pagination/Pagination-button'
function ListCat(props) {
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [categories, setCategories] = useState([])
  const [backCategories, setBackCategories] = useState(props.categories)
  const [backCategories2, setBackCategories2] = useState(props.categories)
  const [id, setId] = useState(null)
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [text, setText] = useState('')
  useEffect(() => {
    const dataNew = Pagination.paginate(backCategories,currentPage,postsPerPage)
    setCategories(dataNew)
  }, []);
  const updateCurrentPage = async (number) => {
    await setCategories([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backCategories,number,postsPerPage)
    await setCategories(dataNew)
  }
  const searchInput = async(value) => {
    await setText(value)
    await setLoading(true)
    const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
    var newData = backCategories2.filter(function(item2){
      var name = ((item2.cat_nombre).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g,"")
      var resName = name.indexOf(inputSearch) > -1
      var resNameLasT = name.indexOf(inputSearch) > -1
      var res = false
      if(resNameLasT||resNameLasT){
        res = true
      }

      return res;
    })
    await setCurrentPage(1)
    await setBackCategories(newData)
    const dataNew = Pagination.paginate(newData,1,postsPerPage)
    await setCategories(dataNew)
    await setLoading(false)
  }
  const deshabilitar = async (value) => {
    var message = 'Quieres eliminar esta categoría'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
          data.append('id', value.cat_id)
        axios.post(ip+'admon/delete_categoria',data).then(response=>{

          Swal.fire('Eliminado correctamente!', '', 'success')
          props.gobackSave()
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  const editar = (data) => {
   props.dataUpdate(data)
  }
  return (
    <div className = "col-md-12">
      <div className = "row">
        <div className="col-12 mt-5 mb-3 d-flex justify-content-between align-items-center" style={{height: 40, border:'1px solid #e4e4e4', borderRadius:20, backgroundColor:'white'}}>
          <input onChange = {(e)=>searchInput(e.target.value)} style = {{width:'inherit',border:'none', fontSize: 14, outlineStyle:'auto', outlineWidth:0}} value = {text} placeholder="Digite el nombre de la categoría"/>
          {
            text.length == 0 ?
            <span style = {{color:'#c3c3c3', cursor:'pointer'}}  className= "material-icons">search</span>
            :<span style = {{color:'#c3c3c3', cursor:'pointer'}}  onClick = {()=>searchInput('')} className= "material-icons-round">cancel</span>
        }


      </div>
      { !loading &&
        categories.map((item,index)=>{
          return (
            <div key = {index} className = "col-md-4 col-12 d-flex justify-content-center align-items-center">
              <div className  = "box-films-admon mb-3">
                <div className="dropdown dropright d-flex justify-content-end">
                  <a className="dropdown-toggle" style = {{color:'black'}} href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span className = "material-icons">more_vert</span>
                  </a>

                  <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a className="dropdown-item" onClick = {()=>editar(item)} href="#">Editar</a>
                    <a onClick = {()=>deshabilitar(item)} className="dropdown-item" href="#">Eliminar</a>

                  </div>
                </div>

                <div className = "d-flex justify-content-center">
                  <img src = {ip+item.cat_imagen} className = "imagen-admon-list"/>
                </div>

                <div className = "pt-1 d-flex justify-content-center">
                  <p className = "title-film-admon">{item.cat_nombre}</p>
                </div>
              </div>
            </div>
          );
        })
      }
      {
        !loading &&
        <div className="d-flex col-md-12 col-12 justify-content-end">
          <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backCategories.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
        </div>
      }

    </div>
  </div>
);
}

export default ListCat;

if (document.getElementById('ListCat')) {
  ReactDOM.render(<ListCat />, document.getElementById('ListCat'));
}
