import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import ListCat from './ListCat';
import New_Cat from './New-Cat';
function CategoriesAdmon() {
  const [loading, setLoading] = useState(true)
  const [showForm, setShowForm] = useState(1)
  const [categories, setCategories] = useState(1)
  const [id, setId] = useState(null)
  const [data, setData] = useState({})
  useEffect(() => {
    obtainCategories()
  }, []);
  const obtainCategories = async () => {
    axios.get(ip+'admon/obtain_categories').then(response=>{
      console.log(response.data);
      setCategories(response.data)
      setLoading(false)
    })
  }
  const goback = () => {
    setShowForm(1)
    setId(null)

  }
  const gobackSave = () => {
    setLoading(true)
    obtainCategories()
    setShowForm(1)
    setId(null)
    setData({})
  }
  const dataUpdate = async (data) => {
    console.log(data);

    var nombre = data.cat_nombre
    var url_img = data.cat_imagen

    const dataForAll = {
      nombre,
      url_img,
    }
    await setId(data.cat_id)
    await setData(dataForAll)
    await setShowForm(2)
    console.log(dataForAll);


  }

  return (
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i> Categorías</h4>
            </div>
            <div className = "card-body">
              {
                showForm == 1 &&
                <div className="row justify-content-end">
                  <button onClick = {()=>setShowForm(2)} className="btn btn-outline-success round btn-min-width mr-1 mb-1">Agregar</button>
                </div>
              }
              <div className = "row padding-forms-admin">
                {
                  showForm == 1 ?
                  <ListCat dataUpdate={(data)=>dataUpdate(data)} categories = {categories} gobackSave = {()=>gobackSave()}/>
                  :showForm == 2 ?
                  <New_Cat   goback = {()=>goback()} data={data}  gobackSave = {()=>gobackSave()} id = {id} />
                  :null
                }
              </div>
            </div>
          </div>
        </div>
      }
    </div>

);
}

export default CategoriesAdmon;

if (document.getElementById('CategoriesAdmon')) {
  ReactDOM.render(<CategoriesAdmon />, document.getElementById('CategoriesAdmon'));
}
