import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import Compressor from 'compressorjs';
import $ from 'jquery';

function New_Cat(props) {
  const [nombre, setNombre] = useState('')
  const [img, setImg] = useState({})
  const [urlImg, setUrlImg] = useState(null)
  const [loading, setLoading] = useState(false)
  const [loadingSave, setLoadingSave] = useState(false)

  useEffect(() => {
    validarId()
  }, []);

  const validarId = async () => {
    if (props.id>0) {
      console.log("entrooo a editar si");
      setNombre(props.data.nombre)
      setUrlImg(ip+props.data.url_img)
    }
  }


  const loadImage = (event) => {
    const file = event.target.files[0];
    if (file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg"){
      if (file.size <= 10000000) {
        new Compressor(file, {
          quality: 0.6,

          success(result) {
            var file = new File ([result], result.name, {type:result.type});
            setImg(file)
            console.log(file);
            let reader = new FileReader();
            reader.onload = e => {
              setUrlImg(e.target.result)
            };
            reader.readAsDataURL(file);
          }
        })
      }
      else{
        alert("imagen muy pesada")
      }
    }
    else{
      alert("Formato no valido")
    }
    console.log(file);

  }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_films = async () => {
    setLoadingSave(true)
    console.log(nombre);

    console.log(img);

    var message = ''
    var error = false
    if (nombre == '') {
      error = true
      message = "El nombre no puede ir vacío"
    }

    else if (urlImg == null) {
      error = true
      message = "Sube la imagen de la serie o película"
    }

    if (error) {
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('id', props.id)
      data.append('nombre',nombre)
      data.append('imagen',img)
      axios.post(ip+'admon/guard_categorie',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Categoría creada correctamente")
          props.gobackSave()
        }
        else{
          MessageSuccess("Categoría editada correctamente")
          props.gobackSave()
        }
      })
    }

  }
  return (
    <div style = {{width:'100%'}}>
      <div  className="form-group">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre</label>
            <input value = {nombre} className="form-control" placeholder = "Nombre de la categoría" maxLength = "70" onChange = {(e)=>setNombre(e.target.value)} id = "Nombre" type="text" name="nombre"/>
          </div>
        </div>
        <div className="col-md-6 pt-2 px-0">
          <label>Imagen</label>
          <div className=" d-flex justify-content-center align-items-center mt-2 ">
            <label htmlFor="hola">
              {
                urlImg == null ?
                <div className="material-icons border radius-10">
                  <div className="img">camera_alt</div>
                </div>
                :<img style = {{
                  maxWidth: 100,
                  maxHeight: 100
                }} src = {urlImg}/>
              }

            </label>
          </div>
          <div className="justify-Content-center flie">
            <input onChange = {(e)=>loadImage(e)}className="file" id = "hola" type="file"/>
          </div>
        </div>
        {
          !loadingSave ?
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-warning mr-2" onClick = {()=>props.goback()} >Regresar</button>
            <button  className="btn btn-success" onClick = {()=>save_films()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }

    </div>

  </div>

);
}

export default New_Cat;

if (document.getElementById('New_Cat')) {
  ReactDOM.render(<New_Cat />, document.getElementById('New_Cat'))
}
