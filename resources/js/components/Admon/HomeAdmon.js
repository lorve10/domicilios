import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../ApiRest';
import '../../../css/app.scss'
function HomeAdmon() {
  const [total_orden, setTotal_orden] = useState(0)
  const [ventas, setVentas] = useState(0)
  const [usuarios, setUsuarios] = useState(0)
  useEffect(() => {
  obtain_data()
  }, []);

  const obtain_data = () => {
    axios.get(ip+'admon/obtain/data/home').then(response=>{
      console.log(response);
      setUsuarios(response.data.registro_usuarios)
      setTotal_orden(response.data.total_orden.length)
      var total_ventas = 0;
      response.data.total_orden.map(item=>{
        total_ventas = total_ventas + (item.valor_total-item.total_descuento)
      })
      setVentas(total_ventas)
    })
  }

  const convertMoney = (amount) => {

  try {
    var thousands = "."
    var decimalCount = 0
    var decimal = "."
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log("Error de moneda "+e)
    return null
  }
}

  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-3 col-xs-6">

          <div className="small-box bg-aqua">
            <div className="inner">
              <h3>{total_orden}</h3>

              <p>Total Orden</p>
            </div>
            <div className="icon">
              <i className="ion ion-bag"></i>
            </div>
            <a href="http://3.238.228.135:3000" className="small-box-footer">Más información <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div className="col-lg-3 col-xs-6">

          <div className="small-box bg-green">
            <div className="inner">
              <h3><span className = "sub-precio">$</span>{convertMoney(ventas)}</h3>

              <p>Ventas</p>
            </div>
            <div className="icon">
              <i className="ion ion-social-usd"></i>
            </div>
            <a href="http://3.238.228.135:3000" className="small-box-footer">Más información <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div className="col-lg-3 col-xs-6">

          <div className="small-box bg-yellow" style = {{backgroundColor:'#e09900 !important'}}>
            <div className="inner">
              <h3>{usuarios}</h3>

              <p> Registros de usuarios</p>
            </div>
            <div className="icon">
              <i className="ion ion-person-add"></i>
            </div>
            <a href="http://3.238.228.135:3000" className="small-box-footer">Más información <i className="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

    </div>
  </div>
);
}

export default HomeAdmon;

if (document.getElementById('HomeAdmon')) {
  ReactDOM.render(<HomeAdmon />, document.getElementById('HomeAdmon'));
}
