import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2';
import Hora_valor from './Hora-valor';
import Restauran from './Restauran';
import Map from './Map';



function Info_rest() {
  const [load, setLoad] = useState(1)
  const [info, setInfo] = useState([])
  const [data, setData] = useState({})
  const [id, setId] = useState(null)
  const [loading, setLoading ] = useState(false)



  useEffect(() => {
    obtainInfo()
  }, []);

  const obtainInfo = async () => {
    axios.get(ip+'admon/obtain_info').then(response=>{
      var res = response.data;
      dataUpdate(response.data[0])
  })
}
const dataUpdate = async (data) => {
  console.log(data);

  var name = data.rest_name
  var nit  = data.rest_nit
  var phone = data.rest_phone
  var adress = data.rest_adress
  var departamento = data.departamento
  var municipio = data.municipio
  var open = data.open
  var close = data.close
  var costo = data.price_domici
  var valueM = data.value_min
  var reserve = data.reservation
  var propina = data.propina
  var cupoM = data.cupo_max
  var lng = data.longitude
  var lati = data.latitud

  const dataForAll = {
     name,
     nit,
     phone,
     adress,
     open,
     close,
     costo,
     valueM,
     reserve,
     propina,
     cupoM,
     lng,
     lati,
     departamento,
     municipio,

  }
  await setId(data.rest_id)
  await setData(dataForAll)
  console.log(dataForAll);


}


  return (
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0, padding:20}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
            <h4 className="form-section d-flex align-items-center "><span className="material-icons">settings</span> Configuracion General</h4>
            </div>
            <div className="nav">
              <div className="d-flex">
                <div className="nav-link">
                  <a className="" onClick={()=>setLoad(2)} > Horario y valor</a>
                </div>
                <div className="nav-link">
                  <a className="" onClick={()=>setLoad(3)}>Restaurante</a>
                </div>
                <div className="nav-link ">
                  <a className="" onClick={()=>setLoad(4)} >Mapa</a>
                </div>
              </div>
            </div>
            <div className = "card-body">
              <div className = "row padding-forms-admin">
              {
                  load == 2?
                  <Hora_valor id={id}  data={data} />
                  :load == 3?
                  <Restauran id={id}  data={data} />
                  :load == 4?
                  <Map id={id} data={data}  />
                  :null
              }
              </div>
            </div>
          </div>
        </div>
      }
    </div>

);
}

export default Info_rest;

if (document.getElementById('Info_rest')) {
  ReactDOM.render(<Info_rest />, document.getElementById('Info_rest'));
}
