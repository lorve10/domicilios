import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import $ from 'jquery';

function Hora_valor(props) {
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [open, setOpen] = useState('')
  const [close, setClose] = useState('')
  const [costo, setCosto] = useState('')
  const [valorM, setValorM] = useState('')
  const [cupoM, setCupoM] = useState('')
  const [propina, setPropina] = useState('')
  const [data, setData] = useState([])
  const [active,  setActive] =useState([])
  const [loadingSave, setLoadingSave] = useState(false)
  const [loading, setLoading] = useState(false)
  const [typeList, setTypeList] = useState([
    {
      value:'1',
      label:'Si',
    },
    {
      value:'2',
      label:'No',
    }
])


const [type, setType] = useState([])

  useEffect(() => {
    console.log("compilo");
    validateId()
  }, []);


  console.log(props.id);
  const validateId = () => {
    console.log("entro a la funcion");
    if (props.id > 0) {
      setOpen(props.data.open)
      setClose(props.data.close)
      setCosto(props.data.costo)
      setValorM(props.data.valueM)
      setCupoM(props.data.cupoM)
      setPropina(props.data.propina)
      if (props.data.reserve == 1) {
        const data = {
            value:'1',
            label:'Si',
          }
          setType(data)
      }
      else if (props.data.reserve == 2) {
        const data ={
          value:'2',
          label:'No',
        }
        setType(data)
      }

    }
  }

console.log(props.id);
  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_info = async () => {
    var message = ''
    var error = false
    if (open == null) {
      error = true
      message = "El campo de hora Abierto no puede ir vacío"
    }

    else if (close == null) {
      error = true
      message = "El campo de hora Cerrado no puede ir vacío"
    }
    else if (costo == null) {
      error = true
      message = "El campo de costo no puede ir vacío"
    }
    else if (valorM == null) {
      error = true
      message = "El campo Valor Maximo no puede ir vacío"
    }
    else if (type == null) {
      error = true
      message = "El campo de reservecion no puede ir vacío"
    }
    else if (cupoM == null) {
      error = true
      message = "El campo Cupo Maximo no puede ir vacío"
    }

    if (error) {
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('rest_id', props.id)
      data.append('name', props.data.name)
      data.append('nit', props.data.nit)
      data.append('phone',props.data.phone)
      data.append('adress', props.data.adress)
      data.append('departamento', props.data.departamento)
      data.append('municipio', props.data.municipio)
      data.append('lng', props.data.lng)
      data.append('lat', props.data.lati)
      data.append('open', open)
      data.append('close', close)
      data.append('costo', costo)
      data.append('valor_min', valorM)
      data.append('reserve', type.value)
      data.append('propina', propina)
      data.append('cupoM', cupoM)

      axios.post(ip+'admon/guard_info',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Hora y valor creada correctamente")
          window.location.reload();
        }
        else{
          MessageSuccess("Hora y valor editada correctamente")
            window.location.reload();
        }
      })
    }

  }
  return (
  <div style = {{width:'100%'}}>
    <div  className="form-group">
      <div className="container">
        <div className="col-md-6">
          <label htmlFor="Nombre" className = "titulo-form">Hora Abierto</label>
          <input value={open} className="form-control" placeholder = "hora de apertura" maxLength = "70" onChange = {(e)=>setOpen(e.target.value)} id="open" type="time"/>
        </div>
        <div className="col-md-6">
          <label htmlFor="Nombre" className = "titulo-form">Hora Cerrado</label>
          <input  value={close} className="form-control" placeholder = "hola de cierre" maxLength = "70" onChange = {(e)=>setClose(e.target.value)} id = "close" type="time" />
        </div>
        <div className="col-md-6">
          <label htmlFor="Nombre" className = "titulo-form">Valor minimo Domicilio</label>
          <input  value={costo} className="form-control" placeholder = "Costo domicilio" maxLength = "70" onChange = {(e)=>setCosto(e.target.value)} id = "costo" type="number" name="costo"/>
        </div>
        <div className="col-md-6">
          <label htmlFor="Nombre" className = "titulo-form">Valor maximo Domicilio</label>
          <input  value={valorM} className="form-control" placeholder = "Valor minimo" maxLength = "70" onChange = {(e)=>setValorM(e.target.value)} id = "valor" type="number" name="Valor"/>
        </div>
        <div className="col-md-6">
        <label>Reserva</label>
            <Select
              value={type}
              closeMenuOnSelect={false}
              components={animatedComponents}
              options={typeList}
              onChange={(e)=>setType(e)}
              placeholder = "Seleccione reserva"
              name="colors"
              />

        </div>
        <div className = "col-md-6">
        <label className = "titulo-form">Propinas (voluntarias)</label>
        <input className = "form-control" value ={propina}  onChange = {(e)=>setPropina(e.target.value)}  type = "number" placeholder = "propinas  (voluntarias) " />

        </div>
        <div className="col-md-6">
          <label htmlFor="Nombre" className = "titulo-form">cupo maximo para reservecion</label>
          <input  value={cupoM} className="form-control" placeholder = "Cupo maximo" min="1" max="3" onChange = {(e)=>setCupoM(e.target.value)} id = "cupo" type="number" name="cupo"/>
        </div>

      {
        !loadingSave ?
        <div className=" col-md-6 justify-content-center mt-4" style = {{float:'left'}}>
          <button v-if="id" className="btn btn-success" onClick = {()=>save_info()}>Guardar Cambios</button>
        </div>

        :<div className="col-md-6 justify-content-start " style = {{float:'left'}}>
        <button v-if="id" className="btn btn-primary">
          <div className="spinner-grow text-light" role="status">
            <span className="sr-only">Loading...</span>
          </div>

        </button>
      </div>
    }
    </div>
  </div>
</div>

);
}

export default Hora_valor;

if (document.getElementById('Hora_valor')) {
  ReactDOM.render(<Hora_valor />, document.getElementById('Hora_valor'))
}
