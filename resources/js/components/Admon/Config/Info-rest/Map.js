import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../../../css/app.css'
import {ip} from '../../../ApiRest';
import axios from 'axios'
import Swal from 'sweetalert2'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import L from 'leaflet';

function Map(props) {
  const [lat, setLat] = useState('')
  const [lng, setLng] = useState('')
  const [img, setImg] = useState({})
  const [urlImg, setUrlImg] = useState(null)
  const [loading, setLoading] = useState(false)
  const [loadingSave, setLoadingSave] = useState(false)


  useEffect(() => {
    validarId()
  }, []);
  const validarId = async () => {
    if (props.id>0) {
      console.log("entrooo a editar si");

      setLat(props.data.lati)
      setLng(props.data.lng)
      geosearchmap()

    }
    else{
      geosearchmap()
    }
  }

  const geosearchmap = async () =>{
      console.log("entrooo?");
      var map = L.map('map').setView([2.9335262287839843, -75.28930664062501], 10);

         L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
           attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
         }).addTo(map);
         var pump = null
         if (props.data.lati != null) {
           pump = L.marker([props.data.lati, props.data.lng], {draggable: true}).addTo(map);
           pump.on('drag', function (e) {
           console.log('pump drag', e);
           setLat(e.latlng.lat)
           setLng(e.latlng.lng)
            });
         }
         const provider = new OpenStreetMapProvider();
         const searchControl = new GeoSearchControl({
           provider: provider,
           showMarker: false,
           searchLabel: 'Busque la dirección en orden Ciudad, dirección y seleccione'
         });
         map.on('click', function(ev) {
           console.log(ev);
           setLat(ev.latlng.lat)
           setLng(ev.latlng.lng)
           var tempMarker = this;
           if (pump != null) {
             map.removeLayer(pump);
           }
           pump = L.marker([ev.latlng.lat, ev.latlng.lng], {draggable: true}).addTo(map);
           pump.on('drag', function (e) {
           console.log('pump drag', e);
           setLat(e.latlng.lat)
           setLng(e.latlng.lng)
            });

          });

        map.on('geosearch/showlocation', function(ev){
          console.log("entro a localizador");
          if (pump != null) {
            map.removeLayer(pump);
          }
          console.log(ev);
          pump = L.marker([ev.location.y, ev.location.x], {draggable: true}).addTo(map);
          setLat(ev.location.y)
          setLng(ev.location.x)
          pump.on('drag', function (e) {
          console.log('pump drag', e);
          setLat(e.latlng.lat)
          setLng(e.latlng.lng)
           });
        });
         map.addControl(searchControl);
    }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_cobertura = async () => {
    setLoadingSave(true)


    var message = ''
    var error = false


    if (lat == '') {
      error = true
      message = "Seleeciona en el mapa la ubicación"
    }

    if (error) {
      MessageError(message)
      setLoadingSave(false)
    }
    else{
      const data = new FormData()
      data.append('rest_id', props.id)
      data.append('name', props.data.name)
      data.append('nit', props.data.nit)
      data.append('phone',props.data.phone)
      data.append('adress', props.data.adress)
      data.append('departamento', props.data.departamento)
      data.append('municipio', props.data.municipio)
      data.append('open', props.data.open)
      data.append('close', props.data.close)
      data.append('costo', props.data.costo)
      data.append('propina', props.data.propina)
      data.append('valor_min', props.data.valueM)
      data.append('reserve', props.data.reserve)
      data.append('cupoM', props.data.cupoM)
      data.append('lat',lat)
      data.append('lng',lng)
      axios.post(ip+'admon/guard_info',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Cobertura creada correctamente")
        }
        else{
          MessageSuccess("Cobertura editada correctamente")
        }
      })
    }

  }

  return (
    <div className = "col-md-12">
      <div  className="form-group">
          <div className = "col-md-6 pt-3">
            <div id = "map" style = {{height:500, maxHeight:500}}>
            </div>
          </div>
        {
          !loadingSave ?
          <div className="my-5 d-flex col-md-3 justify-content-start px-0" style = {{float:'right'}}>
            <button  className="btn btn-success" onClick = {()=>save_cobertura()}>Guardar Cambios</button>
          </div>
          :null
      }

    </div>
    </div>

);
}

export default Map;

if (document.getElementById('Map')) {
  ReactDOM.render(<Map />, document.getElementById('Map'))
}
