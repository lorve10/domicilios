import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import AsyncSelect from 'react-select/async';
import Compressor from 'compressorjs';
import $ from 'jquery';

function Restauran(props) {
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [municipiolist, setMunicipiolist] = useState(null)
  const [muni, setMuni] = useState([])
  const [municipioSelect,  setMunicipioSelect] = useState([])
  const [departamentlist, setDepartamentlist] = useState(null)
  const [departamentSelect,  setDepartamentSelect] = useState([])
  const [name, setName] = useState('')
  const [nit, setNit] = useState('')
  const [phone, setPhone] = useState('')
  const [adress, setAdress] = useState('')
  const [departamento, setDepartamento] = useState('')
  const [municipio, setMunicipio] = useState('')
  const [loadingSave, setLoadingSave] = useState(false)
  const [loadingMuncipio, setLoadingMuncipio] = useState(false)

  useEffect(() => {
    obtainInfo()
    obtain_departamento()
    validateId()
  }, []);

  const obtainInfo = async () => {
    axios.get(ip+'admon/obtain_info').then(response=>{
      var res = response.data

  })
}
const validateId = () => {
  console.log("entro a la funcion");
  if (props.id > 0) {
    setName(props.data.name)
    setNit(props.data.nit)
    setPhone(props.data.phone)
    setAdress(props.data.adress)
  }
}


const obtain_departamento = async () => {
  axios.get(ip+'admon/obtain_departament').then(response=>{
    var res = response.data
    var departamento = [];
    res.map(item=>{
      const data = {
        value:item.id_departamento,
        label:item.departamento
      }
      departamento.push(data)
      setDepartamentlist(departamento)
    })
    var filtro = res.filter(e=>e.id_departamento == props.data.departamento);
    var depar = []
    filtro.map(item=>{
      const data = {
        value:item.id_departamento,
        label:item.departamento
      }
      obtain_municipio(item.id_departamento,false)
      depar.push(data)
      setDepartamentSelect(data)
    })

  })
}

const obtain_municipio = async (data,value) => {
      setMunicipioSelect([])
      axios.get(ip+'admon/obtain_municipio').then(response=>{
        var res = response.data
        var municipio =[]
        var filtro = response.data.filter(e=>e.departamento_id == data);
        var filtro2 = filtro.filter(e=>e.id_municipio == props.data.municipio);
        filtro.map(item=>{
          const data = {
            value:item.id_municipio,
            label:item.municipio
          }
          municipio.push(data)
        })
        setMunicipiolist(municipio)

        filtro2.map(item=>{
          const data = {
            value:item.id_municipio,
            label:item.municipio
          }
          setMunicipioSelect(data)
        })

      })
}




const departame = (value)=>{
    setDepartamentSelect(value);
    obtain_municipio(value.value, true)
}

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_info = async () => {

    var message = ''
    var error = false
    if (name == null) {
      error = true
      message = "El campo de nombre no puede ir vacío"
    }

    else if (nit == null) {
      error = true
      message = "El campo de Nit no puede ir vacío"
    }
    else if (phone == null) {
      error = true
      message = "El campo de Telefono no puede ir vacío"
    }

    else if (adress == null) {
      error = true
      message = "El campo de Direccion no puede ir vacío"
    }
    else if(municipioSelect == null){
      error = true
      message = "debes seleccionar un departamento"
    }


    if (error) {
      MessageError(message)
    }
    else{
      const data = new FormData()
      data.append('rest_id', props.id)
      data.append('open', props.data.open)
      data.append('close', props.data.close)
      data.append('costo', props.data.costo)
      data.append('valor_min', props.data.valueM)
      data.append('reserve', props.data.reserve)
      data.append('propina', props.data.propina)
      data.append('cupoM', props.data.cupoM)
      data.append('name', name)
      data.append('nit', nit)
      data.append('phone', phone)
      data.append('adress', adress)
      data.append('departamento', departamentSelect.value)
      data.append('municipio', municipioSelect.value)
      data.append('lat', props.data.lati)
      data.appped('lng', props.data.lng)
      axios.post(ip+'admon/guard_info',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Info creada correctamente")

          window.location.reload();
        }
        else{
          MessageSuccess("info editada correctamente")
          window.location.reload();
        }
      })
    }

  }
  return (

    <div className="col-md-12">
      <div  className="form-group">
        <div className="container">
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre Establecimiento</label>
            <input value = {name} className="form-control" placeholder = "Nombre del Establecimiento" maxLength = "70" onChange = {(e)=>setName(e.target.value)} id = "name" type="text" name="name"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nit</label>
            <input value = {nit} className="form-control" placeholder = "Nit de la Empresa" maxLength = "10" onChange = {(e)=>setNit(e.target.value)} id = "NitEmpresa" type="number" name="NitEmpresa"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Telefono</label>
            <input value = {phone} className="form-control" placeholder = "Telefono" maxLength = "10" onChange = {(e)=>setPhone(e.target.value)} id = "Telefono" type="number" name="Telefono"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Direccion</label>
            <input value = {adress} className="form-control" placeholder = "Direccion" maxLength = "30" onChange = {(e)=>setAdress(e.target.value)} id = "Direccion" type="text" name="Direccion"/>
          </div>
          <div className="col-md-6">
          <label htmlFor="Nombre" className = "titulo-form">Departamento</label>
          <Select
          value={departamentSelect}
          closeMenuOnSelect={true}
          components={animatedComponents}
          options={departamentlist}
          onChange={(e)=>departame(e)}
          placeholder = "Seleccione el departamento"
          name="suma"
          id ="suma"
            />
          </div>
          { !loadingMuncipio &&
            <div className="col-md-6">
              <label htmlFor="Nombre" className = "titulo-form">Ciudad</label>
              <Select
                value={municipioSelect}
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={municipiolist}
                onChange={(e)=>setMunicipioSelect(e)}
                placeholder = "Seleccione el municipio"
                name="colors"
                />
            </div>

          }

        {
          !loadingSave ?
          <div className=" col-md-6 justify-content-center mt-4" style = {{float:'left'}}>
            <button v-if="id" className="btn btn-primary" onClick = {()=>save_info()}>Guardar Cambios</button>
          </div>

          :<div className="col-md-6 justify-content-start mt-3" style = {{float:'left'}}>
          <button v-if="id" className="btn btn-primary">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }
      </div>

    </div>

  </div>
);
}

export default Restauran;

if (document.getElementById('Restauran')) {
  ReactDOM.render(<Restauran />, document.getElementById('Restauran'))
}
