import React, { useState, useEffect, useRef  } from 'react';
import ReactDOM from 'react-dom';
import '../../../../../css/app.css'
import {ip} from './../../../ApiRest'
import axios from 'axios'
import Swal from 'sweetalert2'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import L from 'leaflet';
import New_cobertur from './New-cobertur'
function CoberturaAdmon () {
  const [loading, setLoading] = useState(true)
  const [cobertura, setCobertura] = useState(true)
  const [paso, setPaso] = useState(1)
  const [data, setData] = useState({})
  const [id, setId] = useState(null)

  useEffect(() => {
    obtainCobertura()
  }, []);
  const obtainCobertura = () => {
    axios.post(ip+'admon/obtain_cobertura').then(response => {
      console.log(response.data);
      setPaso(1)
      setCobertura(response.data)
      setLoading(false)

      setMapa(response.data)
    })
  }
  const setMapa = async (data) => {
    console.log(data);
    // var container = L.DomUtil.get('map2');
    // if(container != null){
    //   container._leaflet_id = null;
    // }
    var map = L.map('map2').setView([data[0].corb_latitud, data[0].corb_longitude], 15);

    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    var pump = null
    if (pump != null) {
      map.removeLayer(pump);
    }
    data.map(item=>{
      pump = L.marker([item.corb_latitud, item.corb_longitude]).addTo(map);
      var circle = L.circle([item.corb_latitud, item.corb_longitude], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: item.corb_valor
      }).addTo(map);
      circle.bindPopup("Soy la cobertura "+item.corb_nombre );

    })



    // const provider = new OpenStreetMapProvider();
    // const searchControl = new GeoSearchControl({
    //   provider: provider,
    //   showMarker: false,
    //   searchLabel: 'Busque la dirección en orden Ciudad, dirección y seleccione'
    // });

  }

  const goback = async() => {
    await setId(null)
    await setPaso(1)
    await setMapa(cobertura)
  }
  const gobackSave = async() => {
    await setId(null)
    await obtainCobertura()
    await setPaso(1)
  }
  const delete_cobertura = async (data) => {
    var message = 'Quieres eliminar esta cobertura?'
    Swal.fire({
      title: message,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
    const dataform = new FormData()
    dataform.append('id',data.corb_id)
    axios.post(ip+'admon/delete_cobertura',dataform).then(response=>{
      setPaso(3)
      gobackSave()
      console.log(response);
    })
  }
  else{
    Swal.fire('Acción cancelada', '', 'info')

  }
  })
  }
  const edit_cobertura = async (data) => {
    setId(data.corb_id)
    const dataform = {
      nombre:data.corb_nombre,
      latitud:data.corb_latitud,
      longitud:data.corb_longitude,
      radio:data.corb_valor
    }
    setData(dataform)
    setPaso(2)
  }
  return(
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i> Cobertura</h4>
            </div>
            {
              paso == 1 ?
              <div className = "card-body">
                <div className = "row">

                  <div className = "col-md-10">
                    <table className="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">nombre</th>
                          <th scope="col">latitud</th>
                          <th scope="col">longitud</th>
                          <th scope="col">radio</th>
                          <th scope="col">Opciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          cobertura.map((item,index)=>{
                            return (
                              <tr key = {index}>
                                <th scope="row">{index}</th>
                                <td>{item.corb_nombre}</td>
                                <td>{item.corb_latitud}</td>
                                <td>{item.corb_longitude}</td>
                                <td>{item.corb_valor} mts</td>
                                <td>
                                  <div className = "d-flex">
                                    <i className="fas fa-edit" style = {{color:'#e09900', cursor:'pointer'}} onClick = {()=>edit_cobertura(item)}></i>
                                    <i className="fas fa-trash ml-2" style = {{color:'red', cursor:'pointer'}} onClick = {()=>delete_cobertura(item)}></i>
                                  </div>
                                </td>

                              </tr>
                            );
                          })
                        }


                      </tbody>
                    </table>

                  </div>
                  <div className = "col-md-2">
                    <button  className="btn btn-success" onClick = {()=>setPaso(2)}>Agregar nuevo</button>
                  </div>
                  <div className = "col-md-6 pt-3">
                    <div id = "map2" style = {{height:500, maxHeight:500}}>
                    </div>
                  </div>
                </div>
              </div>
              :
              <div className = "card-body">
                <New_cobertur goback = {()=>goback()} id = {id} gobackSave = {()=>gobackSave()} data = {data} />
              </div>
            }
          </div>
        </div>
      }
    </div>

  )
}
export default CoberturaAdmon;

if (document.getElementById('CoberturaAdmon')) {
  ReactDOM.render(<CoberturaAdmon />, document.getElementById('CoberturaAdmon'));
}
