import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../../../css/app.css'
import {ip} from './../../../ApiRest'
import axios from 'axios'
import Swal from 'sweetalert2'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import L from 'leaflet';

function New_cobertur(props) {
  const [nombre, setNombre] = useState('')
  const [rango, setRango] = useState('')
  const [lat, setLat] = useState('')
  const [lng, setLng] = useState('')
  const [img, setImg] = useState({})
  const [urlImg, setUrlImg] = useState(null)
  const [loading, setLoading] = useState(false)
  const [loadingSave, setLoadingSave] = useState(false)


  useEffect(() => {
    validarId()
  }, []);

  const validarId = async () => {
    if (props.id>0) {
      console.log("entrooo a editar si");
      setNombre(props.data.nombre)
      setRango(props.data.radio)
      setLat(props.data.latitud)
      setLng(props.data.longitud)
      geosearchmap()

    }
    else{
      geosearchmap()
    }
  }

  const geosearchmap = async () =>{
      console.log("entrooo?");
      var map = L.map('map').setView([2.9335262287839843, -75.28930664062501], 10);

         L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
           attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
         }).addTo(map);
         var pump = null
         if (props.id>0) {
           pump = L.marker([props.data.latitud, props.data.longitud], {draggable: true}).addTo(map);
           pump.on('drag', function (e) {
           console.log('pump drag', e);
           setLat(e.latlng.lat)
           setLng(e.latlng.lng)
            });
         }
         const provider = new OpenStreetMapProvider();
         const searchControl = new GeoSearchControl({
           provider: provider,
           showMarker: false,
           searchLabel: 'Busque la dirección en orden Ciudad, dirección y seleccione'
         });
         map.on('click', function(ev) {
           console.log(ev);
           setLat(ev.latlng.lat)
           setLng(ev.latlng.lng)
           var tempMarker = this;
           if (pump != null) {
             map.removeLayer(pump);
           }
           pump = L.marker([ev.latlng.lat, ev.latlng.lng], {draggable: true}).addTo(map);
           pump.on('drag', function (e) {
           console.log('pump drag', e);
           setLat(e.latlng.lat)
           setLng(e.latlng.lng)
            });

          });

        map.on('geosearch/showlocation', function(ev){
          console.log("entro a localizador");
          if (pump != null) {
            map.removeLayer(pump);
          }
          console.log(ev);
          pump = L.marker([ev.location.y, ev.location.x], {draggable: true}).addTo(map);
          setLat(ev.location.y)
          setLng(ev.location.x)
          pump.on('drag', function (e) {
          console.log('pump drag', e);
          setLat(e.latlng.lat)
          setLng(e.latlng.lng)
           });
        });
         map.addControl(searchControl);
    }

  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',
    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }
  const save_cobertura = async () => {
    setLoadingSave(true)


    var message = ''
    var error = false
    if (nombre == '') {
      error = true
      message = "El nombre no puede ir vacío"
    }

    else if (rango == '') {
      error = true
      message = "El rango no puede ir vacío"
    }
    else if (lat == '') {
      error = true
      message = "Seleeciona en el mapa la ubicación"
    }

    if (error) {
      MessageError(message)
      setLoadingSave(false)
    }
    else{
      const data = new FormData()
      data.append('id', props.id)
      data.append('nombre',nombre)
      data.append('rango',rango)
      data.append('lat',lat)
      data.append('lng',lng)
      axios.post(ip+'admon/guard_cobertura',data).then(response=>{
        if (props.id == null) {
          MessageSuccess("Cobertura creada correctamente")
          props.gobackSave()
        }
        else{
          MessageSuccess("Cobertura editada correctamente")
          props.gobackSave()
        }
      })
    }

  }

  return (
    <div className = "col-md-12">
      <div  className="form-group">

          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Nombre</label>
            <input value = {nombre} className="form-control" placeholder = "Nombre de la cobertura" maxLength = "70" onChange = {(e)=>setNombre(e.target.value)} id = "Nombre" type="text" name="nombre"/>
          </div>
          <div className="col-md-6">
            <label htmlFor="Nombre" className = "titulo-form">Rango (metros)</label>
            <input value = {rango} className="form-control" placeholder = "Rango de la cobertura" maxLength = "70" onChange = {(e)=>setRango(e.target.value)} id = "Rango" type="number" name="nombre"/>
          </div>
          <div className = "col-md-6 pt-3">
            <div id = "map" style = {{height:500, maxHeight:500}}>
            </div>
          </div>


        {
          !loadingSave ?
          <div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
            <button className="btn btn-warning mr-2" onClick = {()=>props.goback()} >Regresar</button>
            <button  className="btn btn-success" onClick = {()=>save_cobertura()}>Guardar Cambios</button>
          </div>
          :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
          <button className="btn btn-success">
            <div className="spinner-grow text-light" role="status">
              <span className="sr-only">Loading...</span>
            </div>

          </button>
        </div>
      }

    </div>
    </div>

);
}

export default New_cobertur;

if (document.getElementById('New_cobertur')) {
  ReactDOM.render(<New_cobertur />, document.getElementById('New_cobertur'))
}
