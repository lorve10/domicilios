import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../../ApiRest';
import Swal from 'sweetalert2';
import Compressor from 'compressorjs';

function Account() {
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [id, setId] = useState(null)
  const [email, setEmail] = useState(null)
  const [pass, setPass] = useState(null)
  const [password, setPassword] = useState('')
  const [password2, setPassword2] = useState('')

  useEffect(()=>{
      obtain_admin()
  },[]);

  const obtain_admin = async () => {
    axios.get(ip+'admon/obtain_admin').then(response=>{
      var res = response.data;
      response.data.map(item=>{
        setId(item.id)
      setEmail(item.email)
      })

      })
  }

  const validateEmail = async (data) => {
    var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    console.log("validacion email");
    console.log(regex.test(data));
    return regex.test(data);

  }

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
        })
    }
   const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
      })
  }

  const editEmail = async () => {
      var message = ''
      var error = false
      if(email == null){
        error = true
        message = "Sube la imagen del Logo"
      }
    else{
      const data = new FormData()
      data.append('id',id)
      data.append('email',email)
      axios.post(ip+'admon/cambiar',data).then(response=>{
        console.log(response);
      if (response.data) {
        MessageSuccess("Correo Editado")
     }
      else{
       MessageSuccess(" No se logro Editar")
     }
  })
}
}
const editpass = async () => {
    var message = ''
    var error = false
    if(pass == ''){
      MessageError("El campo de antigua contraseña no debe ir vacío")
    }
    if (password == '' && password2 == '') {
      MessageError("El campo de contraseña  no debe ir vacío")
    }
    else if (password.length<6) {
      MessageError("La contraseña no es correcta debe tener mínimo 6 caracteres")
    }
    else if (password != password2){
      MessageError("las contraseñas no coinciden")

    }
  else{
    const data = new FormData()
    data.append('id',id)
    data.append('email',email)
    data.append('pass',pass)
    data.append('password', password)
    axios.post(ip+'admon/recovery',data).then(response=>{
      console.log(response);
    if (response.data) {
      MessageSuccess("Contraseña Editada")
   }
    else{
     MessageSuccess(" No se logro Editar verifica tu antigua contraseña")
   }
})
}
}


  return (
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><span class="material-icons">settings</span> Cuenta</h4>
            </div>
            <div className = "card-body">
            <div className = "container">
            <div className = "col-md-6">
                <h5>Cambiar Correo</h5>
                <input value={email} className = "form-control"  onChange = {(e)=>setEmail(e.target.value)} type="email" placeholder = " Tu usuario o email " />
            </div>
            <div className=" col-md-6 justify-content-end mt-2" style = {{float:'right'}}>
              <button v-if="id" className="btn btn-success" onClick = {()=>editEmail()}>Edita Correo</button>
            </div>
              <div>
                <div className="col-md-6 mt-5">
                    <h5>Cambiar Contraseña</h5>
                    <input value={pass} className = "form-control" onChange = {(e)=>setPass(e.target.value)} type = "password" placeholder="escribe tu antigua contraseña"/>
                </div>
              </div>
              <div className="mt-3">
                  <div className = "col-md-6">
                      <input value={password} className = "form-control" onChange = {(e)=>setPassword(e.target.value)} type = "password" placeholder="escribe tu nueva contraseña"/>
                  </div>
              </div>
              <div className="mt-3">
                  <div className = "col-md-6">
                      <input value={password2} className = "form-control" onChange = {(e)=>setPassword2(e.target.value)} type = "password" placeholder="Repite tu contraseña"/>
                  </div>
              </div>
                  <div className=" col-md-6 justify-content-end mt-2" style = {{float:'right'}}>
                    <button v-if="id" className="btn btn-success" onClick = {()=>editpass()}>Edita Contraseña</button>
                  </div>
            </div>




            </div>

          </div>
        </div>
      }
    </div>

);
}

export default Account;

if (document.getElementById('Account')) {
  ReactDOM.render(<Account />, document.getElementById('Account'));
}
