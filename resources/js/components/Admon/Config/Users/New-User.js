import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { ip } from '../../../ApiRest';
import Swal from 'sweetalert2'
import axios from 'axios'
import makeAnimated from 'react-select/animated';
import Select from 'react-select'
import AsyncSelect from 'react-select/async';

function New_Users(props) {
  const [name, setName] = useState(props.name)
  const [email, setEmail] = useState(props.email)
  const [lastname, setLastname] = useState(props.lastname)
  const [phone, setPhone] = useState(props.phone)
  const [adress, setAdress] = useState(props.adress)
  const [document, setDocument] = useState(props.document)
  const [password, setPassword] = useState('')
  const [repetpassword, setRepetpassword] = useState('')
  const [typedocuments, setTypedocuments] = useState(null)
  const [typedocumentSelect, setTypedocumentSelect] = useState(null)
  const [typeuserslists, setTypeuserlists] = useState([])
  const [accesosList, setAccesosList] = useState([
    {
      value:'mesas',
      label:'Mesas'
    },
    {
      value:'pedidos',
      label:'Pedidos'
    },
    {
      value:'banners',
      label:'Banners'
    },
    {
      value:'catalogos',
      label:'Catalogos'
    },
    {
      value:'clientes',
      label:'Clientes'
    },
    {
      value:'inventario',
      label:'Inventario'
    },
    {
      value:'cupones',
      label:'Cupones'
    },


  ])

  const [accesosSelect, setAccesosSelect] = useState(props.accesosSelect)

  const [typeuserSelect, setTypeuserSelect] = useState(null)
  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)


  useEffect(() => {

    obtain_type_users()
    obtain_type_documents()

  }, []);


  const MessageError = async (data) => {
    Swal.fire({
      title: 'Error',
      text: data,
      icon: 'warning',

    })
  }
  const MessageSuccess = async (data) => {
    Swal.fire({
      text: data,
      icon: 'success',
    })
  }

  const obtain_type_documents = async () => {
    axios.get(ip + 'admon/obtain_type_documents').then(response => {
      var res = response.data
      var data = []
      var data2 = []
      var filtro = response.data.filter(e => e.id == props.typedocumentSelect)
      filtro.map(item => {
        const document = {
          value: item.id,
          label: item.nombre
        }
        data.push(document)
        setTypedocumentSelect(document)
      })


      console.log(document);
      res.map(item => {
        const array = {
          value: item.id,
          label: item.nombre,

        }
        data2.push(array)
      })
      setTypedocuments(data2)

    })
  }


  //listado de tipos de usuario
  const obtain_type_users = async () => {
    axios.get(ip + 'admon/obtain_type_users').then(response => {
      var res = response.data
      var data = []
      var data2 = []
      console.log(props.typeuserSelect);
      var filtro = res.filter(e => e.tip_id == props.typeuserSelect)
      console.log("este es el filtro");
      console.log(filtro);
      filtro.map(item => {
        const array = {
          value: item.tip_id,
          label: item.tip_name,

        }
        data.push(array)
        setTypeuserSelect(array)
      })
      res.map(item => {
        const array2 = {
          value: item.tip_id,
          label: item.tip_name,

        }
        data2.push(array2)
      })


      setTypeuserlists(data2)
    })

  }






  const save_User = async () => {
    console.log(typedocumentSelect);
    console.log(phone);
    console.log(password);
    console.log(repetpassword);
    var message = ''
    var error = false
    ///validaciones///
    if (name == '') {
      message = "Digite el nombre"
      error = true

    } else if (lastname == '') {
      message = "Digite el apellido"
      error = true

    } else if (email == '') {
      message = "Digite el email"
      error = true

    } else if (document == '') {
      message = "Digite el documento"
      error = true

    }
    else if (typedocumentSelect == null) {
      message = "Seleccione el tipo de documento"
      error = true
    }
    else if (adress == '') {
      message = "Digite su dirección"
      error = true

    }
    else if (phone == '') {
      message = "Digite su teléfono"
      error = true

    }
    else if (password == '') {
      message = "Digite una contraseña"
      error = true
    }
    else if (repetpassword == '') {
      message = "Digite la contraseña repetida"
      error = true
    }
    else if (password.length < 6) {
      message = "La contraseña no es correcta debe tener mínimo 6 caracteres"
      error = true
    }
    else if (password != repetpassword) {
      message = "las contraseñas no coinciden"
      error = true

    }
    else if (typeuserSelect == null) {
      message = "Tipo de usuario no seleccionado"
      error = true
    }
    else if (typeuserSelect.value == 2 && accesosSelect.length == 0) {
      message = "Accesos no seleccionados"
      error = true
    }



    if (error) {
      MessageError(message)
    }

    else {
      const data = new FormData()
      data.append('id', props.id)
      data.append('name', name)
      data.append('lastname', lastname)
      data.append('phone', phone)
      data.append('document', document)
      data.append('email', email)
      data.append('password', password)
      data.append('adress', adress)
      data.append('tipodocumento', typedocumentSelect.value)
      data.append('tipouser', typeuserSelect.value)
      data.append('accesos', JSON.stringify(accesosSelect))



      axios.post(ip + 'admon/guard_users', data).then(response => {
        if (props.id > 0) {
          MessageSuccess("Usuario editado correctamente")
        }
        else {
          MessageSuccess("Usuario creado correctamente")
        }

        props.resetUsers()
      })
    }

  }
  return (

    <div className="card-body card-dashboard">
      <h1 id="title">Formulario</h1>
      <div className="form-group">
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">Nombre</label>
            <input value={name} className="form-control" maxLength = "40" onChange={(e) => setName(e.target.value)} id="username" type="text" name="name" />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">apellidos</label>
            <input value={lastname} className="form-control" maxLength = "40" onChange={(e) => setLastname(e.target.value)} id="userlastname" type="text" name="lastname" />
          </div>

        </div>

        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">Correo electronico</label>
            <input value={email} className="form-control" maxLength = "90" onChange={(e) => setEmail(e.target.value)} id="useremail" type="email" name="email" />
          </div>

        </div>

        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">Numero de documento</label>
            <input value={document} className="form-control" maxLength = "11" onChange={(e) => setDocument(e.target.value)} id="userdoc" type="text" name="document" />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <label>Tipo de documento</label>
            <Select
              value={typedocumentSelect}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={typedocuments}
              onChange={(e) => setTypedocumentSelect(e)}
              placeholder="Seleccione el tipo de documento"
              name="selectUsers"
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">Direccion</label>
            <input value={adress} className="form-control" maxLength = "50" onChange={(e) => setAdress(e.target.value)} id="useradress" type="text" name="adress" />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">Telefono</label>
            <input value={phone} className="form-control" maxLength = "11" onChange={(e) => setPhone(e.target.value)} id="userphone" type="text" name="phone" />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">Contraseña</label>
            <input value={password} className="form-control" maxLength = "100" onChange={(e) => setPassword(e.target.value)} id="userpassword" type="password" name="password" />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <label htmlFor="projectinput1">Repetir Contraseña</label>
            <input value={repetpassword} className="form-control" maxLength = "100" onChange={(e) => setRepetpassword(e.target.value)} id="userrepitpassword" type="password" name="repetpassword" />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6">
            <label>Tipo de Usuario</label>
            <Select
              value={typeuserSelect}
              closeMenuOnSelect={true}
              components={animatedComponents}
              options={typeuserslists}
              onChange={(e) => setTypeuserSelect(e)}
              placeholder="Seleccione el tipo de usuario"
              name="selectUsers"
            />
          </div>
        </div>
        {
          typeuserSelect!= null && typeuserSelect.value == 2 &&
          <div className="row">
            <div className="col-md-6">
              <label>Accesos</label>
              <Select
                isMulti
                value={accesosSelect}
                closeMenuOnSelect={true}
                components={animatedComponents}
                options={accesosList}
                onChange={(e) => setAccesosSelect(e)}
                placeholder="Seleccione los accesos"
                name="selectUsers"
                />
            </div>
          </div>
        }


        <div className="my-2 d-flex col-md-3 justify-content-end px-0" style={{ float: 'right' }}>
          <button className="btn btn-warning mr-2" onClick={() => props.goback()} >Regresar</button>
          <button v-if="id" className="btn btn-success" onClick={() => save_User()}>Guardar Cambios</button>
        </div>
      </div>

    </div>
  );
}

export default New_Users;

if (document.getElementById('New_Users')) {
  ReactDOM.render(<New_Users />, document.getElementById('New_Users'));
}
