import React, { useState, useEffect } from 'react';
import New_Users from './New-User';
import ReactDOM from 'react-dom';
import { ip } from '../../../ApiRest';
import Swal from 'sweetalert2'
import Pagination from '../../../pagination/Pagination'
import PaginationButton from '../../../pagination/Pagination-button'

function Users() {
    const [loading, setLoading] = useState(false)
    const [showForm, setShowForm] = useState(1)
    const [usuarios, setUsuarios] = useState([])
    const [usuarioBack, setUsuarioBack] = useState([])
    const [userBack, setUserBack] = useState([])
    const [id, setId] = useState(null)
    const [name, setName] = useState('')
    const [lastname, setLastname] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [adress, setAdress] = useState('')
    const [accesosSelect, setAccesosSelect] = useState([])
    const [typedocumentSelect, setTypedocumentSelect] = useState(null)
    const [typeuserSelect, setTypeuserSelect] = useState(null)
    const [document, setDocument] = useState('')
    const [password, setPassword] = useState('')
    const [lisUser, setLisUser] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage, setPostsPerPage] = useState(5)
    const [text, setText] = useState('')
    useEffect(() => {

        obtain_users();

    }, []);


    const updateCurrentPage = async (number) => {
        await setLisUser([])
        await setCurrentPage(number)
        const dataNew = Pagination.paginate(usuarioBack, number, postsPerPage)
        await setLisUser(dataNew)
    }
    //volver
    const goback = async () => {
        setId(null)
        setName('')
        setLastname('')
        setEmail('')
        setDocument('')
        setPassword('')
        setPhone('')
        setAdress('')
        setAccesosSelect([])
        setTypedocumentSelect('')
        setTypeuserSelect('')
        var view = showForm
        view = view - 1
        setShowForm(view)
    }

    // parametros de busqueda
    const searchInput = async (value) => {
        setText(value)
        const inputSearch = (value.toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g, "")
        var newData = lisUser.filter(function (item2) {
            var name = ((item2.user_name).toUpperCase()).normalize('NFD').replace(/[\u0300-\u036f]/g, "")
            var resName = name.indexOf(inputSearch) > -1
            var resNameLasT = name.indexOf(inputSearch) > -1
            var res = false
            if (resNameLasT || resNameLasT) {
                res = true
            }
            return res;
        })
        setLisUser(newData)
    }

    const convertDocument = (value) => {
        if (value == 1) {
            return "R.C"
        }
        if (value == 2) {
            return "T.I";
        }
        if (value == 3) {
            return "C.C"
        }

        if (value == 4) {
            return "C.E"
        }

        if (value == 5) {
            return "P.E"
        }

        if (value == 6) {
            return "M.S"
        }

        if (value == 7) {
            return "A.S"
        }

    }

    const convertUser = (value) => {
        if (value == 1) {
            return "Repartidor"
        }
        if (value == 2) {
            return "Trabajador";
        }

    }

    //listando los usuarios
    const obtain_users = async () => {
        axios.get(ip + 'admon/obtain_users').then(response => {
            setId(null)
            setName('')
            setUsuarioBack(response.data);
            setShowForm(1)
            setUserBack(response.data);
            const dataNew = Pagination.paginate(response.data, currentPage, postsPerPage)
            setLisUser(dataNew)

        })
    }
      //eliminar usuarios
    const deshabilitar = async (value) => {
        Swal.fire({
            title: '¿Quieres eliminar este usuario?',
            showDenyButton: true,
            confirmButtonText: `Sí`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                const data = new FormData()
                data.append('id', value)
                axios.post(ip + 'admon/delete_users', data).then(response => {
                    obtain_users()
                    Swal.fire('Eliminado correctamente!', '', 'success')
                })

            } else if (result.isDenied) {
                Swal.fire('Acción cancelada', '', 'info')
            }
        })
    }



       //llevar datos al formulario
    const edit = async (index) => {

        var data = lisUser[index]

        setName(data.user_name)
        setLastname(data.user_lastname)
        setEmail(data.user_email)
        setDocument(data.user_document)
        setTypedocumentSelect(data.tip_document)
        setTypeuserSelect(data.tip_user)
        setAdress(data.user_adress)
        setId(data.user_id)
        if (data.accesos != null) {
        setAccesosSelect(JSON.parse(data.accesos))
        }
        setPhone(data.user_phone)
        setShowForm(2)
    }



    return (
        <div id='app'>
            {
                showForm == 1 &&
                <h1 id="title">Listado de Usuarios</h1>

            }
            <div className="card" style={{ borderRadius: 10 }}>
                {
                    showForm == 1 &&
                    <div className="mt-5" style={{ marginLeft: 25 }}>

                        <button id="btnNuevo" onClick={() => setShowForm(2)} className="btn btn-success btn btn-large"><i className="material-icons">person</i>Crear nuevo Usuario</button>

                    </div>
                }

                {
                    showForm == 1 &&
                    <div id="search" className="col-12 mt-5 mb-3 d-flex justify-content align-items-center" style={{ height: 40, border: '1px solid #e4e4e4', borderRadius: 20, backgroundColor: 'white' }}>
                        <input onChange={(e) => searchInput(e.target.value)} style={{ width: 'inherit', border: 'none', fontSize: 14, outlineStyle: 'auto', outlineWidth:0 }} value={text} placeholder="Nombre del Usuario" />
                        {
                            text.length == 0 ?
                                <span style={{ color: '#c3c3c3', cursor: 'pointer' }} className="material-icons">search</span>
                                : <span style={{ color: '#c3c3c3', cursor: 'pointer' }} onClick={() => searchInput('')} className="material-icons-round">cancel</span>
                        }
                    </div>
                }
                <div>

                    {
                        showForm == 1 &&
                        <table className=" table-bordered table table-striped">
                            <thead>
                                <tr className="table-warning">
                                    <th>Nombre</th>
                                    <th>Apellidos</th>
                                    <th>Correo Electronico</th>
                                    <th>Tipo de documento</th>
                                    <th>Numero de documento</th>
                                    <th>Dirreccion</th>
                                    <th>Telefono</th>
                                    <th>Rol</th>
                                    <th>Opciones</th>
                                </tr>
                                {
                                    lisUser.map((item, index) => {
                                        return (
                                            <tr key={item.user_id}>

                                                <td>{item.user_name}</td>
                                                <td>{item.user_lastname}</td>
                                                <td>{item.user_email}</td>
                                                <td>{convertDocument(item.tip_document)}</td>
                                                <td>{item.user_document}</td>
                                                <td>{item.user_adress}</td>
                                                <td>{item.user_phone}</td>
                                                <td>{convertUser(item.tip_user)}</td>
                                                <td>
                                                    <div className="d-flex">
                                                        <button onClick={() => edit(index)} className="btn btn-warning mr-2"><i className="material-icons">edit</i></button>
                                                        <button onClick={() => deshabilitar(item.user_id)} className="btn btn-danger"><i className="material-icons">delete</i></button>
                                                    </div>
                                                </td>

                                            </tr>
                                        );
                                    })

                                }

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    }
                    {
                        !loading && showForm == 1 &&

                        <div className="d-flex col-md-12 col-12 justify-content-fluid">
                            <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={usuarioBack.length} updateCurrentPage={(number) => updateCurrentPage(number)} />
                        </div>
                    }
                </div>
                {
                    showForm == 2 ?
                        <div>
                            <New_Users accesosSelect = {accesosSelect} lastname={lastname} name={name} email={email} adress={adress} phone={phone} document={document} typedocumentSelect={typedocumentSelect} typeuserSelect = {typeuserSelect}  id={id} goback={() => goback()} resetUsers={() => obtain_users()} />
                        </div>
                        : null
                }

            </div>
        </div>




    );

}
export default Users;
if (document.getElementById('Users')) {
    ReactDOM.render(<Users />, document.getElementById('Users'));
}
