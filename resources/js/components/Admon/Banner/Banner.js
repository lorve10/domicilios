import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import {ip} from '../../ApiRest';
import Swal from 'sweetalert2';
import Compressor from 'compressorjs';

function Banners() {
  const [loading, setLoading] = useState(false)
  const [showForm, setShowForm] = useState(1)
  const [id, setId] = useState(null)
  const [img, setImg] = useState({})
  const [img2, setImg2] = useState({})
  const [img3, setImg3] = useState({})


  const [urlImg, setUrlImg] = useState(null)
  const [urlImg2, setUrlImg2] = useState(null)
  const [urlImg3, setUrlImg3] = useState(null)
  const [data, setData] = useState({})

  useEffect(() => {
    obtainImg()
  }, []);
  const obtainImg = async () => {
    axios.get(ip+'admon/obtain_img').then(response=>{

      response.data.map(item=>{
        setId(item.id_imagen)
        if (item.logo != null) {
        setUrlImg(ip+item.logo)
        }
        if (item.portada != null) {
        setUrlImg2(ip+item.portada)
        }
        if (item.otra != null) {
        setUrlImg3(ip+item.otra)
        }

      })


    })
  }


    const loadImage = (event) => {
      const file = event.target.files[0];
      if (file.type == "image/png" || file.type == "image/jpg" || file.type == "image/jpeg"){
        if (file.size <= 10000000) {
          new Compressor(file, {
            quality: 0.6,

            success(result) {
              var file = new File ([result], result.name, {type:result.type});
              setImg(file)
              let reader = new FileReader();
              reader.onload = e => {
                setUrlImg(e.target.result)
              };
              reader.readAsDataURL(file);
            }
          })
        }
        else{
          alert("imagen muy pesada")
        }
      }
      else{
        alert("Formato no valido")
      }

    }

        const loadImagen2 = (event) => {
          const fil = event.target.files[0];
          if (fil.type == "image/png" || fil.type == "image/jpg" || fil.type == "image/jpeg"){
            if (fil.size <= 10000000) {
              new Compressor(fil, {
                quality: 0.6,

                success(result) {
                  var fil = new File ([result], result.name, {type:result.type});
                  setImg2(fil)
                  let reader = new FileReader();
                  reader.onload = e => {
                    setUrlImg2(e.target.result)
                  };
                  reader.readAsDataURL(fil);
                }
              })
            }
            else{
              alert("imagen muy pesada")
            }
          }
          else{
            alert("Formato no valido")
          }

        }

        const loadImagen3 = (event) => {
          const data = event.target.files[0];
          if (data.type == "image/png" || data.type == "image/jpg" || data.type == "image/jpeg"){
            if (data.size <= 10000000) {
              new Compressor(data, {
                quality: 0.6,

                success(result) {
                  var data = new File ([result], result.name, {type:result.type});
                  setImg3(data)
                  let reader = new FileReader();
                  reader.onload = e => {
                    setUrlImg3(e.target.result)
                  };
                  reader.readAsDataURL(data);
                }
              })
            }
            else{
              alert("imagen muy pesada")
            }
          }
          else{
            alert("Formato no valido")
          }

        }
        const MessageError = async (data) => {
          Swal.fire({
            title: 'Error',
            text: data,
            icon: 'warning',
          })
        }
        const MessageSuccess = async (data) => {
          Swal.fire({
            text: data,
            icon: 'success',
          })
        }

        const save_img = async () => {
          var message = ''
          var error = false
          if(urlImg2 == null){
            error = true
            message = "Sube la imagen del Logo"
          }
          else if (urlImg == null) {
            error = true
            message = "Sube la imagen de la Portada"
          }
          else{
            const data = new FormData()
            data.append('id',id)
            data.append('logo',img)
            data.append('portada',img2)
            data.append('otra',img3)
            axios.post(ip+'admon/guard_img',data).then(response=>{
              if (id == null) {
                MessageSuccess("creada correctamente")
              }
              else{
                MessageSuccess(" editada correctamente")
              }
            })
          }

        }
        const eliminar1 = () => {
          axios.post(ip+'admon/eliminar/foto/1').then(response => {
            setUrlImg(null)
          })
        }
        const eliminar2 = () => {
          axios.post(ip+'admon/eliminar/foto/2').then(response => {
            setUrlImg2(null)
          })
        }
        const eliminar3 = () => {
          axios.post(ip+'admon/eliminar/foto/3').then(response => {
            setUrlImg3(null)
          })
        }

  return (
    <div >
      {
        !loading &&
        <div className="content-wrapper" style={{minHeight:600, marginLeft:0}}>
          <div className="card" style={{borderRadius:10}}>
            <div className="card-header">
              <h4 className="form-section d-flex align-items-center"><i className="nav-icon fas fa-edit mr-2" ></i> Banners</h4>
            </div>
            <div className = "card-body">

            <div className="col-md-6 justify-content-center">
                <label>Banner 1</label>
                    <div className=" d-flex justify-content-center  mt-2 ">

                      <label htmlFor="hola">
                        {
                          urlImg == null ?
                          <div className="material-icons border radius-10">
                            <div className="img">camera_alt</div>
                            </div>
                            :
                            <div className = "d-flex">
                            <img style = {{
                              maxWidth: 100,
                              maxHeight: 100
                            }} src = {urlImg}/>

                            </div>
                          }

                        </label>
                        <span className = "material-icons ml-3" onClick = {()=>eliminar1()} style = {{cursor:'pointer', color:'red'}}>delete</span>
                      </div>
                      <div className="justify-Content-center flie">
                        <input onChange = {(e)=>loadImage(e)}className="file" id = "hola" type="file"/>
                      </div>
                    </div>

              <div className="col-md-6 justify-content-center">
                  <label>Banner 2</label>
                      <div className=" d-flex justify-content-center align-items-center mt-2 ">
                        <label htmlFor="pepe">
                          {
                            urlImg2 == null ?
                            <div className="material-icons border radius-10">
                              <div className="img">camera_alt</div>
                              </div>
                              :<div className = "d-flex">
                              <img style = {{
                                maxWidth: 100,
                                maxHeight: 100
                              }} src = {urlImg2}/>
                            <span className = "material-icons ml-3" onClick = {()=>eliminar2()} style = {{cursor:'pointer', color:'red'}}>delete</span>
                              </div>
                            }

                          </label>
                        </div>
                        <div className="justify-Content-center flie">
                          <input onChange = {(e)=>loadImagen2(e)} className="file" id = "pepe" type="file"/>
                        </div>
                      </div>

                  <div className="col-md-6 justify-content-center">
                          <label>Banner 3</label>
                      <div className=" d-flex justify-content-center align-items-center mt-2 ">
                            <label htmlFor="lorve">
                                {
                                  urlImg3 == null ?
                                  <div className="material-icons border radius-10">
                                  <div className="img">camera_alt</div>
                                  </div>
                                  :<div className = "d-flex">
                                  <img style = {{
                                    maxWidth: 100,
                                    maxHeight: 100
                                  }} src = {urlImg3}/>
                                <span className = "material-icons ml-3" onClick = {()=>eliminar3()} style = {{cursor:'pointer', color:'red'}}>delete</span>
                                  </div>
                                  }

                                </label>
                              </div>
                          <div className="justify-Content-center flie">
                        <input onChange = {(e)=>loadImagen3(e)}className="file" id = "lorve" type="file"/>
                      </div>
                    </div>




            </div>
            {
              !loading ?
              <div className="my-2 d-flex col-md-4 justify-content-end px-0" style = {{float:'right'}}>
                <button v-if="id" className="btn btn-success" onClick = {()=>save_img()}>Guardar Cambios</button>
              </div>
              :<div className="my-2 d-flex col-md-5 justify-content-end px-0" style = {{float:'right'}}>
              <button v-if="id" className="btn btn-success">
                <div className="spinner-grow text-light" role="status">
                  <span className="sr-only">Loading...</span>
                </div>

              </button>
            </div>
          }
          </div>
        </div>
      }
    </div>

);
}

export default Banners;

if (document.getElementById('Banners')) {
  ReactDOM.render(<Banners />, document.getElementById('Banners'));
}
