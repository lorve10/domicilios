/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/Example');
require('./components/Notifications');
require('./components/Login/LoginAdmon');
require('./components/Admon/HomeAdmon');
require('./components/Admon/Categorias/Categories');
require('./components/Admon/Productos/Products');
require('./components/Admon/Tables/Tables');
require('./components/Admon/Config/Cobetur/Cobertur');
require('./components/Admon/Config/Info-rest/Info-rest');
require('./components/Admon/Config/Users/Users');
require('./components/Admon/config/Cuenta/Account');
require('./components/Admon/Banner/Banner');
require('./components/Admon/Adicionales/Additional');
require('./components/Admon/Clientes/ListClient');
require('./components/Admon/Cupones/Cupones');
require('./components/Admon/PedidosDomicilio/Pedidos');
require('./components/Admon/PedidosMesas/Pedidos');
require('./components/Admon/PedidosTerminados/Pedidos');
require('./components/Admon/Inventario/Terceros/Terceros');
require('./components/Admon/Inventario/Productos/ProductsInventario');
