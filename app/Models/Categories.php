<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
  use HasFactory;
    protected $table='categoria';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id de la categoria
        'cat_id',
        //nombre
        'cat_nombre',
        //imagen
        'cat_imagen',
        //color
        'cat_color',
        //eliminar
        'cat_del'



    ];
    public function adicionales()
    {
      return $this->hasMany('App\Models\Cat_Adi','id_cat','cat_id');
    }
}
