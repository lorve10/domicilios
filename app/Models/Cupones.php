<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cupones extends Model
{
    use HasFactory;
    protected $table='cupones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_cupon',
        'nombre_cupon',
        'cupon',
        'productos',
        'categorias',
        'descuento',
        'todos',
        'deleted',
    ];
}
