<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enters extends Model
{
    use HasFactory;

        protected $table='enters_products';


       protected $fillable = [

           'id',

           'descript',

           'fecha',

           'proov',

           'cantidad',

           'valor_unidad',

           'saldo',

           'valor_total_inv',

           'costo_pro',

           'valor',

           'type',

           'product',

           'deleted',

           'created_at',

       ];
       public function proveedor()
       {
         return $this->hasOne('App\Models\Terceros','user_id','proov');
       }
       public function producto()
       {
         return $this->hasOne('App\Models\ProductsInventario','prod_id','product');
       }

}
