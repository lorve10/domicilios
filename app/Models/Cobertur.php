<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cobertur extends Model
{
    use HasFactory;
    protected $table='cobertura';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'corb_id',
        'corb_nombre',
        'corb_latitud',
        'corb_longitude',
        'corb_radio',
        'corb_valor',
    ];
}
