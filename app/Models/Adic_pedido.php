<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adic_pedido extends Model
{
  use HasFactory;
    protected $table='adic_pedido';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'id_adicpedido',

        'id_pedido',

        'id_producto',

        'id_adic',

    ];
    public function adicional()
    {
      return $this->hasOne('App\Models\Adicional','id_adicional','id_adic');
    }


}
