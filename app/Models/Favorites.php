<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    use HasFactory;
    protected $table='user_fav';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_fav',
        'id_user',
        'id_product',
    ];

    public function productos()
    {
      return $this->hasOne('App\Models\Products','prod_id','id_product');

    }
}
