<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TokenFcm extends Model
{
    use HasFactory;
    protected $table='token_fcm';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tdu_id',
        'tdu_user',
        'tdu_token'
    ];
}
