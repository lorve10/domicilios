<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat_Adi extends Model
{
  use HasFactory;
    protected $table='cat_adicional';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id de la cat and add
        'id_cat_add',
        //// id de la categoria
        'id_cat',
         // id de la adicional
         'id_add'
         ////
    ];

    public function categoria(){
      return $this->hasOne('App\Models\Categories','cat_id','id_cat');
    }

    public function adicional(){
      return $this->hasOne('App\Models\Adicional','id_adicional','id_add')->where('delete',0);
    }




}
