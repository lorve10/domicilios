<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Users extends Authenticatable implements MustVerifyEmail
{
    use HasFactory;
    use Notifiable;

    protected $table='users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      //Id  user
        'user_id',
        'accesos',
        //nombre del usuario
        'user_name',
        //apellido del usuario
        'user_lastname',
        //email usuario
        'user_email',
        //password
        'user_adress',
        //telefono del usuario
        'user_phone',
       // tipo de usuario
        'tip_user',
        //contraseña
        'password',
        //latitud
        'latitud',
        //longitud
        'longitud',
        //documento
        'user_document',
       //tipo de documento
        'tip_document',
          //estado
          'delete'


    ];





}
