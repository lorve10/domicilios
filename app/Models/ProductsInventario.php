<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsInventario extends Model
{
    use HasFactory;

        protected $table='productos_inventario';


       protected $fillable = [
           //Id de la categoria
           'prod_id',
           //nombre
           'prod_nombre',
           //descripcion
           'prod_descp',
           //precio
           'unity',
            //Unidad de medida
            'amount',
            //Cantidad disponible
           'prod_proveedor',
           //proovedor
           'prod_visible',
           //estado
       ];
       public function proveedor()
       {
         return $this->hasOne('App\Models\Terceros','user_id','prod_proveedor');
       }

       

}
