<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prod_pedido extends Model
{
  use HasFactory;
    protected $table='prod_pedido';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'id_prodpedido',

        'id_pedido',

        'id_product',

        'cantidad',

        'valor_u',

        'valot_t',

        'comentario',

        'descuento',

    ];
    public function producto()
    {
      return $this->hasOne('App\Models\Products','prod_id','id_product');
    }

    public function adicionales()
    {
      return $this->hasMany('App\Models\Adic_pedido','id_producto','id_prodpedido');
    }


}
