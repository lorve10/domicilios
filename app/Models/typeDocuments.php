<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class typeDocuments extends Model
{
  use HasFactory;
    protected $table='tip_document';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id del tipo de documento
        'id',
        
        'denominacion',
         //estado
         'nombre'
    ];
    
   
}
