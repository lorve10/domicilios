<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User  as Authenticatable;
use App\Notifications\ResetPasswordNotification;

class Clients extends Authenticatable implements MustVerifyEmail
{
  use HasFactory;
    protected $table='clients';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id del usuario
        'id_usuario',
        //nombre
        'name_user',
        //email
        'email',
        //contraseña
        'password',
        //celular
        'celphone_user',
        //dirección
        'direc_user',
        //lotitud
        'lotitud',
        //longitud
        'longitud',
        //facebook_id
        'facebook_id',
        //google_id
        'google_id',
        //apple_id
        'apple_id',
        'created_at',
        'updated_at',




    ];
    protected $hidden = [
        'password',
    ];

}
