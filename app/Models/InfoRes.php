<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoRes extends Model
{
    use HasFactory;

        protected $table='info_restaurat';


       protected $fillable = [
           'rest_id',
           //id
           'rest_name',
           //nombre empresa
           'rest_nit',
           //nit
           'rest_phone',
           //telefono
           'rest_adress',
           //Direccion
           'open',
           //hora de Abierto
           'close',
           //hora de cerrado
           'price_domici',
           //precio domicilio
           'value_min',
           //valor minimo
           'reservation',
           //Reservacion
           'propina',
           //cobertura restaurante o negocio
           'cupo_max',
           //cupo Maximo
           'departamento',
           //departamento
           'municipio',
           //departamento
           'longitude',
           //longitud
           'latitud',
           //latitud
           'delete',

       ];

}
