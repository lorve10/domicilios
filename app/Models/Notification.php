<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
  use HasFactory;
    protected $table='notifications';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id de la categoria
        'id',
        //mensaje
        'message',
        //lectura del mensaje
        'read_message',
        // estado
        'type_message',
    ];

}
