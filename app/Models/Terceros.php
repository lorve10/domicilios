<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User  as Authenticatable;
use App\Notifications\ResetPasswordNotification;

class Terceros extends Authenticatable implements MustVerifyEmail
{
  use HasFactory;
    protected $table='terceros';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      //Id  user
        'user_id',
        'type_user',
        //nombre del usuario
        'user_name',
        'user_name_2',
        //apellido del usuario
        'user_lastname',
        'user_lastname_2',
        //email usuario
        'user_email',
        //password
        'user_adress',
        //telefono del usuario
        'user_phone',
        'ciudad',
        'departamento',
        //documento
        'user_document',
       //tipo de documento
        'tip_document',
          //estado
        'delete'


    ];
    protected $hidden = [
        'password',
    ];

}
