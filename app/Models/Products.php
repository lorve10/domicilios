<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

        protected $table='productos';


       protected $fillable = [
           //Id de la categoria
           'prod_id',
           //nombre
           'prod_nombre',
           //descripcion
           'prod_descp',
           //descripcion
           'prod_precio',
           //precio
           'prod_gastos',
           //gastos
           'prod_descuento',
           //descuento
           'prod_categoria',
           //categoria
           'prod_imagen',
           //imgen
           'prod_visible',
           //estado
       ];
       public function categoria()
       {
         return $this->hasOne('App\Models\Categories','cat_id','prod_categoria');
       }

}
