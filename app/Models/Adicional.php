<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adicional extends Model
{
  use HasFactory;
    protected $table='adicional';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id de la categoria
        'id_adicional',
        //nombre
        'name_add',
        //price
        'price_add',
        //delete
        'delete'
    ];
    public function data(){
      return $this->hasMany('App\Models\Cat_Adi','id_add','id_adicional');
      }
    ///    public function adicional(){
      //    return $this->hasMany('App\Models\Adicional','id_adicional','id_add')->where('delete',0);
        //  }
        //public function categoria(){
          //return $this->hasMany('App\Models\Categories','cat_id','id_cat')->where('delete',0);
        //}

}
