<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
  use HasFactory;
    protected $table='pedidos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'id_pedido',

        'direcc_pedido',

        'tel_pedido',

        'name_user',

        'token_pedido',

        'estado_pedido',

        'tipo_pedido',

        'cupon',

        'valor_total',

        'total_descuento',

        'id_user',

        'metodo_pago',

        'calification_service',

        'calification_food',

        'calification_time',

        'comentario_cal',

        'domiciliario_id',

        'deleted',
    ];

    public function productos()
    {
      return $this->hasMany('App\Models\Prod_pedido','id_pedido','id_pedido');
    }

    public function adicionales()
    {
      return $this->hasMany('App\Models\Adic_pedido','id_pedido','id_pedido');
    }
    public function usuario()
    {
      return $this->hasOne('App\Models\Clients','id_usuario','id_user');
    }
    public function domiciliario()
    {
      return $this->hasOne('App\Models\Users','user_id','domiciliario_id');
    }


}
