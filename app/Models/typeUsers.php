<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class typeUsers extends Model
{
  use HasFactory;
    protected $table='tip_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id de la categoria
        'tip_id',
        //nombre
        'tip_name',
         //estado
         'delete'
    ];
    
   
}
