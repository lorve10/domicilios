<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
  use HasFactory;
    protected $table='imagen';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Id imagen
        'id_imagen',
        //Imagen logo
        'logo',
        //Portada
        'portada',
        'otra',
    ];

}
