<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Log;
use App\Models\Admon;
use App\Models\Pedidos;
use App\Models\Categories;
use App\Models\Products;
use App\Models\Tables;
use App\Models\Cobertur;
use App\Models\Clients;
use App\Models\Imagen;
use App\Models\Departament;
use App\Models\Municipio;
use App\Models\InfoRes;
use App\Models\typeDocuments;
use App\Models\typeUsers;
use App\Models\Users;
use App\Models\Adicional;
use App\Models\Cat_Adi;
use App\Models\TokenFcm;
use App\Models\Cupones;
use App\Models\Notification;
use App\Models\Terceros;
use App\Models\ProductsInventario;
use App\Models\Enters;
use App\Models\Prod_pedido;
use App\Models\Adic_pedido;
use DB;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Cookie;


//



class AdmonController extends Controller
{



  protected $notificationPush;

  public function __construct(NotificationPush $notificationPush)
  {
    $this->notificationPush = $notificationPush;
  }

  public function guard_pedido_call(Request $request){
    Log::info($request);
    $cart = json_decode($request['cart']);
    $valot_t = 0;
    foreach ($cart as $value) {
      Log::info($value->total);
      $valot_t = $valot_t + $value->total;
    }
    Log::info("este es el valor total");

    Log::info($valot_t);
    $pedido = Pedidos::create([
      'direcc_pedido' => $request['direccion'],
      'tel_pedido' => $request['celular'],
      'name_user' => $request['nombre'],
      'estado_pedido' => 1,
      'tipo_pedido' => 1,
      'valor_total' => $valot_t,
      'total_descuento' => 0,
      'metodo_pago' => 1,
    ]);

    foreach ($cart as $value) {
      $product = Prod_pedido::create([
        'id_pedido' => $pedido->id,
        'id_product' => $value->id,
        'cantidad' => $value->cantidad,
        'valor_u' =>  $value->preciou,
        'valot_t' => $value->total,
        'comentario' => $value->comentario,
        'descuento' => 0,
      ]);
      foreach ($value->adicionales as $valueadic) {
        $adicional = Adic_pedido::create([
          'id_pedido' => $pedido->id,
          'id_producto' => $product->id,
          'id_adic' => $valueadic->value
        ]);
      }
    }
    $response['success'] = true;
    return $response;

  }



  public function consult_enter(Request $request)
  {
    $response = [];
    if ($request['Producto'] != null) {
      $response['data'] = Enters::with('producto','proveedor')->whereDate('fecha','>=',$request['FechaIni'])->whereDate('fecha','<=',$request['FechaEnd'])
      ->where('product',$request['Producto'])->get();
    }
    else{
      $response['data'] = Enters::with('producto','proveedor')->whereDate('fecha','>=',$request['FechaIni'])->whereDate('fecha','<=',$request['FechaEnd'])->get();
    }

    $response['success'] = true;
    return $response;
  }
  public function generate_enter(Request $request)
  {
    Log::info($request);
    $data_to_record = Enters::orderBy('id','DESC')->where('product',$request['product'])->first();
    $valor_total_inv = 0;
    $costo_pro = 0;
    $saldo = 0;
    if ($data_to_record != null) {
      $valor_total_inv = $data_to_record->valor_total_inv;
      $costo_pro = $data_to_record->costo_pro;
      $saldo = $data_to_record->saldo;
    }
    if ($request['type'] == 2) {
      Enters::create([
        'type' => $request['type'],
        'product' => $request['product'],
        'fecha' => $request['fecha'],
        'descript'=> $request['descrip'],
        'proov' => $request['usuariosSelect'],
        'cantidad' => $request['cantidad'],
        'valor_unidad' => ($request['valor']/$request['cantidad']),
        'saldo' => $saldo+$request['cantidad'],
        'valor_total_inv' => $request['valor']+$valor_total_inv,
        'costo_pro' => ($request['valor']+$valor_total_inv)/($saldo+$request['cantidad']),
        'valor' => $request['valor'],
      ]);
    }
    else{
      Enters::create([
        'type' => $request['type'],
        'product' => $request['product'],
        'fecha' => $request['fecha'],
        'descript'=> $request['descrip'],
        'proov' => $request['usuariosSelect'],
        'cantidad' => ($request['cantidad']*-1),
        'valor_unidad' => $costo_pro,
        'saldo' => $saldo+($request['cantidad']*-1),
        'valor_total_inv' => (($request['cantidad']*-1)*$costo_pro)+$valor_total_inv,
        'costo_pro' => ((($request['cantidad']*-1)*$costo_pro)+$valor_total_inv)/($saldo+($request['cantidad']*-1)),
        'valor' =>($request['cantidad']*-1)*$costo_pro,
      ]);
    }

    $producto = ProductsInventario::where('prod_id',$request['product'])->first();
    $cantidad = $producto->amount;
    if ($request['type'] == 1) {
      $cantidad = $cantidad - $request['cantidad'];
    }
    else{
      $cantidad = $cantidad + $request['cantidad'];
    }
    Log::info("esta es la cantidad: ".$cantidad);
    ProductsInventario::where('prod_id',$request['product'])->update([
      'amount' => $cantidad
    ]);
    $response['success'] = true;
    return $response;

  }

  public function guard_producto_inventario(Request $request)
  {
    if ($request['id'] > 0) {
      ProductsInventario::where('prod_id',$request['id'])->update([
      'prod_nombre' => $request['nombre'],
      'prod_descp' => $request['descrip'],
      'unity' => $request['unidad'],
      'prod_proveedor' => $request['proovedores'],
      ]);
    }
    else{
      ProductsInventario::create([
      'prod_nombre' => $request['nombre'],
      'prod_descp' => $request['descrip'],
      'unity' => $request['unidad'],
      'prod_proveedor' => $request['proovedores'],
      ]);

    }
    $response['success'] = true;
    return $response;
  }


  public function obtain_productos_inventario(Request $request)
  {
    $product = ProductsInventario::with('proveedor')->where('prod_visible',0)->get();
    return $product;
  }
  public function inventario_productos(Request $request)
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.ProductsInventario',['cookie'=>$value]);
  }
  public function delete_proveedor(Request $request)
  {
    Terceros::where('user_id',$request['id'])->update(['delete' => 1]);
    $response = "success";
    return $response;
  }
  public function guard_proveedores( Request $request){
    if ($request['id'] > 0) {
      Terceros::where('user_id',$request['id'])->update([
      'user_name' => $request['name'],
      'type_user' => $request['type_user'],
      'user_name_2' => $request['name2'],
      'user_lastname' => $request['lastname'],
      'user_lastname_2' => $request['lastname2'],
      'user_phone' => $request['phone'],
      'user_document' => $request['document'],
      'user_email' => $request['email'],
      'ciudad' => $request['city'],
      'departamento' => $request['departament'],
      'user_adress' => $request['adress'],
      'tip_document' => $request['tipodocumento'],
      ]);
    }
    else{
      Terceros::create([
        'user_name' => $request['name'],
        'type_user' => $request['type_user'],
        'user_name_2' => $request['name2'],
        'user_lastname' => $request['lastname'],
        'user_lastname_2' => $request['lastname2'],
        'user_phone' => $request['phone'],
        'user_document' => $request['document'],
        'user_email' => $request['email'],
        'ciudad' => $request['city'],
        'departamento' => $request['departament'],
        'user_adress' => $request['adress'],
        'tip_document' => $request['tipodocumento'],

      ]);
    }
    $response = true;
    return $response;
  }

  public function obtain_proovedores(Request $request)
  {
    $response['data'] = Terceros::where('delete',0)->get();
    $response['success'] = true;
    return $response;
  }


  public function proovedores()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.Terceros',['cookie'=>$value]);
  }
  public function obtain_data_home()
  {
    $response['total_orden'] = Pedidos::get();
    $response['registro_usuarios'] = Clients::count();
    $response['success'] = true;
    return $response;
  }



  public function eliminar_foto1(Request $request)
  {
    Imagen::where('id_imagen','>',0)->update([
    'logo' => null
    ]);
    $response['success'] = true;
    return $response;
  }
  public function eliminar_foto2(Request $request)
  {
    Imagen::where('id_imagen','>',0)->update([
    'portada' => null
    ]);
    $response['success'] = true;
    return $response;
  }
  public function eliminar_foto3(Request $request)
  {
    Imagen::where('id_imagen','>',0)->update([
    'otra' => null
    ]);
    $response['success'] = true;
    return $response;
  }
  public function domicilioscheck(Request $request)
  {
    Notification::where('type_message',$request['type'])->update([
    'read_message'=>1
    ]);
    $response['success'] = true;
    return $response;
  }
  public function obtainNotifications()
  {
    $data = Notification::where('read_message',0)->get();
    $response['success'] = true;
    $response['data'] = $data;
    return $response;
  }
  public function pedidos()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.PedidosAdmon',['cookie'=>$value]);

  }
  public function pedidos2()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.PedidosAdmon2',['cookie'=>$value]);

  }
  public function pedidos3()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.PedidosAdmon3',['cookie'=>$value]);
  }
  public function delete_pedido(Request $request)
  {
    Pedidos::where('id_pedido',$request['id'])->update([
    'deleted'=>1
    ]);
    $datapusher['title'] = "Eliminación del pedido";
    $datapusher['message'] = "Tú pedido con el número ".$request['id']." fue eliminado";
    $pedido = Pedidos::where('id_pedido',$request['id'])->first();
    $this->notificationPush->notify_message_chat($datapusher,$pedido->token_pedido);

    $response['success'] = true;
    return $response;
  }
  public function edit_state_pedido(Request $request)
  {
    Pedidos::where('id_pedido',$request['id'])->update([
    'estado_pedido'=>$request['value'],
    'domiciliario_id'=>$request['domicilio'],
    'deleted' => 0
    ]);
    $datapusher['title'] = "Estado del pedido";
    if ($request['value'] == 1) {
      $datapusher['message'] = "Tú pedido con el número ".$request['id']." está pendiente de confirmación.";
    }
    if ($request['value'] == 2) {
      $datapusher['message'] = "Tú pedido con el número ".$request['id']." está en preparación.";
    }
    if ($request['value'] == 3) {
      $datapusher['message'] = "Tú pedido con el número ".$request['id']." Está en camino puedes ver su recorrido.";
    }
    if ($request['value'] == 4) {
      $datapusher['message'] = "Tú pedido con el número ".$request['id']." fue entregado, Gracias por preferirnos. Califica nuestro servicio!";
    }

    $pedido = Pedidos::where('id_pedido',$request['id'])->first();
    $this->notificationPush->notify_message_chat($datapusher,$pedido->token_pedido);
    $response['success'] = true;
    return $response;
  }
  public function obtain_pedidos()
  {
    $response['data'] = Pedidos::with('productos.producto','productos.adicionales.adicional','usuario')->orderBy('id_pedido','DESC')->get();
    $response['usuarios'] = Users::where('tip_user',1)->where('delete',0)->get();
    $response['success'] = true;
    return $response;

  }
  public function delete_cobertura(Request $request)
  {
    Cobertur::where('corb_id',$request['id'])->delete();
    $response['success'] = true;
    return $response;
  }
  public function guard_cobertura(Request $request)
  {
    if ($request['id']>0) {
      Cobertur::where('corb_id',$request['id'])->update([
      'corb_nombre' => $request['nombre'],
      'corb_latitud' => $request['lat'],
      'corb_longitude' => $request['lng'],
      'corb_valor' => $request['rango'],
      ]);
    }
    else{
      Cobertur::create([
      'corb_nombre' => $request['nombre'],
      'corb_latitud' => $request['lat'],
      'corb_longitude' => $request['lng'],
      'corb_valor' => $request['rango'],
      ]);
    }
    $response['success'] = true;
    return $response;
  }
  public function cobertura()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.CoberturaAdmon',['cookie'=>$value]);
  }
  public function obtain_cobertura()
  {
    $coberturas = Cobertur::get();
    return $coberturas;
  }
  public function encrypted()
  {
    $pass = "123456";
    $encript = Hash::make($pass);
    echo $encript;
  }
  public function form_log_admon()
  {
    return view('admon.LoginAdmon');
  }
  public function login_admon(Request $request)
  {
    $user = Admon::first();
    $login=Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
    if ($login) {
      Auth::guard('admin')->login($user);
      Cookie::queue(Cookie::forget('online_payment_id'));
      return response()->json('Authenticated',200);
    }else{
      $user2 = Users::where('user_email',$request->input('email'))->first();
      log::info($user2);
      $login2=Auth::guard('domiciliario')->attempt(['user_email' => $request->input('email'), 'password' => $request->input('password'), 'tip_user'=>2]);
      if ($login2) {
        Log::info("entrooooo");
        Auth::guard('admin')->login($user);
        Cookie::queue('online_payment_id', $user2, 5000000000000);

        return response()->json('Authenticated',200);
      }else{
        return response()->json(['errors'=>'Email o contraseña incorrectos.'],422);
      }
    }

  }

  public function logout_admin()
  {
    Auth::guard('admin')->logout();
    return redirect('/');
  }
  public function home(Request $request)
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);


    return view('admon.HomeAdmon',['cookie'=>$value]);

  }
  public function categorias()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.CategoriaAdmon',['cookie'=>$value]);

  }
  public function obtain_categories()
  {
    $categorias = Categories::where('cat_del',0)->get();
    return $categorias;
  }
  public function guard_categorie(Request $request)
  {

    if ($request['id'] > 0) {
      $cat = Categories::where('cat_id',$request['id'])->first();
      if ($request->file('imagen') == null) {
        $file = $cat->cat_imagen;
      }
      else{
        $file = $request->file('imagen')->store('imagenes_categorias','public');
        $file = "storage/".$file;
      }
      Categories::where('cat_id',$request['id'])->update([
      'cat_nombre' => $request['nombre'],
      'cat_imagen' => $file
      ]);
    }
    else{
      Log::info("Entro a crear");
      $file = $request->file('imagen')->store('imagenes_categorias','public');
      $file = "storage/".$file;
      Categories::create([
      'cat_nombre' => $request['nombre'],
      'cat_imagen' => $file
      ]);

    }
    $response['success'] = true;
    return $response;
  }

  public function delete_categoria(Request $request)
  {
    Categories::where('cat_id',$request['id'])->update([
    'cat_del'=>1
    ]);
    $response['success'] = true;
    return $response;
  }
  public function productos()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.ProductosAdmon',['cookie'=>$value]);

  }
  public function obtain_productos(){
    $product = Products::with('categoria.adicionales.adicional')->where('prod_visible',0)->get();
    return $product;
  }
  public function guard_productos(Request $request)
  {
    Log::info($request);
    if ($request['descuento']>0) {
      $TokenFcm = TokenFcm::get();
      $datapusher['title'] = "Descuento";
      $datapusher['message'] = "Disfruta de nuestro asombroso descuento en el producto ".$request['nombre'];
      foreach ($TokenFcm as $value) {
        $this->notificationPush->notify_message_chat($datapusher,$value->tdu_token);
      }
    }
    if ($request['id'] > 0) {
      $product = Products::where('prod_id',$request['id'])->first();
      if ($request->file('file') == null) {
        $file = $product->prod_imagen;
      }
      else{
        $file = $request->file('file')->store('imagenes_categorias','public');
        $file = "storage/".$file;
      }
      Products::where('prod_id',$request['id'])->update([
      'prod_nombre' => $request['nombre'],
      'prod_descp' => $request['descrip'],
      'prod_precio' => $request['price'],
      'prod_gastos' => $request['gastos'],
      'prod_descuento' => $request['descuento'],
      'prod_categoria' => $request['categoria'],
      'prod_imagen' => $file
      ]);
    }
    else{
      Log::info("Entro a crear");
      $file = $request->file('file')->store('imagenes_categorias','public');
      $file = "storage/".$file;
      Log::info($file);
      Products::create([
      'prod_nombre' => $request['nombre'],
      'prod_descp' => $request['descrip'],
      'prod_precio' => $request['price'],
      'prod_gastos' => $request['gastos'],
      'prod_descuento' => $request['descuento'],
      'prod_categoria' => $request['categoria'],
      'prod_imagen' => $file
      ]);

    }
    $response['success'] = true;
    return $response;
  }
  public function delete_productos(Request $request)
  {
    Products::where('prod_id',$request['id'])->update([
    'prod_visible'=>1
    ]);
    $response['success'] = true;
    return $response;
  }
  public function mesas()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.TableAdmon',['cookie'=>$value]);

  }
  public function obtain_tables(Request $request)
  {
    $mesas = Tables::first();
    return $mesas;
  }
  public function guard_tables(Request $request)
  {
    Log::info($request);
    $mesas = Tables::first();
    if ($mesas == null) {
      Log::info("entro a crear mesas");
      Tables::create([
      'cant_tab' => $request['mesas']
      ]);

    }
    else{
      Tables::where('id_tab',1)->update([
      'cant_tab' => $request['mesas']
      ]);
    }

    $response['success'] = true;
    return $response;
  }

  public function Info_rest()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.InfoAdmon',['cookie'=>$value]);

  }
  public function obtain_info(){
    $info = InfoRes::where('delete',0)->get();
    return $info;
  }

  public function guard_info(Request $request)
  {
    Log::info($request);

    if ($request['rest_id'] > 0) {

      InfoRes::where('rest_id',$request['rest_id'])->update([
      'rest_name' => $request['name'],
      'rest_nit' => $request['nit'],
      'rest_phone' => $request['phone'],
      'rest_adress' => $request['adress'],
      'open' => $request['open'],
      'close' => $request['close'],
      'price_domici' => $request['costo'],
      'value_min' => $request['valor_min'],
      'reservation' => $request['reserve'],
      'propina' =>$request['propina'],
      'cupo_max' => $request['cupoM'],
      'departamento' => $request['departamento'],
      'municipio' => $request['municipio'],
      'latitud' => $request['lat'],
      'longitude' =>$request['lng'],
      ]);
    }
    else{
      Log::info("Entro a crear");
      InfoRes::create([
      'rest_name' => $request['name'],
      'rest_nit' => $request['nit'],
      'rest_phone' => $request['phone'],
      'rest_adress' => $request['adress'],
      'open' => $request['open'],
      'close' => $request['close'],
      'price_domici' => $request['costo'],
      'value_min' => $request['valor_min'],
      'reservation' => $request['reserve'],
      'propina' =>$request['propina'],
      'cupo_max' => $request['cupoM'],
      'departamento' => $request['departamento'],
      'municipio' => $request['municipio'],
      'latitud' => $request['lat'],
      'longitude' =>$request['lng'],
      ]);

    }
    $response['success'] = true;
    return $response;

  }
  public function obtain_departament()
  {
    $departament = Departament::get();
    return $departament;
  }
  public function obtain_municipio()
  {
    $municipio = Municipio::get();
    return $municipio;
  }
  public function Banners()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.BannersAdmon',['cookie'=>$value]);

  }
  public function guard_img(Request $request)
  {
    Log::info($request);

    if ($request['id'] > 0) {
      $img = Imagen::where('id_imagen',$request['id'])->first();
      if ($request->file('logo') == null) {
        $file = $img->logo;
      }
      else{
        $file = $request->file('logo')->store('imagenes_banners','public');
        $file = "storage/".$file;
      }
      if ($request->file('portada') == null) {
        $fil = $img->portada;
      }
      else{
        $fil = $request->file('portada')->store('imagenes_banners','public');
        $fil = "storage/".$fil;
      }
      if ($request->file('otra') == null) {
        $file2 = $img->portada;
      }
      else{
        $file2 = $request->file('otra')->store('imagenes_banners','public');
        $file2 = "storage/".$file2;
      }

      Imagen::where('id_imagen',$request['id'])->update([
      'logo' => $file,
      'portada' =>$fil,
      'otra' =>$file2
      ]);
    }
    else{
      Log::info("Entro a crear");
      $file = $request->file('logo')->store('imagenes_banners','public');
      $file = "storage/".$file;
      $fil = $request->file('portada')->store('imagenes_banners','public');
      $fil = "storage/".$fil;
      $file2 = $request->file('otra')->store('imagenes_banners','public');
      $file2 = "storage/".$file2;
      Imagen::create([
      'logo' => $file,
      'portada' =>$fil,
      'otra' =>$file2
      ]);

    }
    $response['success'] = true;
    return $response;
  }
  public function obtain_img()
  {
    $img = Imagen::get();
    return $img;
  }

  public function usuarios()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.UserAdmon',['cookie'=>$value]);

  }

  public function obtain_users(){
    $users = Users::where('delete', 0)->get();
    return $users;
  }

  public function obtain_type_users(){
    $type_users =typeUsers:: where('delete', 1)->get();
    return $type_users;
  }

  public function obtain_type_documents(){
    $type_documents =typeDocuments::get();
    return $type_documents;
  }


  public function guard_users( Request $request){
    $password = Hash::make($request['password']);
    if ($request['id'] > 0) {
      Users::where('user_id',$request['id'])->update([
      'user_name' => $request['name'],
      'accesos' => $request['accesos'],
      'user_lastname' => $request['lastname'],
      'user_phone' => $request['phone'],
      'user_document' => $request['document'],
      'user_email' => $request['email'],
      'user_adress' => $request['adress'],
      'tip_document' => $request['tipodocumento'],
      'password' => $password,
      'tip_user' => $request['tipouser']
      ]);
    }
    else{
      Users::create([
      'user_name' => $request['name'],
      'accesos' => $request['accesos'],
      'user_lastname' => $request['lastname'],
      'user_phone' => $request['phone'],
      'user_document' => $request['document'],
      'user_email' => $request['email'],
      'user_adress' => $request['adress'],
      'tip_document' => $request['tipodocumento'],
      'password' => $password,
      'tip_user' => $request['tipouser']

      ]);
    }
    $response = true;
    return $response;
  }




  public function delete_users(Request $request)
  {
    Users::where('user_id',$request['id'])->update(['delete' => 1]);
    $response = "success";
    return $response;
  }
  public function Adicionales()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.AdicionalesAdmon',['cookie'=>$value]);
  }

  public function guard_add(Request $request){

    $cat_adi = json_decode($request['categoria']);


    if ($request['id'] > 0) {
      $dato = Adicional::where('id_adicional',$request['id'])->update([
      'name_add' => $request['name'],
      'price_add' => $request['price']
      ]);
      Cat_Adi::where('id',$request['id'])->delete();


      foreach ($cat_adi as $value) {
        Cat_Adi::create([
        'id_add' => $request['id'],
        'id_cat' => $value->value

        ]);
      }
    }
    else{
      Log::info("Entro a crear");
      $dato = Adicional::create([
      'name_add' => $request['name'],
      'price_add' => $request['price']
      ]);

      foreach ($cat_adi as $value) {
        Cat_Adi::create([
        'id_add' => $dato->id,
        'id_cat' => $value->value,
        ]);
      }

    }
    $response['success'] = true;
    return $response;
  }

  //   public function obtain_adicion(){
  //   $resul = Adicional::where('delete',0)->get();
  // return $resul;
  //}
  public function obtain_add(){
    $cat = Adicional::with('data.categoria')->where('delete',0)->orderBy('id_adicional','DESC')->get();
    return $cat;
    ///$res = Cat_Adi::where('delete',0)->get();
  }
  public function delete_adicional(Request $request)
  {
    Adicional::where('id_adicional',$request['id'])->update(['delete' => 1]);
    $response = "success";
    return $response;
  }
  public function clientes()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.ClientesAdmon',['cookie'=>$value]);
  }
  public function obtain_clients()
  {
    $clients = Clients::where('deleted',0)->get();
    return $clients;
  }
  public function delete_clients(Request $request)
  {
    Clients::where('id_usuario',$request['id'])->update(['deleted' => 1]);
    $response = "success";
    return $response;
  }
  public function cupones()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.CuponesAdmon',['cookie'=>$value]);
  }
  public function guard_cupon(Request $request)
  {
    if ($request['id'] > 0) {
      Cupones::where('id_cupon',$request['id'])->update([
      'nombre_cupon' => $request['nombre'],
      'cupon' => $request['cupon'],
      'productos' => $request['productos'],
      'categorias' => $request['categorias'],
      'descuento' => $request['descuento'],
      'todos' => $request['todos'],
      ]);
    }
    else{
      Cupones::create([
      'nombre_cupon' => $request['nombre'],
      'cupon' => $request['cupon'],
      'productos' => $request['productos'],
      'categorias' => $request['categorias'],
      'descuento' => $request['descuento'],
      'todos' => $request['todos'],
      ]);
    }
    $response = true;
    return $response;
  }
  public function obtain_cupon()
  {
    $cupon = Cupones::where('deleted',0)->get();
    return $cupon;
  }
  public function Cuenta()
  {
    $value = Cookie::get('online_payment_id');
    Log::info("cookieee");
    Log::info($value);
    return view('admon.CuentaAdmon',['cookie'=>$value]);
  }
  public function obtain_admin(){
    $admin = Admon::get();
    return $admin;
  }

  public function cambiar(Request $request){
    $respuesta =  Admon::where('id', $request['id'])->update([
    'email'=> $request['email']
    ]);
    return $respuesta;
  }
  public function recovery(Request $request){
    Log::info($request);
    Log::info("entro aqui perra");
    $user = Admon::first();
    $login=Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('pass')]);
    $password = Hash::make($request['password']);
    if(!$login){
      Log::info("entro aqui perra 2222");
      $res = Admon::where('id', $request['id'])->update([
      'password'=> $password
      ]);
      return $res;
    }
    else{
      return "false";
    }
  }
}
