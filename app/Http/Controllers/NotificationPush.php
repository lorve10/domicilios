<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Log;

class NotificationPush extends Controller
{

  public function notify_message_chat($data,$user)
  {
    $message = $data['message'];
    $title = $data['title'];
    $notification['user'] = $user;
    $notification['title'] = $title;
    $notification['message'] = $message;
    Log::info("Se envio la notificacion ==> $user");
    $this->send_notification_user($notification);

  }

  // funcion importante para enviar notificacion al celular
  public function send_notification_user($notification)

  {
    $fcmUrl = 'https://exp.host/--/api/v2/push/send';
    $fcmNotification = [
        'to'    => $notification['user'],
        'sound' => 'default',
        'title' => $notification['title'],
        'body' => $notification['message'],

    ];

    $headers = [
        //'Authorization: key=AIzaSyDkboV6dlxdWFwWCHjmuyZYutDvvFBoAmw',
        'Accept-encodinggzip, deflate',
        'Content-Type: application/json'
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    $result = curl_exec($ch);
    curl_close($ch);
  }

}
