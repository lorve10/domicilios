<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;
use App\Models\Categories;
use App\Models\Products;
use App\Models\Tables;
use App\Models\Cobertur;
use App\Models\Imagen;
use App\Models\Departament;
use App\Models\Municipio;
use App\Models\InfoRes;
use App\Models\typeDocuments;
use App\Models\typeUsers;
use App\Models\Clients;
use App\Models\Users;
use App\Models\TokenFcm;
use App\Models\Favorites;
use App\Models\Cupones;
use App\Models\Pedidos;
use App\Models\Prod_pedido;
use App\Models\Adic_pedido;
use App\Models\Notification;
use DB;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class HomeController extends Controller
{

  public function obtain_domiciliario(Request $request)
  {
    Log::info("entroo a consultar");
    Log::info($request);
    if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
      $response['data'] = Pedidos::with('productos.producto','productos.adicionales.adicional')->where('domiciliario_id',$request['id_usuario'])->where('estado_pedido', 3)->where('deleted',0)->get();
      Log::info($response['data']);
      $response['message'] = "";
      $response['success'] = true;
    }
    else{
      $response['message'] = "FAIL LOL";
      $response['success'] = false;
    }
    return $response;
  }
  public function login_domiciliario(Request $request)
  {
    Log::info("entroo a iniciar sesión");
    $user = Users::where('user_email',$request['email'])->first();
    $login=Auth::guard('domiciliario')->attempt(['user_email' => $request->input('email'), 'password' => $request->input('pass'), 'tip_user'=>1]);
    Log::info($login);
    $response = [];
    if ($login) {
      $response['success'] = true;
      $response['data'] = $user;
    }else{
      $response['success'] = false;
      $response['message'] = "Usuario o contraseña incorrecta";
    }
    Log::info($response);

    return $response;
  }
  public function edit_location_domiciliario(Request $request)
  {
    if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
      Users::where('user_id',$request['id_usuario'])->update([
        'latitud' => $request['latitude'],
        'longitud' => $request['longitude'],
      ]);
      $response['message'] = "";
      $response['success'] = true;
    }
    else{
      $response['message'] = "FAIL LOL";
      $response['success'] = false;
    }
    return $response;
  }
  public function edit_password(Request $request)
  {
    Log::info($request);
    $cliente = Clients::where('id_usuario',$request['id'])->first();
    $login=Auth::guard('client')->attempt(['email' => $cliente->email, 'password' => $request->input('oldPass')]);
    Log::info($login);
    $response = [];
    if ($login) {
      $pass = Hash::make($request['pass']);
      Clients::where('id_usuario',$request['id'])->update([
        'password' => $pass
      ]);
      $response['success'] = true;
      $response['message'] = "Contraseña cambiada correctamente";
    }
    else{
      $response['success'] = false;
      $response['message'] = "Contraseña actual incorrecta";
    }
    return $response;
  }

  public function edit_user(Request $request)
  {
    Log::info($request);
    $response=[];
      if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
        $cliente = Clients::where('id_usuario',$request['id'])->first();
        $validate_email = Clients::where('email',$request['correo'])->where('id_usuario', '!=',$request['id'])->first();
        $response['success'] = true;

        if ($validate_email == null) {
          Clients::where('id_usuario',$request['id'])->update([
            'email' => $request['correo'],
            'celphone_user' => $request['celular'],
            'direc_user' => $request['direccion'],
            'name_user' => $request['nombre'],
          ]);
          $response['message'] = "Datos cambiados correctamente";
          $response['success'] = true;
        }
        else{
          $response['message'] = "Este correo ya existe";
          $response['success'] = false;
        }
      }
      else{
        $response['message'] = "FAIL LOL";
        $response['success'] = false;
      }
      return $response;
  }
  public function obtain_data_user(Request $request)
  {
    $response=[];
      if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
        $response['usuario'] = Clients::where('id_usuario',$request['id_usuario'])->first();
        $response['pedido'] = Pedidos::with('productos.producto','productos.adicionales.adicional')->where('id_user',$request['id_usuario'])->orderBy('id_pedido', 'DESC')->get();
        $response['success'] = true;
      }
      else{
        $response['success'] = false;
      }
      return $response;
  }
  public function calificate_pedido(Request $request)
  {
    $response=[];
      if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
        Pedidos::where('id_pedido',$request['id_pedido'])->update([
          'comentario_cal' => $request['comentario'],
          'calification_service' => $request['calification_service'],
          'calification_food' => $request['calification_food'],
          'calification_time' => $request['calification_time'],
        ]);
        $response['success'] = true;
      }
      else{
        $response['success'] = false;
      }
      return $response;
  }
  public function guard_pedido(Request $request)
  {
    $response=[];
    $consulta_validate = Pedidos::where('estado_pedido',1)->where('direcc_pedido',$request['direcc'])->count();
    if ($consulta_validate >= $request['cupomax']) {
      $response['success'] = false;
      $response['data'] = null;
      return $response;
    }
    if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
      $carrito = json_decode($request['carrito']);
      $response['success'] = true;
      Log::info("si entro o no mk");
      Log::info($carrito);
      if ($request['tipo_pedido'] == 1) {
        Clients::where('id_usuario',$request['id_usuario'])->update([
          'direc_user' => $request['direcc'],
          'celphone_user' => $request['celular']
        ]);
        Notification::create([
          'message'=>'Tienes un nuevo pedido a domicilio',
          'type_message'=> 1
        ]);
      }
      else{
        Notification::create([
          'message'=>'Tienes un nuevo pedido para la '.$request['direcc'],
          'type_message'=> 2
        ]);
      }
      $pedido = Pedidos::create([
        'direcc_pedido' => $request['direcc'],
        'tel_pedido' => $request['celular'],
        'name_user' => $request['name_user'],
        'token_pedido' => $request['token_pedido'],
        'estado_pedido' => $request['estado'],
        'tipo_pedido' => $request['tipo_pedido'],
        'cupon' => $request['cupon'],
        'valor_total' => $request['valor_total'],
        'total_descuento' => $request['total_descuento'],
        'id_user' => $request['id_usuario'],
        'metodo_pago' => $request['metodoPago'],
      ]);
      foreach ($carrito as $value) {
        $descuento = 0;
        if ($value->total2 > 0) {
          $descuento = $value->total - $value->total2;
        }
        $product = Prod_pedido::create([
          'id_pedido' => $pedido->id,
          'id_product' => $value->producto->prod_id,
          'cantidad' => $value->cantidad,
          'valor_u' =>  $value->producto->prod_precio,
          'valot_t' => $value->total,
          'comentario' => $value->comentario,
          'descuento' => $descuento,
        ]);
        foreach ($value->filteradic as $valueadic) {
          $adicional = Adic_pedido::create([
            'id_pedido' => $pedido->id,
            'id_producto' => $product->id,
            'id_adic' => $valueadic->id_add
          ]);
        }

      }
      $response['data'] = $pedido;
    }
    else{
      $response['success'] = false;
      $response['data'] = null;
    }
    return $response;
  }
  public function consult_cupones(Request $request)
  {
    $response=[];
    if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
      $response['success'] = true;
      $response['data'] = Cupones::where('deleted',0)->get();
      $response['restaurant'] = InfoRes::first();
    }
    else{
      $response['success'] = false;
      $response['data'] = null;
    }
    return $response;
  }

  public function state_pedido(Request $request)
  {
    $response=[];
    if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
      $response['success'] = true;
      $response['pedido'] = Pedidos::with('productos.producto','productos.adicionales.adicional','usuario','domiciliario')->where('id_pedido',$request['id_pedido'])->where('deleted',0)->first();
    }
    else{
      $response['success'] = false;
      $response['data'] = null;
    }
    return $response;
  }
public function consult_data_user(Request $request)
{
  $response=[];
  if ($request['token_consulta'] == "100200OneTurpial-2021AdminConsult") {
    $response['success'] = true;
    $response['data'] = Clients::where('id_usuario',$request['id'])->first();
    $response['cupones'] = Cupones::where('deleted',0)->get();
    $response['restaurant'] = InfoRes::first();
  }
  else{
    $response['success'] = false;
    $response['data'] = null;
  }
  return $response;
}
  public function guardFavorites(Request $request)
  {
    Log::info("Entro a guardar favoritos");
    Log::info($request);
    if ($request['value'] == 1) {
      Favorites::create([
        'id_user' => $request['id'],
        'id_product' => $request['producto'],
      ]);
    }
    else{
      Favorites::where('id_user',$request['id'])->where('id_product',$request['producto'])->delete();
    }
  }
  public function obtainFavorites(Request $request)
  {
    Log::info("entroo a consultar favoritos");
    $response['data'] = Favorites::with('productos.categoria.adicionales.adicional')->where('id_user',$request['id'])->get();
    $response['success'] = true;
    return $response;
  }
  public function login_facebook(Request $request)
  {
    $user = Clients::where('email',$request['email'])->first();
    $id = null;
    if ($user) {
      $id = $user->id_usuario;
      Clients::where('email',$request['email'])->update([
        'facebook_id' => $request['facebook_id']
      ]);
    }
    else{
      $user = Clients::create([
        'email' => $request['email'],
        'name_user' => $request['name'],
        'facebook_id' => $request['facebook_id']
      ]);
      $id = $user->id;
    }

    TokenFcm::where('tdu_token',$request['token'])->update([
    'tdu_user' => $id
    ]);
    $response['success'] = true;
    $response['data'] = $user;
    $response['id'] = $id;
    return $response;
  }
  public function login_google(Request $request)
  {
    $user = Clients::where('email',$request['email'])->first();
    $id = null;
    if ($user) {
      $id = $user->id_usuario;
      Clients::where('email',$request['email'])->update([
        'google_id' => $request['google_id']
      ]);
    }
    else{
      $user = Clients::create([
        'email' => $request['email'],
        'name_user' => $request['name'],
        'google_id' => $request['google_id']
      ]);
      $id = $user->id;
    }

    TokenFcm::where('tdu_token',$request['token'])->update([
    'tdu_user' => $id
    ]);
    $response['success'] = true;
    $response['data'] = $user;
    $response['id'] = $id;
    return $response;
  }
  public function login(Request $request)
  {
    Log::info("entroo a iniciar sesión");
    $user = Clients::where('email',$request['email'])->first();
    $login=Auth::guard('client')->attempt(['email' => $request->input('email'), 'password' => $request->input('pass')]);
    Log::info($login);
    $response = [];
    if ($login) {
      TokenFcm::where('tdu_token',$request['token'])->update([
      'tdu_user' => $user->id_usuario
      ]);
      $response['success'] = true;
      $response['data'] = $user;
    }else{
      $response['success'] = false;
      $response['message'] = "Usuario o contraseña incorrecta";
    }
    Log::info($response);

    return $response;
  }
  public function user_register(Request $request)
  {
    Log::info($request);
    $validate = Clients::where('email',$request['email'])->first();
    $response = null;
    if ($validate) {
      $response['success'] = false;
      $response['message'] = "Usuario ya existente";
    }
    else{

      $pass = Hash::make($request['pass']);
      $cliente = Clients::create([
      'name_user' => $request['nombre'],
      'email' => $request['email'],
      'password' => $pass,
      'celphone_user' => $request['celphone'],
      'direc_user' => $request['direc'],
      ]);
      Log::info($request['token']);
      Log::info($cliente);
      TokenFcm::where('tdu_token',$request['token'])->update([
      'tdu_user' => $cliente->id
      ]);
      $response['success'] = true;
      $response['data'] = $cliente;
    }
    Log::info($response);
    return $response;

  }
  public function guard_token(Request $request)
  {
    Log::info($request);
    $exist = TokenFcm::where('tdu_token',$request['token'])->first();
    if ($exist) {
      TokenFcm::where('tdu_token',$request['token'])->update([
      'tdu_user' => $request['id_usuario']
      ]);
    }
    else{
      TokenFcm::create([
      'tdu_user' => $request['id_usuario'],
      'tdu_token' => $request['token'],
      ]);
    }
    $response = "true";
    return $response;
  }

  public function obtain_data_products()
  {
    Log::info("jajajajajajajaj");
    $Categories = Categories::where('cat_del',0)->get();
    $product = Products::with('categoria.adicionales.adicional')->where('prod_visible',0)->get();
    $banners = Imagen::first();
    $response['categoria'] = $Categories;
    $response['Products'] = $product;
    $response['Banners'] = $banners;
    return $response;
  }
}
